-- recreate the user --
REVOKE ALL PRIVILEGES ON DATABASE "fox" from foxuser;
REASSIGN OWNED BY foxuser TO postgres;
DROP DATABASE fox;
DROP USER foxuser;
CREATE USER foxuser WITH PASSWORD '1';

-- recreate the database --
CREATE DATABASE fox OWNER foxuser;
GRANT ALL PRIVILEGES ON DATABASE "fox" TO foxuser;

-- fulfill the database --
\connect "dbname=fox user=foxuser password=1"

BEGIN;
\i schema.sql
\i data.sql
COMMIT;
