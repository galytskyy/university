package ua.com.foxminded.university.aop;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import ua.com.foxminded.university.controller.ControllerAspect;
import ua.com.foxminded.university.dao.EntityRepositoryAspect;
import ua.com.foxminded.university.service.EntityServiceAspect;

@Configuration
@EnableAspectJAutoProxy
public class AopConfiguration {

    @Bean
    public CommonAspect commonAspect() {
        return new CommonAspect();
    }

    @Bean
    public EntityRepositoryAspect entityRepositoryAspect() {
        return new EntityRepositoryAspect();
    }

    @Bean
    public EntityServiceAspect entityServiceAspect() {
        return new EntityServiceAspect();
    }

    @Bean
    public ControllerAspect controllerAspect() {
        return new ControllerAspect();
    }
}
