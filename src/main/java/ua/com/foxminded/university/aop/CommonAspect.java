package ua.com.foxminded.university.aop;

import lombok.extern.log4j.Log4j2;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import java.lang.reflect.Method;
import static java.text.MessageFormat.format;

@Log4j2
@Aspect
public class CommonAspect extends AbstractAspect {

    @Pointcut("@annotation(ua.com.foxminded.university.aop.BeforeExecutionDoLogging) && " +
            "CommonAspect.notAnnotatedWithSurroundTryCatchAndDoLogging()")
    public void isAnnotatedWithBeforeDoLogging() {
        /* @Pointcut */
    }

    @Pointcut("@annotation(ua.com.foxminded.university.aop.SurroundTryCatchAndDoLogging)")
    public void isAnnotatedWithSurroundTryCatchAndDoLogging() {
        /* @Pointcut */
    }

    @Pointcut("!CommonAspect.isAnnotatedWithSurroundTryCatchAndDoLogging()")
    public void notAnnotatedWithSurroundTryCatchAndDoLogging() {
        /* @Pointcut */
    }

    @Pointcut("@annotation(ua.com.foxminded.university.aop.AfterReturningDoLogging)")
    public void isAnnotatedWithAfterReturningDoLogging() {
        /* @Pointcut */
    }

    @Pointcut("@annotation(ua.com.foxminded.university.aop.AfterThrowingDoLogging) && " +
            "CommonAspect.notAnnotatedWithSurroundTryCatchAndDoLogging()")
    public void isAnnotatedWithAfterThrowingDoLogging() {
        /* @Pointcut */
    }

    @Before("CommonAspect.isAnnotatedWithBeforeDoLogging()")
    public void beforeExecution(JoinPoint joinPoint) {
        logInvocation(joinPoint, log);
    }

    @Around("CommonAspect.isAnnotatedWithSurroundTryCatchAndDoLogging()")
    public Object aroundExecution(ProceedingJoinPoint joinPoint) {
        Class<? extends AppException> exceptionClass = getMethod(joinPoint)
                .getAnnotation(SurroundTryCatchAndDoLogging.class).value();

        return surroundTryCatchAndLogging(joinPoint, log, getExceptionConstructor(exceptionClass));
    }

    @AfterThrowing(pointcut = "CommonAspect.isAnnotatedWithAfterThrowingDoLogging()", throwing = "e")
    public void throwAfterDoLogging(JoinPoint joinPoint, Throwable e) {
        String longNameOfMethod = getLongNameOfMethod(joinPoint);
        Class<? extends AppException> exceptionClass = getMethod(joinPoint)
                .getAnnotation(AfterThrowingDoLogging.class).value();

        throwAfterDoLogging(longNameOfMethod, e, log, getExceptionConstructor(exceptionClass));
    }

    @AfterReturning(pointcut = "CommonAspect.isAnnotatedWithAfterReturningDoLogging()", returning = "returnedValue")
    public void afterReturning(JoinPoint joinPoint, Object returnedValue) {
        String longNameOfMethod = getLongNameOfMethod(joinPoint);
        log.info("Return {} from {}", returnedValue.toString(), longNameOfMethod);
    }

    private Method getMethod(JoinPoint joinPoint) {
        return ((MethodSignature) joinPoint.getSignature()).getMethod();
    }

    private ExceptionConstructor getExceptionConstructor(Class<? extends AppException> exceptionClass) {
        return (String m, Throwable t) -> {
            try {
                return exceptionClass.getDeclaredConstructor(String.class, Throwable.class).newInstance(m, t);
            } catch (Exception e) {
                String message = format("Error {0}.newInstance({1}, {2})", exceptionClass.getName(), m, t);
                log.error(message, e);
                return new AppException(message, e) {
                };
            }
        };
    }
}
