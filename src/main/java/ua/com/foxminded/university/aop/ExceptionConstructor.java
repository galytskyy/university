package ua.com.foxminded.university.aop;

import java.util.function.BiFunction;

public interface ExceptionConstructor extends BiFunction<String, Throwable, RuntimeException> {

}
