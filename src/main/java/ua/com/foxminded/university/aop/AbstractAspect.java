package ua.com.foxminded.university.aop;

import lombok.SneakyThrows;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import java.util.Arrays;
import static java.text.MessageFormat.format;
import static org.apache.commons.lang3.StringUtils.substring;

public abstract class AbstractAspect {

    private static final String INVOKED = "Invoke {}";
    private static final String INVOCATION_ERROR = "Invocation error: {0} Cause: {1}";

    protected AbstractAspect() {
    }

    public String getInvocationErrorMessage(String methodName, Throwable e) {
        return format(INVOCATION_ERROR, methodName, getCauseMessage(e));
    }

    public String getFatalErrorMessage(String methodName, Throwable e) {
        return format(INVOCATION_ERROR, methodName, getCauseMessage(e));
    }

    private String getCauseMessage(Throwable e) {
        return e.getClass().getSimpleName() + "{ " + e.getMessage() + " }";
    }

    public String getLongNameOfMethod(JoinPoint joinPoint) {
        return format("{0}.{1}({2})",
                joinPoint.getTarget(), joinPoint.getSignature().getName(), toStringWithoutBrackets(joinPoint.getArgs()));
    }

    @SneakyThrows
    public Object surroundTryCatchAndLogging(ProceedingJoinPoint joinPoint, Logger log,
                                             ExceptionConstructor exceptionConstructor) {
        log.getMessageFactory().newMessage("").getFormattedMessage();
        String longNameOfMethod = getLongNameOfMethod(joinPoint);
        log.info(INVOKED, longNameOfMethod);
        Object result = null;
        try {
            result = joinPoint.proceed();
        } catch (Throwable e) {
            throwAfterDoLogging(longNameOfMethod, e, log, exceptionConstructor);
        }
        return result;
    }

    public void logInvocation(JoinPoint joinPoint, Logger log) {
        String longNameOfMethod = getLongNameOfMethod(joinPoint);
        log.info(INVOKED, longNameOfMethod);
    }

    @SneakyThrows
    public void throwAfterDoLogging(String longNameOfMethod, Throwable e, Logger log,
                                    ExceptionConstructor exceptionConstructor) {
        if (e instanceof RuntimeException) {
            String invocationErrorMessage = getInvocationErrorMessage(longNameOfMethod, e);
            log.error(invocationErrorMessage, e);
            throw exceptionConstructor.apply(invocationErrorMessage, e);
        } else {
            String fatalErrorMessage = getFatalErrorMessage(longNameOfMethod, e);
            log.fatal(fatalErrorMessage, e);
            throw e;
        }
    }

    private String toStringWithoutBrackets(Object[] array) {
        return substring(Arrays.toString(array), 1, -1);
    }
}
