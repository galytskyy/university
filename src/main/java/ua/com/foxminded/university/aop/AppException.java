package ua.com.foxminded.university.aop;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = false)
public abstract class AppException extends RuntimeException {

    public AppException(String message) {
        super(message);
    }

    public AppException(String message, Throwable cause) {
        super(message, cause);
    }

    public AppException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
