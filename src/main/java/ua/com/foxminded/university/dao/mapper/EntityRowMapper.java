package ua.com.foxminded.university.dao.mapper;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import ua.com.foxminded.university.entity.Entity;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import static java.util.Objects.isNull;

public class EntityRowMapper<T extends Entity<?>> implements RowMapper<T> {

    private final Function<T, Integer> idGetter;
    private final RowMapper<T> entityMapper;
    private final Map<T, T> entityMap = new HashMap<>();

    public EntityRowMapper(Class<T> tClass, Function<T, Integer> idGetter) {
        this.idGetter = idGetter;
        this.entityMapper = BeanPropertyRowMapper.newInstance(tClass);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public static <T> RowMapper<T> newInstance(Class<T> tClass, Function<T, Integer> idGetter) {
        return new EntityRowMapper(tClass, idGetter);
    }

    @Override
    public T mapRow(ResultSet rs, int rowNum) throws SQLException {
        T entity = entityMapper.mapRow(rs, rowNum);
        return (isNull(entity) || isNull(idGetter.apply(entity))) ? null : entityMap.computeIfAbsent(entity, (e) -> e);
    }
}
