package ua.com.foxminded.university.dao;

import lombok.NonNull;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Component;
import ua.com.foxminded.university.dao.query.*;
import ua.com.foxminded.university.entity.Room;
import ua.com.foxminded.university.service.criteria.RoomCriteria;
import javax.annotation.PostConstruct;
import java.util.Map;
import static java.text.MessageFormat.format;

@Component
public class RoomQueryFactory extends AbstractQueryFactory<Room> {

    static final String ROOM_FIELDS = format("{0}room_id, {0}room_name", "rooms.");
    static final String SELECT_FROM_ROOMS = format("""
            SELECT {1}
            FROM {0}rooms
            """, SCHEMA, ROOM_FIELDS);

    @PostConstruct
    void init() {
        RoomCriteria.setRoomQueryFactory(this);
    }

    @Override
    Count<Room> count() {
        return new CountEntities<>(format("SELECT COUNT(room_id) FROM {0}rooms", SCHEMA));
    }

    @Override
    public Select<Room> selectAll() {
        return new SelectEntities<>(SELECT_FROM_ROOMS,
                BeanPropertyRowMapper.newInstance(Room.class));
    }

    @Override
    public SelectOne<Room> selectById(int id) {
        return new SelectEntity<>(SELECT_FROM_ROOMS + """
                WHERE rooms.room_id = :roomId
                """,
                Map.of("roomId", id),
                BeanPropertyRowMapper.newInstance(Room.class));
    }

    @Override
    Insert<Room> insert(Room entity) {
        return new InsertEntity<>(format("""
                INSERT INTO {0}rooms(room_name)
                VALUES (:roomName)
                """, SCHEMA),
                entity);
    }

    @Override
    Delete<Room> delete(Room entity) {
        return new DeleteEntity<>(format("""
                DELETE FROM {0}rooms
                WHERE rooms.room_id = :roomId
                """, SCHEMA),
                entity);
    }

    @Override
    Update<Room> update(Room entity) {
        return new UpdateEntity<>(format("""
                UPDATE {0}rooms
                SET room_name = :roomName
                WHERE rooms.room_id = :roomId
                """, SCHEMA),
                new BeanPropertySqlParameterSource(entity));
    }

    public Select<Room> selectByLikeName(@NonNull String likeName) {
        return new SelectEntities<>(SELECT_FROM_ROOMS + """
                WHERE rooms.room_name LIKE :likeName
                """,
                Map.of("likeName", likeName + "%"),
                BeanPropertyRowMapper.newInstance(Room.class));
    }
}
