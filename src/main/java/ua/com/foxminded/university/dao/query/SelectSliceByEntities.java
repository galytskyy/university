package ua.com.foxminded.university.dao.query;

import lombok.ToString;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import ua.com.foxminded.university.entity.Entity;
import java.util.Optional;
import java.util.function.Function;

@ToString(callSuper = true)
public class SelectSliceByEntities<T extends Entity<?>> extends SelectEntities<T> implements SelectSlice<T> {

    private final Function<T, Optional<T>> dtoToOptionalMapper;

    public SelectSliceByEntities(String sql, RowMapper<T> rowMapper, Function<T, Optional<T>> dtoToOptionalMapper) {
        super(sql, rowMapper);
        this.dtoToOptionalMapper = dtoToOptionalMapper;
    }

    @Override
    public QueryResultOfSlice<T> selectSlice(NamedParameterJdbcTemplate jdbcTemplate) {
        return QueryResultOfSlice.newInstance(() -> jdbcTemplate.queryForStream(sql, namedParams, rowMapper),
                this.dtoToOptionalMapper);
    }
}
