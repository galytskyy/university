package ua.com.foxminded.university.dao;

import lombok.NonNull;
import lombok.Value;
import org.springframework.stereotype.Component;
import ua.com.foxminded.university.dao.mapper.LectureRowMapper;
import ua.com.foxminded.university.dao.mapper.SliceByLecturesRowMapper;
import ua.com.foxminded.university.dao.query.*;
import ua.com.foxminded.university.entity.Lecture;
import ua.com.foxminded.university.service.criteria.LectureCriteria;
import ua.com.foxminded.university.timetable.Filter;
import javax.annotation.PostConstruct;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Stream;
import static java.lang.System.lineSeparator;
import static java.text.MessageFormat.format;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toMap;
import static org.apache.commons.lang3.BooleanUtils.isFalse;
import static ua.com.foxminded.university.dao.GroupQueryFactory.GROUP_FIELDS;
import static ua.com.foxminded.university.dao.LectureQueryFactory.LectureOrder.Field.*;
import static ua.com.foxminded.university.dao.RoomQueryFactory.ROOM_FIELDS;
import static ua.com.foxminded.university.dao.TeacherQueryFactory.TEACHER_FIELDS;

@Component
public class LectureQueryFactory extends AbstractQueryFactory<Lecture> {

    static final String TABLE_NAME = "lectures";
    static final String TABLE_FULL_NAME = SCHEMA + TABLE_NAME;
    static final String COUNT_BY = "lecture_id";
    static final String LECTURE_FIELDS =
            format("{0}lecture_id, {0}lecture_name, {0}lecture_date, {0}beginning, {0}ending", TABLE_NAME + ".");

    static final String COUNT_ALL_LECTURES = sqlStringBuilder().countBy("lectureId").build().getCountSql();
    static final String SELECT_FROM_LECTURES = sqlStringBuilder().field(LECTURE_FIELDS).build().getSelectSql();
    static final String SELECT_ALL_FROM_LECTURES = sqlStringBuilderFull().build().getSelectSql();

    static SqlString.SqlStringBuilder sqlStringBuilder() {
        return SqlString.builder().fromTable(TABLE_FULL_NAME).field(LECTURE_FIELDS);
    }

    static SqlString.SqlStringBuilder sqlStringBuilderFull() {
        return sqlStringBuilder().fields(List.of(LS + TEACHER_FIELDS, GROUP_FIELDS, ROOM_FIELDS))
                .leftJoins(Stream.of(
                        "{0}teachers ON teachers.teacher_id = lectures.teacher_id",
                        "{0}groups ON groups.group_id = lectures.group_id",
                        "{0}rooms ON rooms.room_id = lectures.room_id")
                        .map(lJ -> format(lJ, SCHEMA)).toList());
    }

    @PostConstruct
    void init() {
        LectureCriteria.setLectureQueryFactory(this);
    }

    @Override
    Count<Lecture> count() {
        return new CountEntities<>(COUNT_ALL_LECTURES);
    }

    @Override
    public Select<Lecture> selectAll() {
        return new SelectEntities<>(SELECT_ALL_FROM_LECTURES, LectureRowMapper.newInstance());
    }

    @Override
    public SelectOne<Lecture> selectById(int lectureId) {
        String sql = sqlStringBuilderFull().where("lectures.lecture_id = :lectureId").build().getSelectSql();
        return new SelectEntity<>(sql, Map.of("lectureId", lectureId), LectureRowMapper.newInstance());
    }

    @Override
    Insert<Lecture> insert(@NonNull Lecture entity) {
        return new InsertEntity<>(format("""
                INSERT INTO {0}lectures(lecture_name, lecture_date, beginning, ending, teacher_id, group_id, room_id)
                VALUES (:lectureName, :lectureDate, :beginning, :ending, :teacherId, :groupId, :roomId);
                """, SCHEMA),
                entity);
    }

    @Override
    Update<Lecture> update(@NonNull Lecture entity) {
        return new UpdateEntity<>(format("""
                UPDATE {0}lectures
                SET lecture_name = :lectureName, lecture_date = :lectureDate, beginning = :beginning, ending = :ending, 
                    teacher_id = :teacherId, group_id = :groupId, room_id = :roomId
                WHERE lecture_id = :lectureId;
                """, SCHEMA),
                entity);
    }

    @Override
    Delete<Lecture> delete(@NonNull Lecture entity) {
        return new DeleteEntity<>(format("""
                DELETE FROM {0}lectures
                WHERE lectures.lecture_id = :lectureId;
                """, SCHEMA),
                entity);
    }

    private Function<Lecture, Optional<Lecture>> dtoToLectureOptional() {
        return (Lecture l) -> {
            if (isNull(l.getLectureId())) {
                return Optional.empty();
            }
            Lecture lecture = l.copy();
            lecture.setLectureId(l.getLectureId());
            return Optional.of(lecture);
        };
    }

    public SelectSliceByEntities<Lecture> selectSlice(@NonNull LectureOrder orderBy, int offset, int limit) {
        String sql = sqlStringBuilderFull().countBy(COUNT_BY).orderBy(orderBy.asString())
                .limit(limit).offset(offset).build().getSelectSql();
        return new SelectSliceByEntities<>(sql, SliceByLecturesRowMapper.newInstance(), dtoToLectureOptional());
    }

    public SelectSliceByEntities<Lecture> selectSliceByFilter(@NonNull Filter filter, @NonNull LectureOrder orderBy,
                                                              int offset, int limit) {
        String whereClause = buildWhereByFilter(filter);
        String sql = sqlStringBuilderFull().countBy(COUNT_BY).where(whereClause).orderBy(orderBy.asString())
                .limit(limit).offset(offset).build().getSelectSql();
        return new SelectSliceByEntities<>(sql, SliceByLecturesRowMapper.newInstance(), dtoToLectureOptional());
    }

    public Select<Lecture> selectByFilter(@NonNull Filter filter) {
        String whereClause = buildWhereByFilter(filter);
        String sql = sqlStringBuilderFull().where(whereClause).build().getSelectSql();
        return new SelectEntities<>(sql, LectureRowMapper.newInstance());
    }

    private String buildWhereByFilter(Filter filter) {
        List<String> whereClauses = buildWhereListForFilter(filter);
        if (filter.isOnlyDrafts()) {
            addConditionForDraft(whereClauses);
        } else {
            addConditionForGroup(whereClauses, filter);
            addConditionForTeacher(whereClauses, filter);
        }
        whereClauses.add("TRUE");
        return whereClauses.stream().collect(joining(lineSeparator() + ") AND (", "(", ")"));
    }

    private List<String> buildWhereListForFilter(Filter filter) {
        List<String> whereClauses = new LinkedList<>();
        final String dateBetween = "lecture_date BETWEEN '%tF' AND '%tF'";
        whereClauses.add(String.format(dateBetween, filter.getBeginningDate(), filter.getEndingDate()));
        if (isFalse(filter.isAllDay())) {
            final String timeBetween = "beginning BETWEEN '%tT' AND '%tT'";
            whereClauses.add(String.format(timeBetween, filter.getBeginningTime(), filter.getEndingTime()));
        }
        return whereClauses;
    }

    private void addConditionForDraft(List<String> whereClauses) {
        whereClauses.add("lectures.group_id IS NULL OR lectures.teacher_id IS NULL OR lectures.room_id IS NULL");
    }

    private void addConditionForGroup(List<String> whereClauses, Filter filter) {
        if (isFalse(filter.isAllGroups())) {
            String conditionalGroupId = isNull(filter.getGroupId()) ? "IS NULL" : "= " + filter.getGroupId();
            whereClauses.add(format("lectures.group_id {0}", conditionalGroupId));
        }
    }

    private void addConditionForTeacher(List<String> whereClause, Filter filter) {
        if (isFalse(filter.isAllTeachers())) {
            String conditionalTeacherId = isNull(filter.getTeacherId()) ? "IS NULL" : "= " + filter.getTeacherId();
            whereClause.add(format("lectures.teacher_id {0}", conditionalTeacherId));
        }
    }

    @Value
    public static class LectureOrder {

        Map<String, String> orderBy = new LinkedHashMap<>();

        public static LectureOrder create(Field firstBy, Map<Field, Direction> orderByMap) {
            LectureOrder lectureOrder = new LectureOrder();
            (switch (firstBy) {
                case DATETIME -> List.of(DATETIME, GROUP, TEACHER);
                case GROUP -> List.of(GROUP, DATETIME, TEACHER);
                case TEACHER -> List.of(TEACHER, DATETIME, GROUP);
            }).forEach((f) -> f.withDirection(lectureOrder, orderByMap.get(f)));
            return lectureOrder;
        }

        public static Map<Field, Direction> mapForAll(Function<Field, Direction> directionByField) {
            return Arrays.stream(Field.values()).collect(toMap(k -> k, directionByField,
                    (d1, d2) -> d1, () -> new EnumMap<>(Field.class)));
        }

        public LectureOrder dateTime(@NonNull Direction direction) {
            putToEnd("lecture_date", direction);
            putToEnd("beginning", direction);
            return this;
        }

        public LectureOrder group(@NonNull Direction direction) {
            putToEnd("group_name", direction);
            return this;
        }

        public LectureOrder teacher(@NonNull Direction direction) {
            putToEnd("surname", direction);
            putToEnd("first_name", direction);
            return this;
        }

        public LectureOrder orderBy(@NonNull Field field, Direction direction) {
            return field.withDirection(this, direction);
        }

        void putToEnd(String fieldName, Direction direction) {
            orderBy.remove(fieldName);
            orderBy.put(fieldName, direction.name());
        }

        public String asString() {
            Stream.Builder<String> streamBuilder = Stream.builder();
            orderBy.forEach((k, v) -> streamBuilder.accept(k + " " + v));
            streamBuilder.accept("lecture_id ASC");
            return streamBuilder.build().collect(joining(", "));
        }

        public enum Field {
            DATETIME("dateTime", LectureOrder::dateTime),
            GROUP("group", LectureOrder::group),
            TEACHER("teacher", LectureOrder::teacher);

            private final String alias;
            private final BiFunction<LectureOrder, Direction, LectureOrder> orderByWither;

            Field(String alias, BiFunction<LectureOrder, Direction, LectureOrder> orderByWither) {
                this.alias = alias;
                this.orderByWither = orderByWither;
            }

            public static Field of(String alias) {
                try {
                    return Field.valueOf(String.valueOf(alias).toUpperCase());
                } catch (Exception e) {
                    return firstByDefault();
                }
            }

            public static Field firstByDefault() {
                return DATETIME;
            }

            public String alias() {
                return this.alias;
            }

            LectureOrder withDirection(LectureOrder lectureOrder, Direction direction) {
                if (nonNull(direction)) {
                    return this.orderByWither.apply(lectureOrder, direction);
                }
                return lectureOrder;
            }
        }

        public enum Direction {
            ASC, DESC;

            public static Direction of(String alias) {
                try {
                    return Direction.valueOf(String.valueOf(alias).toUpperCase());
                } catch (Exception e) {
                    return byDefault();
                }
            }

            public static Direction byDefault() {
                return ASC;
            }

            public String alias() {
                return this.name().toLowerCase();
            }
        }
    }
}
