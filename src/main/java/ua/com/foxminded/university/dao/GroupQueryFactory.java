package ua.com.foxminded.university.dao;

import lombok.NonNull;
import lombok.ToString;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import ua.com.foxminded.university.dao.mapper.StudentRowMapper;
import ua.com.foxminded.university.dao.query.*;
import ua.com.foxminded.university.entity.Group;
import ua.com.foxminded.university.entity.Student;
import ua.com.foxminded.university.service.criteria.GroupCriteria;
import javax.annotation.PostConstruct;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;
import static java.text.MessageFormat.format;
import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toSet;
import static ua.com.foxminded.university.dao.StudentQueryFactory.STUDENT_FIELDS;

@Component
public class GroupQueryFactory extends AbstractQueryFactory<Group> {

    static final String GROUP_FIELDS = format("{0}group_id, {0}group_name", "groups.");
    static final String SELECT_FROM_GROUPS = format("""
            SELECT {1}
            FROM {0}groups
            """, SCHEMA, GROUP_FIELDS);

    @PostConstruct
    void init() {
        GroupCriteria.setGroupQueryFactory(this);
    }

    @Override
    Count<Group> count() {
        return new CountEntities<>(format("SELECT COUNT(group_id) FROM {0}groups", SCHEMA));
    }

    @Override
    public Select<Group> selectAll() {
        return new SelectEntities<>(SELECT_FROM_GROUPS,
                BeanPropertyRowMapper.newInstance(Group.class));
    }

    @Override
    Insert<Group> insert(@NonNull Group entity) {
        return new InsertEntity<>(format("""
                INSERT INTO {0}groups(group_name)
                VALUES (:groupName)
                """, SCHEMA),
                entity);
    }

    @Override
    Delete<Group> delete(@NonNull Group entity) {
        return new DeleteEntity<>(format("""
                DELETE FROM {0}groups
                WHERE groups.group_id = :groupId
                """, SCHEMA),
                entity);
    }

    @Override
    Update<Group> update(@NonNull Group entity) {
        return new UpdateEntity<>(format("""
                UPDATE {0}groups
                SET group_name = :groupName
                WHERE groups.group_id = :groupId
                """, SCHEMA),
                entity);
    }

    public SelectOne<Group> selectById(int groupId) {
        return new SelectEntity<>(SELECT_FROM_GROUPS + """
                WHERE groups.group_id = :groupId
                """,
                Map.of("groupId", groupId),
                BeanPropertyRowMapper.newInstance(Group.class));
    }

    public SelectOne<Group> selectById(int groupId, StudentsSupplierWither supplierSetter) {
        final String fromTable = format("{0}groups", SCHEMA);
        final List<String> leftJoins = List.of(
                format("{0}student_group ON student_group.group_id = groups.group_id", SCHEMA),
                format("{0}students ON student_group.student_id = students.student_id", SCHEMA));
        String sql = SqlString.builder().field(GROUP_FIELDS).fromTable(fromTable)
                .field(STUDENT_FIELDS).leftJoins(leftJoins)
                .where("groups.group_id = :groupId")
                .build().getSelectSql();
        return new SelectEagerGroup(sql, Map.of("groupId", groupId), supplierSetter);
    }

    public Select<Group> selectByLikeName(@NonNull String likeName) {
        return new SelectEntities<>(SELECT_FROM_GROUPS + """
                WHERE groups.group_name LIKE :likeName
                """,
                Map.of("likeName", likeName + "%"),
                BeanPropertyRowMapper.newInstance(Group.class));
    }

    public interface StudentsSupplierWither extends BiFunction<Group, Supplier<Set<Student>>, Group> {

    }
}

@ToString(callSuper = true)
class SelectEagerGroup extends SelectEntity<Group> {

    protected final RowMapper<Student> studentRowMapper = StudentRowMapper.newInstance();
    protected final GroupQueryFactory.StudentsSupplierWither supplierWither;

    public SelectEagerGroup(String sql, Map<String, Object> namedParams, GroupQueryFactory.StudentsSupplierWither supplierWither) {
        super(sql, namedParams, BeanPropertyRowMapper.newInstance(Group.class));
        this.supplierWither = supplierWither;
    }

    @Override
    public Optional<Group> selectForOptional(NamedParameterJdbcTemplate jdbcTemplate) {
        Function<Map.Entry<Group, Set<Student>>, Group> toEagerGroup =
                (e) -> supplierWither.apply(e.getKey(), e::getValue);
        return jdbcTemplate.queryForStream(super.sql, super.namedParams, studentRowMapper)
                .filter(Objects::nonNull).collect(groupingBy(Student::getGroup, toSet()))
                .entrySet().stream().filter(e -> nonNull(e.getKey())).limit(1).map(toEagerGroup).findAny();
    }
}
