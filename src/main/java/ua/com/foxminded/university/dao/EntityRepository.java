package ua.com.foxminded.university.dao;

import lombok.NonNull;
import ua.com.foxminded.university.dao.query.*;
import ua.com.foxminded.university.entity.Entity;
import java.util.Optional;

public interface EntityRepository<T extends Entity<?>> {

    QueryResult<T> findAll();

    QueryResult<T> findByQuery(Select<T> query);

    QueryResultOfSlice<T> findSlice(@NonNull SelectSliceByEntities<T> query);

    Optional<T> findAny(SelectOne<T> query);

    Optional<T> findById(int id);

    int count();

    int add(T entity);

    int remove(T entity);

    int replace(T entity);
}
