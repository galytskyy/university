package ua.com.foxminded.university.dao.query;

import lombok.ToString;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.transaction.annotation.Transactional;
import ua.com.foxminded.university.entity.Entity;
import static java.util.Objects.nonNull;

@ToString(callSuper = true)
public class InsertEntity<T extends Entity<?>> extends Query implements Insert<T> {

    protected SqlParameterSource namedParameters;
    protected T entity;

    public InsertEntity(String sql, T entity) {
        super(sql);
        this.namedParameters = new BeanPropertySqlParameterSource(entity);
        this.entity = entity;
    }

    @Override
    @Transactional
    public int insert(NamedParameterJdbcTemplate jdbcTemplate) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        int updatedRows = jdbcTemplate.update(sql, namedParameters, keyHolder);
        if (updatedRows > 0) {
            trySetId(keyHolder);
        }
        return updatedRows;
    }

    protected boolean trySetId(KeyHolder keyHolder) {
        Integer key = keyHolder.getKeyAs(Integer.class);
        if (nonNull(key)) {
            this.entity.setId(key);
            return true;
        }
        return false;
    }
}
