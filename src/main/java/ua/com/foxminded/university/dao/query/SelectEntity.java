package ua.com.foxminded.university.dao.query;

import lombok.ToString;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import ua.com.foxminded.university.entity.Entity;
import java.util.Map;
import java.util.Optional;

@ToString(callSuper = true)
public class SelectEntity<T extends Entity<?>> extends SelectEntities<T> implements SelectOne<T> {

    public SelectEntity(String sql, Map<String, Object> namedParams, RowMapper<T> rowMapper) {
        super(sql, namedParams, rowMapper);
    }

    @Override
    public Optional<T> selectForOptional(NamedParameterJdbcTemplate jdbcTemplate) {
        return jdbcTemplate.queryForStream(super.sql, super.namedParams, super.rowMapper).findAny();
    }
}
