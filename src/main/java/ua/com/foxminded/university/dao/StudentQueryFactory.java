package ua.com.foxminded.university.dao;

import lombok.NonNull;
import lombok.ToString;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ua.com.foxminded.university.dao.mapper.StudentRowMapper;
import ua.com.foxminded.university.dao.query.*;
import ua.com.foxminded.university.entity.Student;
import ua.com.foxminded.university.service.criteria.StudentCriteria;
import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Map;
import static java.text.MessageFormat.format;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.joining;
import static ua.com.foxminded.university.dao.AbstractQueryFactory.SCHEMA;
import static ua.com.foxminded.university.dao.GroupQueryFactory.GROUP_FIELDS;

@Component
public class StudentQueryFactory extends AbstractQueryFactory<Student> {

    static final String STUDENT_FIELDS = format("{0}student_id, {0}first_name, {0}surname", "students.");
    static final String SELECT_FROM_STUDENTS_JOIN_GROUPS = format("""
            SELECT {1}, {2}
            FROM {0}students
            LEFT JOIN {0}student_group ON student_group.student_id = students.student_id
            LEFT JOIN {0}groups ON groups.group_id = student_group.group_id
            """, SCHEMA, STUDENT_FIELDS, GROUP_FIELDS);

    @Override
    Count<Student> count() {
        return new CountEntities<>(format("SELECT COUNT(student_id) FROM {0}students", SCHEMA));
    }

    @Override
    public Select<Student> selectAll() {
        return new SelectEntities<>(SELECT_FROM_STUDENTS_JOIN_GROUPS,
                StudentRowMapper.newInstance());
    }

    @Override
    public SelectOne<Student> selectById(int id) {
        if (id <= 0) {
            return EmptyQuery.emptyQuery();
        }
        return new SelectEntity<>(SELECT_FROM_STUDENTS_JOIN_GROUPS + """
                WHERE students.student_id = :studentId
                """,
                Map.of("studentId", id),
                StudentRowMapper.newInstance());
    }

    @PostConstruct
    void init() {
        StudentCriteria.setStudentQueryFactory(this);
    }

    @Override
    Insert<Student> insert(Student entity) {
        return new InsertStudent(entity);
    }

    @Override
    Update<Student> update(Student entity) {
        if (entity.getId() <= 0) {
            return EmptyQuery.emptyQuery();
        }
        final String UPDATE_STUDENTS = format("""
                UPDATE {0}students
                SET first_name = :firstName, surname = :surname
                WHERE students.student_id = :studentId;
                """, SCHEMA);
        BeanPropertySqlParameterSource sqlParameterSource = new BeanPropertySqlParameterSource(entity);
        if (isNull(entity.getGroupId())) {
            return new UpdateEntity<>(UPDATE_STUDENTS + format("""
                    UPDATE {0}student_group SET group_id = NULL
                    WHERE student_group.student_id = :studentId;
                    """, SCHEMA),
                    sqlParameterSource);
        } else {
            return new UpdateEntity<>(UPDATE_STUDENTS + format("""
                    INSERT INTO {0}student_group(student_id, group_id) VALUES(:studentId, :groupId)
                    ON CONFLICT DO NOTHING;
                    UPDATE {0}student_group SET group_id = :groupId
                    WHERE student_group.student_id = :studentId;
                    """, SCHEMA),
                    sqlParameterSource);
        }
    }

    @Override
    Delete<Student> delete(Student entity) {
        if (entity.getId() <= 0) {
            return EmptyQuery.emptyQuery();
        }
        return new DeleteEntity<>(format("""
                DELETE FROM {0}students
                WHERE students.student_id = :studentId
                """, SCHEMA),
                entity);
    }

    public Select<Student> selectByLikeFullName(@NonNull String likeFullName) {
        likeFullName = Arrays.stream(likeFullName.split("(\\s)+")).collect(joining("%", "", "%"));
        return new SelectEntities<>(SELECT_FROM_STUDENTS_JOIN_GROUPS + """
                WHERE students.first_name || ' ' || students.surname LIKE :likeFullName
                """,
                Map.of("likeFullName", likeFullName),
                StudentRowMapper.newInstance());
    }

    public Select<Student> selectByGroupId(int groupId) {
        if (groupId <= 0) {
            return EmptyQuery.emptyQuery();
        }
        return new SelectEntities<>(SELECT_FROM_STUDENTS_JOIN_GROUPS + """
                WHERE student_group.group_id = :groupId
                """,
                Map.of("groupId", groupId),
                StudentRowMapper.newInstance());
    }
}

@ToString(callSuper = true)
class InsertStudent extends InsertEntity<Student> {

    protected static final String INSERT_STUDENT = format("""
            INSERT INTO {0}students(first_name, surname) VALUES (:firstName, :surname)
            """, SCHEMA);
    protected static final String INSERT_STUDENT_GROUP = format("""
            INSERT INTO {0}student_group(student_id, group_id) VALUES (:studentId, :groupId)
            """, SCHEMA);

    public InsertStudent(Student entity) {
        super(INSERT_STUDENT, entity);
    }

    @Override
    @Transactional
    public int insert(NamedParameterJdbcTemplate jdbcTemplate) {
        Integer studentId = super.entity.getStudentId();
        BeanPropertySqlParameterSource parameterSource = new BeanPropertySqlParameterSource(super.entity);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        int updatedRows = jdbcTemplate.update(INSERT_STUDENT, parameterSource, keyHolder);
        if (updatedRows > 0 && super.trySetId(keyHolder) && nonNull(super.entity.getGroupId())
                && (jdbcTemplate.update(INSERT_STUDENT_GROUP, parameterSource) < 1)) {
            updatedRows = 0;
            super.entity.setStudentId(studentId);
        }
        return updatedRows;
    }
}