package ua.com.foxminded.university.dao.mapper;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import ua.com.foxminded.university.entity.Group;
import ua.com.foxminded.university.entity.Student;
import java.sql.ResultSet;
import java.sql.SQLException;
import static java.util.Objects.isNull;

public class StudentRowMapper implements RowMapper<Student> {

    private final RowMapper<Student> studentMapper = BeanPropertyRowMapper.newInstance(Student.class);
    private final RowMapper<Group> groupMapper = EntityRowMapper.newInstance(Group.class, Group::getGroupId);

    public static StudentRowMapper newInstance() {
        return new StudentRowMapper();
    }

    @Override
    public Student mapRow(ResultSet rs, int rowNum) throws SQLException {
        Student student = studentMapper.mapRow(rs, rowNum);
        if (isNull(student) || isNull(student.getStudentId())) {
            return null;
        }
        student.setGroup(groupMapper.mapRow(rs, rowNum));
        return student;
    }
}
