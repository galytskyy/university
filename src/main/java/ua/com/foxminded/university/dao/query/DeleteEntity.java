package ua.com.foxminded.university.dao.query;

import lombok.ToString;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import ua.com.foxminded.university.entity.Entity;
import static java.lang.Integer.min;

@ToString(callSuper = true)
public class DeleteEntity<T extends Entity<?>> extends Query implements Delete<T> {

    protected SqlParameterSource namedParams;
    protected T entity;

    public DeleteEntity(String sql, T entity) {
        super(sql);
        this.namedParams = new BeanPropertySqlParameterSource(entity);
        this.entity = entity;
    }

    @Override
    public int delete(NamedParameterJdbcTemplate jdbcTemplate) {
        int result = jdbcTemplate.update(sql, namedParams);
        if (result > 0) {
            entity.setId(-entity.getId());
        }
        return min(1, result);
    }
}
