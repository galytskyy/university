package ua.com.foxminded.university.dao.query;

import lombok.ToString;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.EmptySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import ua.com.foxminded.university.entity.Entity;
import java.util.Map;

@ToString(callSuper = true)
public class SelectEntities<T extends Entity<?>> extends Query implements Select<T> {

    protected RowMapper<T> rowMapper;
    protected SqlParameterSource namedParams;

    public SelectEntities(String sql, RowMapper<T> rowMapper) {
        super(sql);
        initialize(new EmptySqlParameterSource(), rowMapper);
    }

    public SelectEntities(String sql, SqlParameterSource namedParams, RowMapper<T> rowMapper) {
        super(sql);
        initialize(namedParams, rowMapper);
    }

    public SelectEntities(String sql, Map<String, Object> namedParams, RowMapper<T> rowMapper) {
        super(sql);
        initialize(new MapSqlParameterSource(namedParams), rowMapper);
    }

    private void initialize(SqlParameterSource namedParams, RowMapper<T> rowMapper) {
        this.rowMapper = rowMapper;
        this.namedParams = namedParams;
    }

    @Override
    public QueryResult<T> selectResult(NamedParameterJdbcTemplate jdbcTemplate) {
        return QueryResult.newInstance(() -> jdbcTemplate.queryForStream(sql, namedParams, rowMapper));
    }
}
