package ua.com.foxminded.university.dao.mapper;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import ua.com.foxminded.university.dao.DaoException;
import ua.com.foxminded.university.dao.dto.SliceByLecturesDto;
import ua.com.foxminded.university.entity.Lecture;
import java.sql.ResultSet;
import java.sql.SQLException;
import static java.text.MessageFormat.format;
import static java.util.Objects.nonNull;

public class SliceByLecturesRowMapper extends LectureRowMapper implements RowMapper<Lecture> {

    private final RowMapper<SliceByLecturesDto> sliceDtoRowMapper =
            BeanPropertyRowMapper.newInstance(SliceByLecturesDto.class);

    public static SliceByLecturesRowMapper newInstance() {
        return new SliceByLecturesRowMapper();
    }

    @Override
    public SliceByLecturesDto mapRow(ResultSet rs, int rowNum) throws SQLException {
        SliceByLecturesDto sliceByLecturesDto = sliceDtoRowMapper.mapRow(rs, rowNum);
        Lecture lecture = super.mapRow(rs, rowNum);
        if (nonNull(sliceByLecturesDto)) {
            if (nonNull(lecture)) {
                return sliceByLecturesDto.withLecture(lecture);
            }
            return sliceByLecturesDto;
        }
        String message = format("Mapping error: {0}.rowMap(rs={1}, rowNum={2})", getClass().getName(), rs, rowNum);
        throw new DaoException(message, new RuntimeException());
    }
}
