package ua.com.foxminded.university.dao.query;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import ua.com.foxminded.university.entity.Entity;

public interface Delete<T extends Entity<?>> {

    int delete(NamedParameterJdbcTemplate jdbcTemplate);
}
