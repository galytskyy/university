package ua.com.foxminded.university.dao.query;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import ua.com.foxminded.university.entity.Entity;

public interface Insert<T extends Entity<?>> {

    int insert(NamedParameterJdbcTemplate jdbcTemplate);
}
