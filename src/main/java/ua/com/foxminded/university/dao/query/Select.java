package ua.com.foxminded.university.dao.query;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import ua.com.foxminded.university.entity.Entity;

public interface Select<T extends Entity<?>> {

    QueryResult<T> selectResult(NamedParameterJdbcTemplate jdbcTemplate);
}
