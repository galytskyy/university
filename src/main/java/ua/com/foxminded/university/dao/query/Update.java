package ua.com.foxminded.university.dao.query;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import ua.com.foxminded.university.entity.Entity;

public interface Update<T extends Entity<?>> {

    int update(NamedParameterJdbcTemplate jdbcTemplate);
}
