package ua.com.foxminded.university.dao.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ua.com.foxminded.university.entity.Lecture;

@Data
@EqualsAndHashCode(callSuper = false)
public class SliceByLecturesDto extends Lecture implements DaoSummary {

    DaoSummary daoSummary = DaoSummary.newInstance();

    public SliceByLecturesDto withLecture(Lecture lecture) {
        super.setGroup(lecture.getGroup());
        super.setTeacher(lecture.getTeacher());
        super.setRoom(lecture.getRoom());
        return this;
    }

    public DaoSummary getDaoSummary() {
        return daoSummary;
    }

    /* This getter is required for AbstractEntity  */
    public Integer getSliceByLecturesDtoId() {
        return super.getLectureId();
    }

    /* This getter is required for AbstractEntity  */
    public void setSliceByLecturesDtoId(Integer id) {
        super.setLectureId(id);
    }

    @Override
    public boolean nonPersistence() {
        return true;
    }
}
