package ua.com.foxminded.university.dao.query;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import ua.com.foxminded.university.entity.Entity;

public interface SelectSlice<T extends Entity<?>> extends Select<T> {

    QueryResultOfSlice<T> selectSlice(NamedParameterJdbcTemplate jdbcTemplate);
}
