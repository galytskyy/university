package ua.com.foxminded.university.dao;

import lombok.NonNull;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Component;
import ua.com.foxminded.university.dao.query.*;
import ua.com.foxminded.university.entity.Teacher;
import ua.com.foxminded.university.service.criteria.TeacherCriteria;
import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Map;
import static java.text.MessageFormat.format;
import static java.util.stream.Collectors.joining;

@Component
public class TeacherQueryFactory extends AbstractQueryFactory<Teacher> {

    static final String TEACHER_FIELDS = format("{0}teacher_id, {0}first_name, {0}surname", "teachers.");
    static final String SELECT_FROM_TEACHERS = format("""
            SELECT {1} 
            FROM {0}teachers
            """, SCHEMA, TEACHER_FIELDS);

    @PostConstruct
    void init() {
        TeacherCriteria.setTeacherQueryFactory(this);
    }

    @Override
    Count<Teacher> count() {
        return new CountEntities<>(format("SELECT COUNT(teacher_id) FROM {0}teachers", SCHEMA));
    }

    @Override
    public Select<Teacher> selectAll() {
        return new SelectEntities<>(SELECT_FROM_TEACHERS,
                BeanPropertyRowMapper.newInstance(Teacher.class));
    }

    @Override
    public SelectOne<Teacher> selectById(int id) {
        return new SelectEntity<>(SELECT_FROM_TEACHERS + """
                WHERE teachers.teacher_id = :teacherId
                """,
                Map.of("teacherId", id),
                BeanPropertyRowMapper.newInstance(Teacher.class));
    }

    @Override
    Insert<Teacher> insert(Teacher entity) {
        return new InsertEntity<>(format("""
                INSERT INTO {0}teachers(first_name, surname)
                VALUES (:firstName, :surname)
                """, SCHEMA),
                entity);
    }

    @Override
    Delete<Teacher> delete(Teacher entity) {
        return new DeleteEntity<>(format("""
                DELETE FROM {0}teachers
                WHERE teachers.teacher_id = :teacherId
                """, SCHEMA),
                entity);
    }

    @Override
    Update<Teacher> update(Teacher entity) {
        return new UpdateEntity<>(format("""
                UPDATE {0}teachers
                SET first_name = :firstName, surname = :surname
                WHERE teachers.teacher_id = :teacherId
                """, SCHEMA),
                new BeanPropertySqlParameterSource(entity));
    }

    public Select<Teacher> selectByLikeFullName(@NonNull String likeFullName) {
        likeFullName = Arrays.stream(likeFullName.split("(\\s)+")).collect(joining("%", "", "%"));
        return new SelectEntities<>(SELECT_FROM_TEACHERS + """
                WHERE teachers.first_name || ' ' || teachers.surname LIKE :likeFullName
                """,
                Map.of("likeFullName", likeFullName),
                BeanPropertyRowMapper.newInstance(Teacher.class));
    }
}
