package ua.com.foxminded.university.dao.query;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import ua.com.foxminded.university.entity.Entity;
import java.util.Optional;

public interface SelectOne<T extends Entity<?>> extends Select<T> {

    Optional<T> selectForOptional(NamedParameterJdbcTemplate jdbcTemplate);
}
