package ua.com.foxminded.university.dao.query;

import lombok.*;
import lombok.extern.log4j.Log4j2;
import ua.com.foxminded.university.aop.AbstractAspect;
import ua.com.foxminded.university.dao.DaoException;
import ua.com.foxminded.university.dao.dto.DaoSummary;
import ua.com.foxminded.university.entity.Entity;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.flatMapping;
import static java.util.stream.Collectors.groupingBy;

public class QueryResults {

    private QueryResults() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    public static <T> Collector<T, ?, Set<T>> toLinkedSet() {
        return Collectors.toCollection(LinkedHashSet::new);
    }

    public interface Summary {

        static Summary empty() {
            return DaoSummary.newInstance();
        }

        default Integer getTotal() {
            return this.getSummary().getTotal();
        }

        default Integer getLimit() {
            return this.getSummary().getLimit();
        }

        default Integer getOffset() {
            return this.getSummary().getOffset();
        }

        Summary getSummary();
    }

    @Data
    @Log4j2
    @Getter(AccessLevel.PACKAGE)
    @Setter(AccessLevel.PRIVATE)
    @EqualsAndHashCode(callSuper = false)
    @AllArgsConstructor(access = AccessLevel.PROTECTED)
    abstract static class AbstractQueryResult<T extends Entity<?>> extends AbstractAspect {

        protected Supplier<Stream<T>> streamSupplier;

        Stream<T> getStream() {
            if (nonNull(streamSupplier)) {
                try {
                    return streamSupplier.get();
                } catch (Throwable t) {
                    String longNameOfMethod = toString() + ".getStream()";
                    super.throwAfterDoLogging(longNameOfMethod, t, log, DaoException::new);
                }
            }
            return Stream.empty();
        }
    }

    @ToString(callSuper = true)
    @EqualsAndHashCode(callSuper = true)
    static class QueryResultImpl<T extends Entity<?>> extends AbstractQueryResult<T> implements QueryResult<T> {

        public QueryResultImpl(Supplier<Stream<T>> streamSupplier) {
            super(streamSupplier);
        }

        @Override
        public <R, U, A> R collect(ResultCollector<T, U, A, R> collector) {
            return collector.collect(this.getStream());
        }

        @Override
        public <R, A> R collect(@NonNull Collector<? super T, A, R> collector) {
            return getStream().collect(collector);
        }

        @Override
        public Optional<T> findAny(@NonNull Predicate<T> predicate) {
            return getStream().filter(predicate).findAny();
        }
    }

    @ToString(callSuper = true)
    @EqualsAndHashCode(callSuper = true)
    static class QueryResultOfSliceImpl<T extends Entity<?>> extends QueryResultImpl<T> implements QueryResultOfSlice<T> {

        private final Function<T, Optional<T>> dtoToOptional;

        QueryResultOfSliceImpl(Supplier<Stream<T>> streamSupplier, Function<T, Optional<T>> dtoToOptionalMapper) {
            super(streamSupplier);
            this.dtoToOptional = dtoToOptionalMapper;
        }

        @Override
        public <R, U, A> R collect(@NonNull ResultCollector<T, U, A, R> collector) {
            return collector.collect(getNonNullFilteredStream());
        }

        @Override
        public <R, A> R collect(@NonNull Collector<? super T, A, R> collector) {
            return getNonNullFilteredStream().collect(collector);
        }

        @Override
        public Optional<T> findAny(@NonNull Predicate<T> predicate) {
            return getNonNullFilteredStream().filter(predicate).findAny();
        }

        private Stream<T> getNonNullFilteredStream() {
            return filterEmptyAndMapToEntity(this.getStream().map(dtoToOptional));
        }

        private Stream<T> filterEmptyAndMapToEntity(Stream<Optional<T>> tOptionalStream) {
            return tOptionalStream.filter(Optional::isPresent).map(Optional::get);
        }

        @Override
        public <A> SliceResult<A> collect(@NonNull Function<T, A> mapper) {
            Function<T, Summary> toSummary = (sliceDto) -> ((Summary) sliceDto).getSummary();
            Function<T, Stream<A>> toEntityStream = dtoToOptional.andThen(Stream::of)
                    .andThen(this::filterEmptyAndMapToEntity).andThen(t -> t.map(mapper));

            return this.getStream().collect(groupingBy(toSummary, flatMapping(toEntityStream, toLinkedSet())))
                    .entrySet().stream().limit(1).map(SliceResult::of).findAny().orElse(SliceResult.empty());
        }
    }

    @ToString
    public static class SliceResult<D> implements Summary {

        @Getter
        private final Summary summary;
        @Getter
        private final Collection<D> rows;

        private SliceResult(Summary summary, Collection<D> list) {
            this.summary = summary;
            this.rows = list;
        }

        public static <D> SliceResult<D> empty() {
            return new SliceResult<>(Summary.empty(), Collections.emptySet());
        }

        static <A> SliceResult<A> of(Map.Entry<Summary, ? extends Collection<A>> entry) {
            return new SliceResult<>(entry.getKey(), entry.getValue());
        }
    }

    @Value
    @Getter(AccessLevel.PACKAGE)
    @AllArgsConstructor(staticName = "of")
    public static class ResultCollector<T extends Entity<?>, U, A, R> {

        @NonNull
        Predicate<T> predicate;
        @NonNull
        Comparator<T> comparator;
        @NonNull
        Function<? super T, ? extends U> mapper;
        @NonNull
        Collector<? super U, A, R> collector;

        R collect(Stream<T> stream) {
            return stream.filter(predicate).sorted(comparator).map(mapper).collect(collector);
        }
    }
}
