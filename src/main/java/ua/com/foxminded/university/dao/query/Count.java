package ua.com.foxminded.university.dao.query;

import org.springframework.jdbc.core.JdbcTemplate;
import ua.com.foxminded.university.entity.Entity;

public interface Count<T extends Entity<?>> {

    Integer selectForInteger(JdbcTemplate jdbcTemplate);
}
