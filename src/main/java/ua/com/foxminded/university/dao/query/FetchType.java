package ua.com.foxminded.university.dao.query;

public enum FetchType {

    LAZY,
    EAGER
}
