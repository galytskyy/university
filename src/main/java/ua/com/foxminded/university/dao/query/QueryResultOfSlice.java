package ua.com.foxminded.university.dao.query;

import lombok.NonNull;
import ua.com.foxminded.university.entity.Entity;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

public interface QueryResultOfSlice<T extends Entity<?>> extends QueryResult<T> {

    static <T extends Entity<?>> QueryResultOfSlice<T>
    newInstance(Supplier<Stream<T>> streamSupplier, Function<T, Optional<T>> dtoToOptionalMapper) {
        return new QueryResults.QueryResultOfSliceImpl<>(streamSupplier, dtoToOptionalMapper);
    }

    <D> QueryResults.SliceResult<D> collect(@NonNull Function<T, D> mapper);
}
