package ua.com.foxminded.university.dao;

import lombok.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringSubstitutor;
import org.springframework.stereotype.Component;
import ua.com.foxminded.university.dao.query.Count;
import ua.com.foxminded.university.dao.query.Delete;
import ua.com.foxminded.university.dao.query.Insert;
import ua.com.foxminded.university.dao.query.Update;
import ua.com.foxminded.university.entity.Entity;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;
import static java.lang.Integer.max;
import static java.text.MessageFormat.format;
import static java.util.stream.Collectors.joining;
import static org.apache.commons.lang3.StringUtils.*;

@Component
abstract class AbstractQueryFactory<T extends Entity<?>> implements QueryFactory<T> {

    static final String LS = "\n";
    static String SCHEMA = "";

    static void setSchema(String schema) {
        AbstractQueryFactory.SCHEMA = stripToEmpty(schema);
    }

    abstract Count<T> count();

    abstract Insert<T> insert(T entity);

    abstract Delete<T> delete(T entity);

    abstract Update<T> update(T entity);

    @Value
    @Builder
    @Getter(AccessLevel.PRIVATE)
    @Setter(AccessLevel.PRIVATE)
    static class SqlString {

        /**
         * from_table - full name of table (e.g. "university.lectures" or "lectures")
         * field_names - all necessary fields from tables or others (e.g. "0 AS zero, count_sq.count_id, lectures.lecture_id")
         * where - WHERE (e.g. "WHERE table_name.field_name IS NOT NULL")
         * joins - JOINs (e.g. "LEFT JOIN table_name ON table_name.field_name = lectures.lecture_id
         * |                    JOIN (SELECT * FROM ...) AS subquery_table_name")
         * order_by - ORDER BY, LIMIT, OFFSET (e.g. "ORDER BY table_name.field_name ASC")
         * limitOffset - LIMIT-OFFSET (e.g. "LIMIT 7", "LIMIT 7 OFFSET 17")
         * Blocks JOINs, WHERE, ORDER BY, LIMIT-OFFSET can be empty
         */
        static final String SELECT_TEMPLATE = """
                SELECT {@field_names}
                FROM {@from_table}
                {@joins}
                {@where}
                {@order_by}
                {@limitOffset}
                """;
        /**
         * from_table
         * count_by - field name from main table for COUNT(count_by) (e.g. "lecture_id") !!! without dot
         * where
         */
        static final String COUNT_TEMPLATE = """
                SELECT COUNT({@count_by}) AS _$total 
                FROM {@from_table}
                {@where}
                """;
        static final Map<String, String> SUBSTITUTES = Map.of(
                "from_table", "{0}",
                "field_names", "{1}",
                "count_by", "{1}",
                "where", "{2}",
                "joins", "{3}",
                "order_by", "{4}",
                "limitOffset", "{5}"
        );
        static Predicate<String> isNotBlank = StringUtils::isNotBlank;

        String fromTable;
        @Singular
        List<String> fields;
        String countBy;
        @Singular
        List<String> leftJoins;
        String where;
        @Singular("orderBy")
        List<String> ordersBy;
        int limit;
        int offset;

        private static String substitute(String template) {
            return new StringSubstitutor(SUBSTITUTES, "{@", "}").replace(template);
        }

        static String createSelectSql(@NonNull Params params) {
            return format(substitute(SELECT_TEMPLATE), params.getFromTable(), params.getFields(),
                    params.getWhere(), params.getJoins(), params.getOrderBy(), params.getLimitOffset());
        }

        static String createCountSql(@NonNull Params params) {
            return format(substitute(COUNT_TEMPLATE), params.getFromTable(), params.getCountBy(), params.getWhere());
        }

        static String finalizeSql(String text) {
            return text.lines().filter(isNotBlank).map(String::strip).collect(joining(LS, "", ";"));
        }

        static Function<Object, String> makeFormatter(String template) {
            return (s) -> format(template, s);
        }

        static Function<Object, String> makeFormatter(String template, Object arg1) {
            return (s) -> format(template, arg1, s);
        }

        String getSelectSql() {
            Params params = getParamsBuilder().fields(getFields()).joins(getLeftJoins()).orderBy(getOrderBy())
                    .limitOffset(getLimitOffset()).build();
            String selectData = createSelectSql(params);
            if (isBlank(this.countBy)) {
                return finalizeSql(selectData);
            }
            String fromTable = format("({0}{1}{0}) AS _data$sq", LS, selectData);
            String rightJoin = format("RIGHT JOIN ({0}{1}{0}) AS _count$sq ON TRUE", LS, getRawCountSql());
            params = Params.builder().fields("*").fromTable(fromTable).joins(rightJoin).build();
            return finalizeSql(createSelectSql(params));
        }

        String getCountSql() {
            return finalizeSql(getRawCountSql());
        }

        String getRawCountSql() {
            Params params = getParamsBuilder().countBy(tryGetCountBy().orElse("*")).build();
            return createCountSql(params);
        }

        Params.ParamsBuilder getParamsBuilder() {
            return Params.builder().fromTable(getFromTable()).where(getWhere());
        }

        private String getFields() {
            return Stream.concat(this.fields.stream(), tryGetFieldsLimitOffset().stream())
                    .filter(isNotBlank).collect(joining(", "));
        }

        public Optional<String> tryGetCountBy() {
            return Optional.ofNullable(stripToNull(countBy));
        }

        private Optional<String> tryGetFieldsLimitOffset() {
            int limit = max(0, this.limit);
            int offset = max(0, this.offset);
            if (limit == 0 && offset == 0) {
                return Optional.empty();
            }
            return Optional.of(format("{0}{1} AS _$limit, {2} AS _$offset", LS, limit, offset));
        }

        private Optional<String> tryGetCountJoin() {
            UnaryOperator<String> applyFormat = (c) -> format("({0}{1}{0}) AS _count$sq ON TRUE",
                    LS, getRawCountSql());
            return tryGetCountBy().map(applyFormat);
        }

        private String getLeftJoins() {
            Function<Object, String> applyFormat = makeFormatter("LEFT JOIN {0}");
            return this.leftJoins.stream().filter(isNotBlank).map(applyFormat).collect(joining(LS));
        }

        private String getWhere() {
            Function<Object, String> applyFormat = makeFormatter("WHERE ({0})");
            return Optional.ofNullable(stripToNull(this.where)).map(applyFormat).orElse("");
        }

        private String getOrderBy() {
            Function<Object, String> applyFormat = makeFormatter("ORDER BY {0}");
            BinaryOperator<String> concat = (a, b) -> a + ", " + b;
            return this.ordersBy.stream().filter(isNotBlank).reduce(concat).map(applyFormat).orElse("");
        }

        private String getLimitOffset() {
            int limit = max(0, this.limit);
            int offset = max(0, this.offset);
            if (limit == 0) {
                if (offset == 0) {
                    return "";
                }
                return format("LIMIT ALL OFFSET {0}", offset);
            }
            return format("LIMIT {0} OFFSET {1}", limit, offset);
        }

        @Value
        @Builder
        @AllArgsConstructor(access = AccessLevel.PRIVATE)
        static class Params {

            @Builder.Default
            String fromTable = "";
            @Builder.Default
            String fields = "*";
            @Builder.Default
            String countBy = "";
            @Builder.Default
            String joins = "";
            @Builder.Default
            String where = "";
            @Builder.Default
            String orderBy = "";
            @Builder.Default
            String limitOffset = "";
        }
    }
}
