package ua.com.foxminded.university.dao.query;

import lombok.Getter;
import static org.apache.commons.lang3.StringUtils.substring;

@Getter
public abstract class Query {

    private static boolean TRACE_SQL;
    protected String sql;

    protected Query(String sql) {
        this.sql = sql;
    }

    public static void setTraceSql(boolean traceSql) {
        Query.TRACE_SQL = traceSql;
    }

    @Override
    public String toString() {
        if (Query.TRACE_SQL) {
            return "Query{sql='\n" + sql + "\n'}";
        }
        return "Query{sql='" + substring(sql, 0, 50) + "...'}";
    }
}
