package ua.com.foxminded.university.dao.query;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import ua.com.foxminded.university.entity.Entity;
import java.util.Optional;
import java.util.stream.Stream;

@ToString
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class EmptyQuery<T extends Entity<?>> implements
        SelectOne<T>, SelectSlice<T>, Insert<T>, Update<T>, Delete<T>, Count<T> {

    public static <A extends Entity<?>> EmptyQuery<A> emptyQuery() {
        return new EmptyQuery<>();
    }

    @Override
    public QueryResult<T> selectResult(NamedParameterJdbcTemplate jdbcTemplate) {
        return QueryResult.newInstance(Stream::empty);
    }

    @Override
    public QueryResultOfSlice<T> selectSlice(NamedParameterJdbcTemplate jdbcTemplate) {
        return QueryResultOfSlice.newInstance(Stream::empty, (T e) -> Optional.empty());
    }

    @Override
    public Optional<T> selectForOptional(NamedParameterJdbcTemplate jdbcTemplate) {
        return Optional.empty();
    }

    @Override
    public int delete(NamedParameterJdbcTemplate jdbcTemplate) {
        return 0;
    }

    @Override
    public int insert(NamedParameterJdbcTemplate jdbcTemplate) {
        return 0;
    }

    @Override
    public int update(NamedParameterJdbcTemplate jdbcTemplate) {
        return 0;
    }

    @Override
    public Integer selectForInteger(JdbcTemplate jdbcTemplate) {
        return 0;
    }
}
