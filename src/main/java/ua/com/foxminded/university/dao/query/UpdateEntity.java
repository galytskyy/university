package ua.com.foxminded.university.dao.query;

import lombok.ToString;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.transaction.annotation.Transactional;
import ua.com.foxminded.university.entity.Entity;

@ToString(callSuper = true)
public class UpdateEntity<T extends Entity<?>> extends Query implements Update<T> {

    protected SqlParameterSource namedParameters;

    public UpdateEntity(String sql, SqlParameterSource namedParameters) {
        super(sql);
        this.namedParameters = namedParameters;
    }

    public UpdateEntity(String sql, T entity) {
        super(sql);
        this.namedParameters = new BeanPropertySqlParameterSource(entity);
    }

    @Override
    @Transactional
    public int update(NamedParameterJdbcTemplate jdbcTemplate) {
        return jdbcTemplate.update(sql, namedParameters);
    }
}
