package ua.com.foxminded.university.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import ua.com.foxminded.university.dao.query.Query;
import ua.com.foxminded.university.entity.*;
import javax.sql.DataSource;

@Configuration
public class DaoConfiguration {

    private final Environment environment;
    @Value("${ua.com.fox.university.trace-sql:false}")
    public Boolean traceSql;

    @Autowired
    public DaoConfiguration(Environment environment) {
        this.environment = environment;
    }

    @Bean
    public EntityRepository<Group> groupRepository(NamedParameterJdbcTemplate namedParameterJdbcTemplate,
                                                   GroupQueryFactory groupQueryFactory) {
        return new EntityRepositoryImpl<>(Group.class, namedParameterJdbcTemplate, groupQueryFactory);
    }

    @Bean
    public EntityRepository<Teacher> teacherRepository(NamedParameterJdbcTemplate namedParameterJdbcTemplate,
                                                       TeacherQueryFactory teacherQueryFactory) {
        return new EntityRepositoryImpl<>(Teacher.class, namedParameterJdbcTemplate, teacherQueryFactory);
    }

    @Bean
    public EntityRepository<Student> studentRepository(NamedParameterJdbcTemplate namedParameterJdbcTemplate,
                                                       StudentQueryFactory studentQueryFactory) {
        return new EntityRepositoryImpl<>(Student.class, namedParameterJdbcTemplate, studentQueryFactory);
    }

    @Bean
    @Primary
    public EntityRepository<Room> roomRepository(NamedParameterJdbcTemplate namedParameterJdbcTemplate,
                                                 RoomQueryFactory roomQueryFactory) {
        return new EntityRepositoryImpl<>(Room.class, namedParameterJdbcTemplate, roomQueryFactory);
    }

    @Bean
    public EntityRepository<Lecture> lectureRepository(NamedParameterJdbcTemplate namedParameterJdbcTemplate,
                                                       LectureQueryFactory roomQueryFactory) {
        return new EntityRepositoryImpl<>(Lecture.class, namedParameterJdbcTemplate, roomQueryFactory);
    }

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();

        dataSource.setDriverClassName(environment.getRequiredProperty("datasource.driver-name"));
        dataSource.setUrl(environment.getRequiredProperty("datasource.url")
                + environment.getRequiredProperty("datasource.parameters"));
        dataSource.setUsername(environment.getRequiredProperty("datasource.username"));
        dataSource.setPassword(environment.getRequiredProperty("datasource.password"));

        AbstractQueryFactory.setSchema(environment.getRequiredProperty("datasource.schema"));
        Query.setTraceSql(this.traceSql);

        return dataSource;
    }

    @Bean
    public JdbcTemplate jdbcTemplate() {
        return new JdbcTemplate(dataSource());
    }

    @Bean
    @Primary
    public NamedParameterJdbcTemplate namedParameterJdbcTemplate() {
        return new NamedParameterJdbcTemplate(dataSource());
    }
}
