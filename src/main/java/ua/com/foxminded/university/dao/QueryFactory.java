package ua.com.foxminded.university.dao;

import org.springframework.stereotype.Component;
import ua.com.foxminded.university.dao.query.Select;
import ua.com.foxminded.university.dao.query.SelectOne;
import ua.com.foxminded.university.entity.Entity;

@Component
public interface QueryFactory<T extends Entity<?>> {

    Select<T> selectAll();

    SelectOne<T> selectById(int id);
}