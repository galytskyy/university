package ua.com.foxminded.university.dao.mapper;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import ua.com.foxminded.university.entity.Group;
import ua.com.foxminded.university.entity.Lecture;
import ua.com.foxminded.university.entity.Room;
import ua.com.foxminded.university.entity.Teacher;
import java.sql.ResultSet;
import java.sql.SQLException;
import static java.util.Objects.isNull;

public class LectureRowMapper implements RowMapper<Lecture> {

    private final RowMapper<Lecture> lectureMapper = BeanPropertyRowMapper.newInstance(Lecture.class);
    private final RowMapper<Group> groupMapper = EntityRowMapper.newInstance(Group.class, Group::getGroupId);
    private final RowMapper<Teacher> teacherMapper = EntityRowMapper.newInstance(Teacher.class, Teacher::getTeacherId);
    private final RowMapper<Room> roomMapper = EntityRowMapper.newInstance(Room.class, Room::getRoomId);

    public static LectureRowMapper newInstance() {
        return new LectureRowMapper();
    }

    @Override
    public Lecture mapRow(ResultSet rs, int rowNum) throws SQLException {
        Lecture lecture = lectureMapper.mapRow(rs, rowNum);
        if (isNull(lecture) || isNull(lecture.getLectureId())) {
            return null;
        }
        lecture.setGroup(groupMapper.mapRow(rs, rowNum));
        lecture.setRoom(roomMapper.mapRow(rs, rowNum));
        lecture.setTeacher(teacherMapper.mapRow(rs, rowNum));
        return lecture;
    }
}
