package ua.com.foxminded.university.dao.dto;

import lombok.*;
import ua.com.foxminded.university.dao.query.QueryResults.Summary;

/**
 * set_$* - methods for JdbcTemplate RowMapper
 */
public interface DaoSummary extends Summary {

    static DaoSummary newInstance() {
        return new DaoSummaryImpl(0, 0, 0);
    }

    default void setTotal(Integer total) {
        this.getDaoSummary().setTotal(total);
    }

    default void setLimit(Integer limit) {
        this.getDaoSummary().setLimit(limit);
    }

    default void setOffset(Integer offset) {
        this.getDaoSummary().setOffset(offset);
    }

    /* This setter is required for RowMapper  */
    default void set_$total(Integer total) {
        this.setTotal(total);
    }

    /* This setter is required for RowMapper  */
    default void set_$Limit(Integer limit) {
        this.setLimit(limit);
    }

    /* This setter is required for RowMapper  */
    default void set_$Offset(Integer offset) {
        this.setOffset(offset);
    }

    DaoSummary getDaoSummary();

    @Override
    default Summary getSummary() {
        return this.getDaoSummary();
    }

    @Data
    @Setter
    @ToString
    @NoArgsConstructor(access = AccessLevel.PACKAGE)
    @AllArgsConstructor(access = AccessLevel.PACKAGE)
    class DaoSummaryImpl implements DaoSummary {

        Integer total;
        Integer limit;
        Integer offset;

        @Override
        public DaoSummary getDaoSummary() {
            return this;
        }
    }
}
