package ua.com.foxminded.university.dao;

import ua.com.foxminded.university.aop.AppException;

public class DaoException extends AppException {

    public DaoException(String message, Throwable cause) {
        super(message, cause);
    }
}
