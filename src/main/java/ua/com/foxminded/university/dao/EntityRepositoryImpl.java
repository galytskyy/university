package ua.com.foxminded.university.dao;

import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import ua.com.foxminded.university.dao.query.*;
import ua.com.foxminded.university.entity.Entity;
import java.util.Optional;
import java.util.function.IntSupplier;
import static java.lang.Integer.min;

@AllArgsConstructor(onConstructor = @__(@Autowired))
public class EntityRepositoryImpl<T extends Entity<?>> implements EntityRepository<T> {

    protected final Class<T> tClass;
    protected final NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    protected final AbstractQueryFactory<T> queryFactory;

    private QueryResult<T> select(Select<T> query) {
        return query.selectResult(namedParameterJdbcTemplate);
    }

    private Optional<T> select(SelectOne<T> query) {
        return query.selectForOptional(namedParameterJdbcTemplate);
    }

    private QueryResultOfSlice<T> selectSlice(SelectSliceByEntities<T> query) {
        return query.selectSlice(namedParameterJdbcTemplate);
    }

    private int insert(Insert<T> query) {
        return query.insert(namedParameterJdbcTemplate);
    }

    private int delete(Delete<T> query) {
        return query.delete(namedParameterJdbcTemplate);
    }

    private int update(Update<T> query) {
        return query.update(namedParameterJdbcTemplate);
    }

    @Override
    public QueryResult<T> findAll() {
        return select(queryFactory.selectAll());
    }

    @Override
    public QueryResult<T> findByQuery(@NonNull Select<T> query) {
        return select(query);
    }

    @Override
    public QueryResultOfSlice<T> findSlice(@NonNull SelectSliceByEntities<T> query) {
        return selectSlice(query);
    }

    @Override
    public Optional<T> findAny(@NonNull SelectOne<T> query) {
        return select(query);
    }

    @Override
    public Optional<T> findById(int id) {
        return select(queryFactory.selectById(id));
    }

    @Override
    public int count() {
        return queryFactory.count().selectForInteger(namedParameterJdbcTemplate.getJdbcTemplate());
    }

    @Override
    public int add(T entity) {
        return ifPersistenceThenGet(entity, () -> insert(queryFactory.insert(entity)));
    }

    @Override
    public int remove(T entity) {
        return ifPersistenceThenGet(entity, () -> delete(queryFactory.delete(entity)));
    }

    @Override
    public int replace(T entity) {
        return ifPersistenceThenGet(entity, () -> update(queryFactory.update(entity)));
    }

    private int ifPersistenceThenGet(@NonNull T entity, IntSupplier method) {
        return entity.nonPersistence() ? 0 : min(1, method.getAsInt());
    }

    @Override
    public String toString() {
        return "EntityRepositoryImpl<" + tClass.getSimpleName() + ">";
    }
}
