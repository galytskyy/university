package ua.com.foxminded.university.dao;

import lombok.extern.log4j.Log4j2;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import ua.com.foxminded.university.aop.AbstractAspect;

@Aspect
@Log4j2
public class EntityRepositoryAspect extends AbstractAspect {

    @Pointcut("execution(public * ua.com.foxminded.university.dao.EntityRepository.*(..))")
    public void anyPublicMethod() {
        /* @Pointcut */
    }

    @Around("EntityRepositoryAspect.anyPublicMethod()")
    public Object surroundAnyMethod(ProceedingJoinPoint joinPoint) {
        return surroundTryCatchAndLogging(joinPoint, log, DaoException::new);
    }
}
