package ua.com.foxminded.university.dao.query;

import lombok.NonNull;
import ua.com.foxminded.university.entity.Entity;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Stream;

public interface QueryResult<T extends Entity<?>> {

    static <A extends Entity<?>> QueryResult<A> newInstance(Supplier<Stream<A>> streamSupplier) {
        return new QueryResults.QueryResultImpl<>(streamSupplier);
    }

    <R, U, A> R collect(QueryResults.ResultCollector<T, U, A, R> collector);

    <R, A> R collect(@NonNull Collector<? super T, A, R> collector);

    Optional<T> findAny(@NonNull Predicate<T> predicate);
}
