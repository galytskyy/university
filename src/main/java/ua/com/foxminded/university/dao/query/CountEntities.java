package ua.com.foxminded.university.dao.query;

import lombok.ToString;
import org.springframework.jdbc.core.JdbcTemplate;
import ua.com.foxminded.university.entity.Entity;

@ToString(callSuper = true)
public class CountEntities<T extends Entity<?>> extends Query implements Count<T> {

    public CountEntities(String sql) {
        super(sql);
    }

    @Override
    public Integer selectForInteger(JdbcTemplate jdbcTemplate) {
        return jdbcTemplate.queryForObject(sql, Integer.class);
    }
}
