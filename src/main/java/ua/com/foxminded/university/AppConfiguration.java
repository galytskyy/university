package ua.com.foxminded.university;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ua.com.foxminded.university")
public class AppConfiguration {

}
