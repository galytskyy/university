package ua.com.foxminded.university.timetable;

import java.time.*;
import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNullElse;

public class DateTimeUtils {

    static final boolean FLOOR_SECONDS = true;
    static final LocalTime MAX = LocalTime.MAX;

    private DateTimeUtils() {
    }

    public static LocalDateTime floor(LocalDateTime dateTime) {
        return LocalDateTime.of(dateTime.toLocalDate(), floor(dateTime.toLocalTime()));
    }

    public static LocalTime floor(LocalTime time) {
        if (FLOOR_SECONDS) {
            return LocalTime.of(time.getHour(), time.getMinute());
        }
        return LocalTime.of(time.getHour(), time.getMinute(), time.getSecond());
    }

    public static LocalDateTime ceil(LocalDateTime dateTime) {
        return LocalDateTime.of(dateTime.toLocalDate(), ceil(dateTime.toLocalTime()));
    }

    public static LocalTime ceil(LocalTime time) {
        if (FLOOR_SECONDS) {
            return LocalTime.of(time.getHour(), time.getMinute(), MAX.getSecond(), MAX.getNano());
        }
        return LocalTime.of(time.getHour(), time.getMinute(), time.getSecond(), MAX.getNano());
    }

    public static Duration floor(Duration duration) {
        long seconds = duration.getSeconds();
        if (FLOOR_SECONDS) {
            seconds -= seconds % 60L;
        }
        return Duration.ofSeconds(seconds);
    }

    public static LocalTime calcBeginning(LocalTime beginning) {
        return floor(requireNonNullElse(beginning, LocalTime.MIN));
    }

    public static LocalTime calcEnding(LocalTime ending) {
        return ceil(requireNonNullElse(ending, LocalTime.MAX));
    }

    public static LocalTime calcEnding(LocalTime beginning, Duration duration) {
        duration = requireNonNullElseZero(duration);
        if (duration.isZero()) {
            return beginning;
        }
        return beginning.plus(floor(duration)).minusNanos(1);
    }

    public static LocalDate calcEnding(LocalDate beginning, Period period) {
        period = requireNonNullElseZero(period);
        if (period.isZero()) {
            return beginning;
        }
        return beginning.plus(period).minusDays(1);
    }

    public static Period requireNonNullElseZero(Period period) {
        if (isNull(period)) {
            return Period.ZERO;
        }
        return period;
    }

    public static Duration requireNonNullElseZero(Duration duration) {
        if (isNull(duration)) {
            return Duration.ZERO;
        }
        return duration;
    }

    public static Duration calcDuration(LocalTime beginning, LocalTime ending) {
        return Duration.between(beginning, ending).plusNanos(1);
    }

    public static Period calcPeriod(LocalDate beginning, LocalDate ending) {
        return Period.between(beginning, ending).plusDays(1);
    }

    public static LocalTime floorSeconds(LocalTime time) {
        return LocalTime.of(time.getHour(), time.getMinute());
    }
}
