package ua.com.foxminded.university.timetable;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import ua.com.foxminded.university.entity.*;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import static java.util.Objects.requireNonNullElseGet;
import static ua.com.foxminded.university.timetable.TypicalDateRange.TODAY;

@Value
@Builder(toBuilder = true)
public class Filter implements Predicate<Lecture>, HasDateRange, HasTimeRange, HasGroup, HasTeacher {

    @With
    DateRange dateRange;
    @With
    TimeRange timeRange;
    Group group;
    Teacher teacher;
    boolean allGroups;
    boolean allTeachers;
    boolean onlyDrafts;

    private Filter(DateRange dateRange, TimeRange timeRange, Group group, Teacher teacher,
                   boolean allGroups, boolean allTeachers, boolean onlyDrafts) {
        this.dateRange = requireNonNullElseGet(dateRange, TODAY::get);
        this.timeRange = requireNonNullElseGet(timeRange, TimeRange::allDay);
        this.group = group;
        this.teacher = teacher;
        this.allGroups = allGroups;
        this.allTeachers = allTeachers;
        this.onlyDrafts = onlyDrafts;
    }

    public static Filter newInstance() {
        return new Filter(null, null, null, null, true, true, false);
    }

    public boolean isAllDay() {
        return this.timeRange.isAllDay();
    }

    public Filter withGroup(Group group) {
        return new Filter(this.dateRange, this.timeRange, group, this.teacher, false, this.allTeachers, false);
    }

    public Filter withTeacher(Teacher teacher) {
        return new Filter(this.dateRange, this.timeRange, this.group, teacher, this.allGroups, false, false);
    }

    public Filter withAllTeachers() {
        return new Filter(this.dateRange, this.timeRange, this.group, this.teacher, this.allGroups, true, false);
    }

    public Filter withAllGroups() {
        return new Filter(this.dateRange, this.timeRange, this.group, this.teacher, true, this.allTeachers, false);
    }

    public Filter withOnlyDrafts() {
        return new Filter(this.dateRange, this.timeRange, this.group, this.teacher, this.allGroups, this.allTeachers, true);
    }

    @Override
    public boolean test(Lecture lecture) {
        Predicate<Lecture> checkDate = l -> this.dateRange.contains(l.getLectureDate());
        Predicate<Lecture> checkTime = l -> this.timeRange.contains(l.getBeginning());
        Predicate<Lecture> checkDraft = l -> this.onlyDrafts && l.isDraft();
        Predicate<Lecture> checkGroup = l -> this.allGroups || Objects.equals(l.getGroup(), this.getGroup());
        Predicate<Lecture> checkTeacher = l -> this.allTeachers || Objects.equals(l.getTeacher(), this.getTeacher());
        return checkDate.and(checkTime).and(checkDraft.or(checkGroup.and(checkTeacher))).test(lecture);
    }

    public static class FilterBuilder {

        FilterBuilder() {
            this.allGroups = true;
            this.allTeachers = true;
            this.onlyDrafts = false;
        }

        public FilterBuilder onlyDrafts() {
            return this.onlyDrafts(true);
        }

        private FilterBuilder onlyDrafts(boolean onlyDrafts) {
            this.onlyDrafts = onlyDrafts;
            return this;
        }

        public FilterBuilder group(Group group) {
            this.group = group;
            return this.onlyDrafts(false).allGroups(false);
        }

        public FilterBuilder teacher(Teacher teacher) {
            this.teacher = teacher;
            return this.onlyDrafts(false).allTeachers(false);
        }

        public FilterBuilder allGroups() {
            return this.onlyDrafts(false).allGroups(true);
        }

        private FilterBuilder allGroups(boolean allGroups) {
            this.allGroups = allGroups;
            return this.onlyDrafts(false);
        }

        public FilterBuilder allTeachers() {
            return this.onlyDrafts(false).allTeachers(true);
        }

        private FilterBuilder allTeachers(boolean allTeachers) {
            this.allTeachers = allTeachers;
            return this.onlyDrafts(false);
        }

        public FilterBuilder withIfTrue(@NonNull UnaryOperator<FilterBuilder> wither, boolean value) {
            return value ? wither.apply(this) : this;
        }
    }
}
