package ua.com.foxminded.university.timetable;

import java.time.LocalTime;

public interface HasTimeRange {

    TimeRange getTimeRange();

    default LocalTime getBeginningTime() {
        return getTimeRange().getBeginning();
    }

    default LocalTime getEndingTime() {
        return getTimeRange().getEnding();
    }
}
