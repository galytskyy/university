package ua.com.foxminded.university.timetable;

import java.time.LocalDate;
import java.time.YearMonth;

public enum TypicalDateRange {
    TODAY {
        @Override
        DateRange get() {
            LocalDate today = LocalDate.now();
            return DateRange.between(today, today);
        }
    },
    TOMORROW {
        @Override
        DateRange get() {
            LocalDate tomorrow = LocalDate.now().plusDays(1);
            return DateRange.between(tomorrow, tomorrow);
        }
    },
    AFTER_TOMORROW {
        @Override
        DateRange get() {
            LocalDate afterTomorrow = LocalDate.now().plusDays(2);
            return DateRange.between(afterTomorrow, afterTomorrow);
        }
    },
    CURRENT_WEEK {
        @Override
        DateRange get() {
            LocalDate firstDayOfCurrentWeek = LocalDate.now().with(DateRange.FIRST_DAY_OF_WEEK);
            return DateRange.between(firstDayOfCurrentWeek, firstDayOfCurrentWeek.plusDays(6));
        }
    },
    NEXT_WEEK {
        @Override
        DateRange get() {
            LocalDate firstDayOfNextWeek = LocalDate.now().with(DateRange.FIRST_DAY_OF_WEEK).plusDays(7);
            return DateRange.between(firstDayOfNextWeek, firstDayOfNextWeek.plusDays(6));
        }
    },
    CURRENT_MONTH {
        @Override
        DateRange get() {
            YearMonth currentMonth = YearMonth.now();
            return DateRange.between(currentMonth.atDay(1), currentMonth.atEndOfMonth());
        }
    },
    NEXT_MONTH {
        @Override
        DateRange get() {
            YearMonth nextMonth = YearMonth.now().plusMonths(1);
            return DateRange.between(nextMonth.atDay(1), nextMonth.atEndOfMonth());
        }
    };

    abstract DateRange get();
}
