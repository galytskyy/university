package ua.com.foxminded.university.timetable;

import java.time.LocalDate;
import java.time.YearMonth;

public enum DateRangeType {
    DAY {
        @Override
        DateRange getByDate(LocalDate dateFromRange) {
            return DateRange.between(dateFromRange, dateFromRange);
        }

        @Override
        DateRange getFromDate(LocalDate dateStartRange) {
            return DateRange.between(dateStartRange, dateStartRange);
        }
    },
    WEEK {
        @Override
        DateRange getByDate(LocalDate dateFromRange) {
            LocalDate firstDayOfWeek = dateFromRange.with(DateRange.FIRST_DAY_OF_WEEK);
            return DateRange.between(firstDayOfWeek, firstDayOfWeek.plusDays(6));
        }

        @Override
        DateRange getFromDate(LocalDate dateStartRange) {
            return DateRange.between(dateStartRange, dateStartRange.plusDays(6));
        }
    },
    MONTH {
        @Override
        DateRange getByDate(LocalDate dateFromRange) {
            YearMonth month = YearMonth.from(dateFromRange);
            return DateRange.between(month.atDay(1), month.atEndOfMonth());
        }

        @Override
        DateRange getFromDate(LocalDate dateStartRange) {
            return DateRange.between(dateStartRange, dateStartRange.plusMonths(1));
        }
    },
    EXTENDED_MONTH {
        @Override
        DateRange getByDate(LocalDate dateFromRange) {
            YearMonth month = YearMonth.from(dateFromRange);
            LocalDate firstDay = month.atDay(1).with(DateRange.FIRST_DAY_OF_WEEK);
            LocalDate lastDay = month.atEndOfMonth().with(DateRange.LAST_DAY_OF_WEEK);
            return DateRange.between(firstDay, lastDay);
        }

        @Override
        DateRange getFromDate(LocalDate dateStartRange) {
            LocalDate firstDay = dateStartRange.with(DateRange.FIRST_DAY_OF_WEEK);
            LocalDate lastDay = dateStartRange.plusMonths(1).with(DateRange.LAST_DAY_OF_WEEK);
            return DateRange.between(firstDay, lastDay);
        }
    };

    abstract DateRange getByDate(LocalDate dateFromRange);

    abstract DateRange getFromDate(LocalDate dateStartRange);
}
