package ua.com.foxminded.university.timetable;

import lombok.Value;
import java.time.Duration;
import java.time.LocalTime;
import static ua.com.foxminded.university.timetable.DateTimeUtils.*;

@Value
public class TimeRange {

    LocalTime beginning;
    LocalTime ending;

    private TimeRange(LocalTime beginning, LocalTime ending) {
        this.beginning = calcBeginning(beginning);
        this.ending = calcEnding(ending);
    }

    public static TimeRange between(LocalTime beginningInclusive, LocalTime endingInclusive) {
        return new TimeRange(beginningInclusive, endingInclusive);
    }

    public static TimeRange of(LocalTime beginningInclusive, Duration duration) {
        return between(beginningInclusive, beginningInclusive.plus(duration));
    }

    public static TimeRange allDay() {
        return between(LocalTime.MIN, LocalTime.MAX);
    }

    public static TimeRange after(LocalTime beginningInclusive) {
        return between(beginningInclusive, LocalTime.MAX);
    }

    public static TimeRange before(LocalTime endingExclusive) {
        return between(LocalTime.MIN, endingExclusive);
    }

    public boolean isAllDay() {
        return LocalTime.MIN.equals(beginning) && LocalTime.MAX.equals(ending);
    }

    public boolean contains(LocalTime time) {
        return time.compareTo(this.beginning) >= 0 && time.compareTo(this.ending) <= 0;
    }

    public Duration getDuration() {
        return calcDuration(beginning, ending);
    }

    public TimeRange withDuration(Duration duration) {
        LocalTime newEnding = calcEnding(this.beginning, duration);
        if (this.ending.equals(newEnding)) {
            return this;
        }
        return between(this.getBeginning(), newEnding);
    }

    public TimeRange beginningPlus(Duration amountToAdd) {
        amountToAdd = requireNonNullElseZero(amountToAdd);
        if (amountToAdd.isZero()) {
            return this;
        }
        return between(this.beginning.plus(amountToAdd), this.ending);
    }

    public TimeRange endingPlus(Duration amountToAdd) {
        amountToAdd = requireNonNullElseZero(amountToAdd);
        if (amountToAdd.isZero()) {
            return this;
        }
        return between(this.beginning, this.ending.plus(amountToAdd));
    }
}
