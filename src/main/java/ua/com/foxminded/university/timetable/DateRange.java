package ua.com.foxminded.university.timetable;

import lombok.Value;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Period;
import java.time.YearMonth;
import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNullElse;
import static ua.com.foxminded.university.timetable.DateTimeUtils.*;

@Value
public class DateRange {

    public static final DayOfWeek FIRST_DAY_OF_WEEK = DayOfWeek.MONDAY;
    public static final DayOfWeek LAST_DAY_OF_WEEK = DayOfWeek.SUNDAY;

    LocalDate beginning;
    LocalDate ending;

    private DateRange(LocalDate beginning, LocalDate ending) {
        this.beginning = requireNonNullElse(beginning, LocalDate.now());
        this.ending = requireNonNullElse(ending, LocalDate.now());
    }

    public static DateRange between(LocalDate beginningInclusive, LocalDate endingInclusive) {
        return new DateRange(beginningInclusive, endingInclusive);
    }

    public static DateRange ofType(DateRangeType type, LocalDate dateFromRange) {
        return type.getByDate(dateFromRange);
    }

    public static DateRange ofType(TypicalDateRange typical) {
        return typical.get();
    }

    public static DateRange of(LocalDate fromDate, DateRangeType type) {
        return type.getFromDate(fromDate);
    }

    public static DateRange of(LocalDate fromDate) {
        return of(fromDate, DateRangeType.DAY);
    }

    public static DateRange of(YearMonth yearMonth) {
        return between(yearMonth.atDay(1), yearMonth.atEndOfMonth());
    }

    public boolean contains(LocalDate date) {
        return date.compareTo(this.beginning) >= 0 && date.compareTo(this.ending) <= 0;
    }

    public Period getPeriod() {
        return calcPeriod(beginning, ending);
    }

    public DateRange withPeriod(Period period) {
        LocalDate newEnding = calcEnding(this.beginning, period);
        if (this.ending.equals(newEnding)) {
            return this;
        }
        return between(this.beginning, newEnding);
    }

    public DateRange beginningPlus(Period amountToAdd) {
        amountToAdd = requireNonNullElseZero(amountToAdd);
        if (amountToAdd.isZero()) {
            return this;
        }
        return between(this.beginning.plus(amountToAdd), this.ending);
    }

    public DateRange endingPlus(Period amountToAdd) {
        amountToAdd = requireNonNullElseZero(amountToAdd);
        if (amountToAdd.isZero()) {
            return this;
        }
        return between(this.beginning, this.ending.plus(amountToAdd));
    }

    public DateRange shift(Period amountToAdd) {
        amountToAdd = requireNonNullElseZero(amountToAdd);
        if (amountToAdd.isZero()) {
            return this;
        }
        return between(this.beginning.plus(amountToAdd), this.ending.plus(amountToAdd));
    }

    public DateRange shiftBeginningOn(LocalDate beginningOn) {
        if (isNull(beginningOn)) {
            return this;
        }
        return shift(Period.between(this.beginning, beginningOn));
    }

    public DateRange shiftEndingOn(LocalDate endingOn) {
        if (isNull(endingOn)) {
            return this;
        }
        return shift(Period.between(this.ending, endingOn));
    }
}
