package ua.com.foxminded.university.timetable;

import java.time.LocalDate;

public interface HasDateRange {

    DateRange getDateRange();

    default LocalDate getBeginningDate() {
        return getDateRange().getBeginning();
    }

    default LocalDate getEndingDate() {
        return getDateRange().getEnding();
    }
}
