package ua.com.foxminded.university.service.criteria;

import ua.com.foxminded.university.dao.RoomQueryFactory;
import ua.com.foxminded.university.dao.query.Select;
import ua.com.foxminded.university.dao.query.SelectOne;
import ua.com.foxminded.university.entity.Room;
import java.util.function.Predicate;
import static ua.com.foxminded.university.service.criteria.CriteriaUtils.createPatternMatcherLambda;

public class RoomCriteria {

    private static RoomQueryFactory roomQueryFactory;

    private RoomCriteria() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    public static Predicate<Room> byEqualName(String value) {
        return (room) -> room.getRoomName().equals(value);
    }

    public static Predicate<Room> byPatternOfFullName(String regex) {
        return createPatternMatcherLambda(regex, Room::getRoomName);
    }

    public static Select<Room> byLikeName(String value) {
        return roomQueryFactory.selectByLikeName(value);
    }

    public static SelectOne<Room> byId(int id) {
        return roomQueryFactory.selectById(id);
    }

    public static void setRoomQueryFactory(RoomQueryFactory roomQueryFactory) {
        RoomCriteria.roomQueryFactory = roomQueryFactory;
    }
}
