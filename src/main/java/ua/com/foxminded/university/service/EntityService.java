package ua.com.foxminded.university.service;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import ua.com.foxminded.university.dao.EntityRepository;
import ua.com.foxminded.university.dao.QueryFactory;
import ua.com.foxminded.university.dao.query.QueryResult;
import ua.com.foxminded.university.dao.query.QueryResults.ResultCollector;
import ua.com.foxminded.university.dao.query.Select;
import ua.com.foxminded.university.dao.query.SelectOne;
import ua.com.foxminded.university.entity.Entity;
import java.util.Optional;
import java.util.function.IntSupplier;
import java.util.function.Predicate;

@AllArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class EntityService<T extends Entity<?>> {

    protected final Class<T> tClass;
    protected EntityRepository<T> repository;
    protected QueryFactory<T> queryFactory;

    public abstract T create();

    private QueryResult<T> getAll() {
        return repository.findAll();
    }

    public <U, R, A> R getAll(@NonNull ResultCollector<T, U, A, R> collector) {
        return getAll().collect(collector);
    }

    public Optional<T> tryGetById(int id) {
        return repository.findById(id);
    }

    public Optional<T> tryGetAny(@NonNull SelectOne<T> query) {
        return repository.findAny(query);
    }

    public Optional<T> tryGetAny(@NonNull Predicate<T> predicate) {
        return getAll().findAny(predicate);
    }

    private QueryResult<T> getByQuery(@NonNull Select<T> query) {
        return repository.findByQuery(query);
    }

    public <U, R, A> R getByQuery(Select<T> query, @NonNull ResultCollector<T, U, A, R> collector) {
        return getByQuery(query).collect(collector);
    }

    private boolean add(@NonNull T entity) {
        return ifPersistenceThenGet(entity, () -> repository.add(entity));
    }

    private boolean update(@NonNull T entity) {
        return ifPersistenceThenGet(entity, () -> repository.replace(entity));
    }

    public boolean save(@NonNull T entity) {
        return (entity.getId() > 0) ? update(entity) : add(entity);
    }

    public boolean delete(@NonNull T entity) {
        return ifPersistenceThenGet(entity, () -> repository.remove(entity));
    }

    private boolean ifPersistenceThenGet(@NonNull T entity, IntSupplier method) {
        if (entity.nonPersistence()) {
            return false;
        }
        int result = method.getAsInt();
        return result > 0;
    }

    @Override
    public String toString() {
        return "EntityService<" + tClass.getSimpleName() + ">";
    }
}
