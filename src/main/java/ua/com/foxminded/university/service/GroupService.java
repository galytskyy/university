package ua.com.foxminded.university.service;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import ua.com.foxminded.university.dao.EntityRepository;
import ua.com.foxminded.university.dao.GroupQueryFactory;
import ua.com.foxminded.university.dao.GroupQueryFactory.StudentsSupplierWither;
import ua.com.foxminded.university.entity.Group;
import ua.com.foxminded.university.entity.Student;
import java.util.Optional;
import java.util.Set;

@Service
@Primary
public class GroupService extends EntityService<Group> {

    private static final Friend FRIEND = new Friend();
    private final GroupQueryFactory groupQueryFactory;

    @Autowired
    protected GroupService(EntityRepository<Group> groupRepository, GroupQueryFactory groupQueryFactory) {
        super(Group.class, groupRepository, groupQueryFactory);
        this.groupQueryFactory = groupQueryFactory;
    }

    @Override
    public Group create() {
        return Group.newInstance();
    }

    public Group getNullGroup() {
        return Group.nullGroup();
    }

    public Set<Student> putStudents(@NonNull Group group) {
        tryGetEagerGroup(group.getId())
                .ifPresent(g -> {
                    Set<Student> students = g.getStudents();
                    group.withStudents(FRIEND, () -> students);
                });
        return group.getStudents();
    }

    public Optional<Group> tryGetEagerGroup(int groupId) {
        StudentsSupplierWither studentsSupplierWither = (g, s) -> g.withStudents(FRIEND, s);
        return super.repository.findAny(groupQueryFactory.selectById(groupId, studentsSupplierWither));
    }

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static final class Friend {

    }
}
