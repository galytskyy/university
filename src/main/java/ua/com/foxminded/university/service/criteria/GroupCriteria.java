package ua.com.foxminded.university.service.criteria;

import ua.com.foxminded.university.dao.GroupQueryFactory;
import ua.com.foxminded.university.dao.query.Select;
import ua.com.foxminded.university.dao.query.SelectOne;
import ua.com.foxminded.university.entity.Group;
import java.util.function.Predicate;
import static ua.com.foxminded.university.service.criteria.CriteriaUtils.createPatternMatcherLambda;

public class GroupCriteria {

    private static GroupQueryFactory groupQueryFactory;

    private GroupCriteria() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    public static Predicate<Group> byEqualName(String value) {
        return (group) -> group.getGroupName().equals(value);
    }

    public static Predicate<Group> byPatternOfFullName(String regex) {
        return createPatternMatcherLambda(regex, Group::getGroupName);
    }

    public static Select<Group> byLikeName(String value) {
        return groupQueryFactory.selectByLikeName(value);
    }

    public static SelectOne<Group> byId(int value) {
        return groupQueryFactory.selectById(value);
    }

    public static void setGroupQueryFactory(GroupQueryFactory groupQueryFactory) {
        GroupCriteria.groupQueryFactory = groupQueryFactory;
    }
}
