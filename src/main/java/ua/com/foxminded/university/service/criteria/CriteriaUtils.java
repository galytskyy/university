package ua.com.foxminded.university.service.criteria;

import lombok.NonNull;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import static java.util.Objects.requireNonNull;
import static org.apache.commons.lang3.StringUtils.stripToNull;

public class CriteriaUtils {

    private CriteriaUtils() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    static <T> Predicate<T> createPatternMatcherLambda(String regex, @NonNull Function<T, String> stringFunction) {
        requireNonNull(stripToNull(regex), "Regex expresion is empty");
        Pattern pattern = Pattern.compile(regex);
        return (T o) -> pattern.matcher(stringFunction.apply(o)).find();
    }
}
