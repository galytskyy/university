package ua.com.foxminded.university.service;

import lombok.extern.log4j.Log4j2;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import ua.com.foxminded.university.aop.AbstractAspect;

@Aspect
@Log4j2
public class EntityServiceAspect extends AbstractAspect {

    @Pointcut("execution(public * ua.com.foxminded.university.service.*Service.*(..))")
    public void anyPublicMethod() {
        /* @Pointcut */
    }

    @Around("EntityServiceAspect.anyPublicMethod()")
    public Object surroundAnyMethod(ProceedingJoinPoint joinPoint) {
        return surroundTryCatchAndLogging(joinPoint, log, ServiceException::new);
    }
}
