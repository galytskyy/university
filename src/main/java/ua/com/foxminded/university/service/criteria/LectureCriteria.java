package ua.com.foxminded.university.service.criteria;

import ua.com.foxminded.university.dao.LectureQueryFactory;
import ua.com.foxminded.university.dao.LectureQueryFactory.LectureOrder;
import ua.com.foxminded.university.dao.query.Select;
import ua.com.foxminded.university.dao.query.SelectOne;
import ua.com.foxminded.university.dao.query.SelectSliceByEntities;
import ua.com.foxminded.university.entity.Lecture;
import ua.com.foxminded.university.timetable.Filter;
import java.util.Comparator;
import static java.util.Comparator.*;

public class LectureCriteria {

    private static LectureQueryFactory lectureQueryFactory;

    private LectureCriteria() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    public static Select<Lecture> byFilter(Filter filter) {
        return lectureQueryFactory.selectByFilter(filter);
    }

    public static LectureOrder newOrder() {
        return new LectureOrder();
    }

    public static SelectSliceByEntities<Lecture> slice(LectureOrder orderBy, int offset, int limit) {
        return lectureQueryFactory.selectSlice(orderBy, offset, limit);
    }

    public static SelectSliceByEntities<Lecture> sliceByFilter(Filter filter, LectureOrder orderBy, int offset, int limit) {
        return lectureQueryFactory.selectSliceByFilter(filter, orderBy, offset, limit);
    }

    public static SelectOne<Lecture> byId(int id) {
        return lectureQueryFactory.selectById(id);
    }

    public static Comparator<Lecture> byDateAndBeginning() {
        return nullsFirst(comparing(Lecture::getLectureDate, nullsFirst(naturalOrder()))
                .thenComparing(Lecture::getBeginning, nullsFirst(naturalOrder())));
    }

    public static void setLectureQueryFactory(LectureQueryFactory lectureQueryFactory) {
        LectureCriteria.lectureQueryFactory = lectureQueryFactory;
    }
}
