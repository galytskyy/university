package ua.com.foxminded.university.service.criteria;

import ua.com.foxminded.university.dao.TeacherQueryFactory;
import ua.com.foxminded.university.dao.query.Select;
import ua.com.foxminded.university.dao.query.SelectOne;
import ua.com.foxminded.university.entity.Teacher;
import java.util.function.Predicate;
import static ua.com.foxminded.university.service.criteria.CriteriaUtils.createPatternMatcherLambda;

public class TeacherCriteria {

    private static TeacherQueryFactory teacherQueryFactory;

    private TeacherCriteria() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    public static Predicate<Teacher> byEqualFullName(String value) {
        return (teacher) -> teacher.getFullName().equals(value);
    }

    public static Predicate<Teacher> byPatternOfFullName(String regex) {
        return createPatternMatcherLambda(regex, Teacher::getFullName);
    }

    public static Select<Teacher> byLikeFullName(String value) {
        return teacherQueryFactory.selectByLikeFullName(value);
    }

    public static SelectOne<Teacher> byId(int id) {
        return teacherQueryFactory.selectById(id);
    }

    public static void setTeacherQueryFactory(TeacherQueryFactory teacherQueryFactory) {
        TeacherCriteria.teacherQueryFactory = teacherQueryFactory;
    }
}
