package ua.com.foxminded.university.service;

import ua.com.foxminded.university.aop.AppException;

public class ServiceException extends AppException {

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
