package ua.com.foxminded.university.service;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ua.com.foxminded.university.dao.EntityRepository;
import ua.com.foxminded.university.dao.QueryFactory;
import ua.com.foxminded.university.entity.Room;
import ua.com.foxminded.university.entity.Student;
import ua.com.foxminded.university.entity.Teacher;

@Configuration
public class ServiceConfiguration {

    @Bean
    public EntityService<Room> roomService(EntityRepository<Room> roomRepository,
                                           QueryFactory<Room> roomQueryFactory) {
        return new EntityService<>(Room.class, roomRepository, roomQueryFactory) {
            @Override
            public Room create() {
                return Room.newInstance();
            }
        };
    }

    @Bean
    public EntityService<Student> studentService(EntityRepository<Student> studentRepository,
                                                 QueryFactory<Student> studentQueryFactory) {
        return new EntityService<>(Student.class, studentRepository, studentQueryFactory) {
            @Override
            public Student create() {
                return Student.newInstance();
            }
        };
    }

    @Bean
    public EntityService<Teacher> teacherService(EntityRepository<Teacher> teacherRepository,
                                                 QueryFactory<Teacher> teacherQueryFactory) {
        return new EntityService<>(Teacher.class, teacherRepository, teacherQueryFactory) {
            @Override
            public Teacher create() {
                return Teacher.newInstance();
            }
        };
    }
}
