package ua.com.foxminded.university.service.criteria;

import ua.com.foxminded.university.dao.StudentQueryFactory;
import ua.com.foxminded.university.dao.query.Select;
import ua.com.foxminded.university.dao.query.SelectOne;
import ua.com.foxminded.university.entity.Group;
import ua.com.foxminded.university.entity.Student;
import java.util.function.Predicate;
import static ua.com.foxminded.university.service.criteria.CriteriaUtils.createPatternMatcherLambda;

public class StudentCriteria {

    private static StudentQueryFactory studentQueryFactory;

    private StudentCriteria() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    public static Predicate<Student> byEqualFullName(String value) {
        return (student) -> student.getFullName().equals(value);
    }

    public static Predicate<Student> byPatternOfFullName(String regex) {
        return createPatternMatcherLambda(regex, Student::getFullName);
    }

    public static Select<Student> byLikeFullName(String value) {
        return studentQueryFactory.selectByLikeFullName(value);
    }

    public static SelectOne<Student> byId(int id) {
        return studentQueryFactory.selectById(id);
    }

    public static Select<Student> byGroup(Group group) {
        return studentQueryFactory.selectByGroupId(group.getGroupId());
    }

    public static void setStudentQueryFactory(StudentQueryFactory studentQueryFactory) {
        StudentCriteria.studentQueryFactory = studentQueryFactory;
    }
}
