package ua.com.foxminded.university.service.criteria;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.Value;
import ua.com.foxminded.university.dao.query.QueryResults.ResultCollector;
import ua.com.foxminded.university.entity.Entity;
import java.util.Comparator;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Collector;

public class ResultCollectors {

    private ResultCollectors() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    public static <T extends Entity<?>> ResultCollectorBuilder<T> filtering(Predicate<T> filterPredicate) {
        return new ResultCollectorBuilder<T>().filtering(filterPredicate);
    }

    public static <T extends Entity<?>> ResultCollectorBuilder<T> sorting(Comparator<T> orderByComparator) {
        return new ResultCollectorBuilder<T>().sorting(orderByComparator);
    }

    public static <T extends Entity<?>, U, A, R>
    ResultCollector<T, U, A, R> mapping(Function<T, U> mapper, Collector<? super U, A, R> collector) {
        return new ResultCollectorBuilder<T>().mapping(mapper, collector);
    }

    public static <T extends Entity<?>, A, R>
    ResultCollector<T, T, A, R> collecting(Collector<? super T, A, R> collector) {
        return new ResultCollectorBuilder<T>().collecting(collector);
    }

    @Value
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    public static class ResultCollectorBuilder<T extends Entity<?>> {

        @NonNull
        Predicate<T> filterPredicate;
        @NonNull
        Comparator<T> orderByComparator;

        private ResultCollectorBuilder() {
            this.filterPredicate = (e) -> true;
            this.orderByComparator = (a, b) -> 0;
        }

        public ResultCollectorBuilder<T> filtering(Predicate<T> filterPredicate) {
            return new ResultCollectorBuilder<>(filterPredicate, this.orderByComparator);
        }

        public ResultCollectorBuilder<T> sorting(Comparator<T> orderByComparator) {
            return new ResultCollectorBuilder<>(this.filterPredicate, orderByComparator);
        }

        public <U, A, R>
        ResultCollector<T, U, A, R> mapping(Function<T, U> mapper, Collector<? super U, A, R> collector) {
            return ResultCollector.of(this.filterPredicate, this.orderByComparator, mapper, collector);
        }

        public <A, R> ResultCollector<T, T, A, R> collecting(Collector<? super T, A, R> collector) {
            return ResultCollector.of(this.filterPredicate, this.orderByComparator, UnaryOperator.identity(), collector);
        }
    }
}
