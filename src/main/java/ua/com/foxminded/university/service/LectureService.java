package ua.com.foxminded.university.service;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import ua.com.foxminded.university.dao.EntityRepository;
import ua.com.foxminded.university.dao.LectureQueryFactory;
import ua.com.foxminded.university.dao.query.QueryResults.SliceResult;
import ua.com.foxminded.university.dao.query.SelectSliceByEntities;
import ua.com.foxminded.university.entity.Lecture;
import java.util.function.Function;

@Service
@Primary
public class LectureService extends EntityService<Lecture> {

    @Autowired
    public LectureService(EntityRepository<Lecture> lectureRepository, LectureQueryFactory lectureQueryFactory) {
        super(Lecture.class, lectureRepository, lectureQueryFactory);
    }

    @Override
    public Lecture create() {
        return Lecture.newInstance();
    }

    public <D> SliceResult<D> getSlice(@NonNull SelectSliceByEntities<Lecture> query,
                                       @NonNull Function<Lecture, D> mapper) {
        return super.repository.findSlice(query).collect(mapper);
    }

    public boolean replace(@NonNull Lecture target, final @NonNull Lecture replacement) {
        if (target.getId() <= 0) {
            return false;
        }
        Lecture entity = replacement.copy();
        entity.setId(target.getId());
        if (super.repository.replace(entity) > 0) {
            target.setId(-target.getId());
            replacement.setId(entity.getId());
            return true;
        }
        return false;
    }
}
