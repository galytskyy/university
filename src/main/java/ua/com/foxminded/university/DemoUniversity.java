package ua.com.foxminded.university;

import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.time.YearMonth;
import java.util.stream.Stream;
import static java.lang.String.format;
import static java.util.stream.Collectors.joining;

@ToString
@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class DemoUniversity {

    private final JdbcTemplate jdbcTemplate;
    @Value("${ua.com.fox.university.demo:false}")
    private Boolean demo;

    @PostConstruct
    void init() {
        if (Boolean.TRUE.equals(demo)) {
            final String INSERT = """
                    INSERT INTO university.students(student_id, first_name, surname)
                    values (10, 'Neville', 'Longbottom'),
                           (11, 'Parvati', 'Patil'),
                           (12, 'Dean', 'Thomas');
                    INSERT INTO university.student_group(student_id, group_id)
                    values (10, 1), (11, 1), (12, 1);
                    INSERT INTO university.rooms(room_id, room_name)
                    VALUES (10, 'Laboratory of Herbology'), 
                           (11, 'Classroom 2E'), 
                           (12, 'Studies classroom'), 
                           (13, 'Art Classroom');
                    INSERT INTO university.teachers(teacher_id, first_name, surname)
                    VALUES (10, 'Pomona', 'Sprout'), 
                           (11, 'Filius', 'Flitwick'), 
                           (12, 'Septima', 'Vector'), 
                           (13, 'Charity', 'Burbage');
                    INSERT INTO university.lectures(lecture_name, lecture_date, beginning, ending, teacher_id, group_id, room_id)
                    VALUES 
                    %s;
                    """;
            final String VALUES = """
                    ('Charms', '%tF', '15:00', '16:29:59', 11, 1, 11),
                    ('Arithmancy', '%tF', '08:30', '09:59:59', 12, 1, 12),
                    ('Herbology', '%tF', '08:30', '09:59:59', 10, 1, 10),
                    ('Muggle Art', '%tF', '08:30', '09:59:59', 13, 1, 13)
                    """;

            LocalDate date = YearMonth.now().minusMonths(1).atDay(1);
            LocalDate dateEnd = date.plusMonths(3);
            Stream.Builder<String> builder = Stream.builder();
            while (date.isBefore(dateEnd)) {
                builder.accept(format(VALUES, date, date.plusDays(2), date.plusDays(4), date.plusDays(6)));
                date = date.plusDays(5);
            }

            String sql = format(INSERT, builder.build().collect(joining(",\n")));
            this.jdbcTemplate.update(sql);
        }
    }
}
