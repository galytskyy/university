package ua.com.foxminded.university.controller.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class RestBadRequest extends RestControllerException implements HttpClientError {

    public RestBadRequest() {
        super(HttpStatus.BAD_REQUEST);
    }

    public RestBadRequest(String message) {
        super(HttpStatus.BAD_REQUEST, message);
    }
}
