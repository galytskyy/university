package ua.com.foxminded.university.controller.exception;

import lombok.EqualsAndHashCode;
import lombok.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import ua.com.foxminded.university.aop.AppException;

@Value
@EqualsAndHashCode(callSuper = true)
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class MvcBadRequest extends AppException implements HttpClientError, MvcHttpException {

    HttpStatus status = HttpStatus.BAD_REQUEST;

    public MvcBadRequest() {
        super(HttpStatus.BAD_REQUEST.getReasonPhrase());
    }

    public MvcBadRequest(String message) {
        super(message);
    }

    @Override
    public RestControllerException toRestHttpException() {
        return new RestBadRequest(getMessage());
    }
}
