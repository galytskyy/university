package ua.com.foxminded.university.controller.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class RestNotFound extends RestControllerException implements HttpClientError {

    public RestNotFound() {
        super(HttpStatus.NOT_FOUND);
    }

    public RestNotFound(String message) {
        super(HttpStatus.NOT_FOUND, message);
    }
}
