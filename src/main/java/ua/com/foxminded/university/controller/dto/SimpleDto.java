package ua.com.foxminded.university.controller.dto;

import lombok.*;
import ua.com.foxminded.university.entity.Entity;
import static java.util.Objects.isNull;
import static ua.com.foxminded.university.controller.TimetableController.createTimetableHref;

@Data
@Setter(AccessLevel.PRIVATE)
@AllArgsConstructor(staticName = "of", access = AccessLevel.PRIVATE)
@NoArgsConstructor(staticName = "of")
public class SimpleDto {

    private Integer id;
    private String name = "";
    private String calendarHref;

    public static <T extends Entity<?>> SimpleDto of(T entity) {
        if (isNull(entity)) {
            return of();
        }
        return of(entity.getId(), entity.getName(), createTimetableHref(entity));
    }

    public static <T extends Entity<?>> SimpleDto of(Integer id, String name) {
        return of(id, name, null);
    }
}
