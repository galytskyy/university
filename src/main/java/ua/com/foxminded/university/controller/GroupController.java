package ua.com.foxminded.university.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import ua.com.foxminded.university.annotation.VisibleForTesting;
import ua.com.foxminded.university.controller.dto.Page;
import ua.com.foxminded.university.controller.dto.SimpleDto;
import ua.com.foxminded.university.controller.exception.RestNotFound;
import ua.com.foxminded.university.dao.query.Select;
import ua.com.foxminded.university.entity.Group;
import ua.com.foxminded.university.entity.Student;
import ua.com.foxminded.university.service.GroupService;
import java.util.Collection;
import java.util.function.Function;
import static java.text.MessageFormat.format;
import static ua.com.foxminded.university.dao.query.QueryResults.toLinkedSet;
import static ua.com.foxminded.university.service.criteria.GroupCriteria.byLikeName;

@Controller
@RequestMapping("/groups")
public class GroupController extends SimpleEntityController<Group> {

    private static final String TEMPLATE_PATH = "groups/group";

    private final GroupService groupService;

    @Autowired
    public GroupController(GroupService groupService) {
        super(Properties.builder().templatePath(TEMPLATE_PATH).pluralName("groups").build(), groupService);
        this.groupService = groupService;
    }

    @GetMapping("/{groupId}/students")
    @ResponseBody
    public ResponseEntity<Page<SimpleDto>> getStudentsOfGroup(@PathVariable(value = "groupId", required = false) int groupId) {
        return ResponseEntity.ok(buildPage(groupId));
    }

    @VisibleForTesting
    protected Page<SimpleDto> buildPage(int groupId) {
        Page.PageBuilder<SimpleDto> pageBuilder = Page.<SimpleDto>builder().order("name", "asc");
        Function<Student, SimpleDto> mapper = (student) -> SimpleDto.of(student.getId(), student.getName());
        Collection<SimpleDto> rows = groupService.tryGetEagerGroup(groupId)
                .map((group) -> group.getStudents().stream().sorted().map(mapper).collect(toLinkedSet()))
                .orElseThrow(() -> new RestNotFound(format("Group with ID {0}", groupId)));
        return pageBuilder.rows(rows).size(rows.size()).total(rows.size()).build();
    }

    @Override
    protected Select<Group> getSelectBySearchParams(String likeName) {
        return byLikeName(likeName);
    }
}
