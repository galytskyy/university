package ua.com.foxminded.university.controller.exception;

import ua.com.foxminded.university.aop.AppException;

public class MvcControllerException extends AppException {

    public MvcControllerException(String message, Throwable cause) {
        super(message);
    }
}
