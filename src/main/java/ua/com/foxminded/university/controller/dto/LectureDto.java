package ua.com.foxminded.university.controller.dto;

import lombok.Value;
import ua.com.foxminded.university.controller.exception.MvcInternalServerErrorException;
import ua.com.foxminded.university.entity.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.function.Function;

@Value
public class LectureDto {

    Integer id;
    String name;
    LocalDate date;
    LocalTime beginning;
    LocalTime ending;
    /* Another is Teacher if Subject is Group and it is Group if Subject is Teacher */
    String another;
    String teacher;
    String group;
    String room;

    private LectureDto(Lecture lecture, String another) {
        this.id = lecture.getLectureId();
        this.name = lecture.getLectureName();
        this.date = lecture.getLectureDate();
        this.beginning = lecture.getBeginning();
        this.ending = lecture.getEnding();
        this.another = another;
        this.teacher = lecture.getTeacherName();
        this.group = lecture.getGroupName();
        this.room = lecture.getRoomName();
    }

    public static LectureDto of(Lecture lecture) {
        return new LectureDto(lecture, "");
    }

    public static <T extends Entity<?>> Function<Lecture, LectureDto> forSubject(T subject) {
        if (subject instanceof Teacher) {
            return (lecture) -> new LectureDto(lecture, lecture.getGroupName());
        } else if (subject instanceof Student) {
            return (lecture) -> new LectureDto(lecture, lecture.getTeacherName());
        } else if (subject instanceof Group) {
            return (lecture) -> new LectureDto(lecture, lecture.getTeacherName());
        }
        throw new MvcInternalServerErrorException("Unexpected argument " + subject);
    }
}
