package ua.com.foxminded.university.controller.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class RestInternalServerErrorException extends RestControllerException implements HttpInternalServerError {

    public RestInternalServerErrorException() {
        super(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public RestInternalServerErrorException(String message) {
        super(HttpStatus.INTERNAL_SERVER_ERROR, message);
    }

    public RestInternalServerErrorException(String message, Throwable e) {
        super(HttpStatus.INTERNAL_SERVER_ERROR, message);
    }
}
