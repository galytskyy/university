package ua.com.foxminded.university.controller.exception;

import lombok.EqualsAndHashCode;
import lombok.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import ua.com.foxminded.university.aop.AppException;

@Value
@EqualsAndHashCode(callSuper = true)
@ResponseStatus(HttpStatus.NOT_FOUND)
public class MvcNotFound extends AppException implements HttpClientError, MvcHttpException {

    HttpStatus status = HttpStatus.NOT_FOUND;

    public MvcNotFound() {
        super(HttpStatus.NOT_FOUND.getReasonPhrase());
    }

    public MvcNotFound(String message) {
        super(message);
    }

    @Override
    public RestControllerException toRestHttpException() {
        return new RestNotFound(getMessage());
    }
}
