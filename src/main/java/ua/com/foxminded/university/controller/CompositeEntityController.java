package ua.com.foxminded.university.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ua.com.foxminded.university.controller.dto.FormOptions;
import ua.com.foxminded.university.controller.exception.MvcInternalServerErrorException;
import ua.com.foxminded.university.controller.exception.MvcNotFound;
import ua.com.foxminded.university.entity.Entity;
import ua.com.foxminded.university.service.EntityService;
import java.util.Optional;
import static java.lang.String.format;

@AllArgsConstructor
public abstract class CompositeEntityController<T extends Entity<?>> {

    protected final String templatePath;
    protected final String pluralName;
    protected final EntityService<T> entityService;
    protected final ObjectMapper objectMapper;

    @GetMapping("/{id}")
    public String showOne(Model model, @PathVariable("id") int id) throws MvcNotFound {
        Optional<T> optional = entityService.tryGetById(id);
        if (optional.isPresent()) {
            T entity = optional.get();
            model.addAttribute("entity", entity);
            model.addAttribute("options", FormOptions.of(entity));
            model.addAttribute("id", id);
            return templatePath;
        }
        throw new MvcNotFound(format("Item with ID %s not found", id));
    }

    @GetMapping("/new")
    public String create(Model model,
                         @ModelAttribute("entity") T entity, BindingResult entityBinding,
                         @ModelAttribute("options") FormOptions options, BindingResult optionsBinding) {
        model.addAttribute("id", "new");
        return templatePath;
    }

    @PostMapping("/new")
    public String add(Model model,
                      @ModelAttribute("entity") T entity, BindingResult entityBinding,
                      @ModelAttribute("options") FormOptions options, BindingResult optionsBinding)
            throws MvcInternalServerErrorException {
        fillEntityFromOptions(entity, options);
        if (entity.getId() <= 0 && entityService.save(entity)) {
            return format("redirect:/%s/%s", pluralName, entity.getId());
        }
        throw new MvcInternalServerErrorException();
    }

    @PatchMapping("/{id}")
    public String update(Model model, @PathVariable("id") int id,
                         @ModelAttribute("entity") T entity, BindingResult entityBinding,
                         @ModelAttribute("options") FormOptions options, BindingResult optionsBinding)
            throws MvcInternalServerErrorException {
        fillEntityFromOptions(entity, options);
        if (id == entity.getId() && entityService.save(entity)) {
            model.addAttribute("id", id);
            options.fillFrom(entity);
            return templatePath;
        }
        throw new MvcInternalServerErrorException();
    }

    @DeleteMapping("/{id}")
    public String delete(Model model, @PathVariable("id") int id,
                         @ModelAttribute("entity") T entity, BindingResult entityBinding,
                         @ModelAttribute("options") FormOptions options, BindingResult optionsBinding)
            throws MvcInternalServerErrorException {
        fillEntityFromOptions(entity, options);
        if (id == entity.getId() && entityService.delete(entity)) {
            model.addAttribute("id", "deleted");
            options.fillFrom(entity);
            return templatePath;
        }
        throw new MvcInternalServerErrorException();
    }

    abstract protected void fillEntityFromOptions(T entity, FormOptions options);

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}
