package ua.com.foxminded.university.controller;

import lombok.RequiredArgsConstructor;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

@RequiredArgsConstructor(staticName = "of")
class RequestParams {

    private final Map<String, String> params;

    String getOrElse(String paramName, String orElse) {
        return tryGet(paramName, String::valueOf).orElse(orElse);
    }

    <T> T getOrElse(String paramName, FromString<T> fromString, T orElse) {
        return tryGet(paramName, fromString).orElse(orElse);
    }

    <T> Optional<T> tryGet(String paramName, FromString<T> fromString) {
        return Optional.ofNullable(params.get(paramName)).map(value -> {
            try {
                return fromString.apply(value);
            } catch (Exception e) {
                return null;
            }
        });
    }

    String get(String paramName) {
        return params.get(paramName);
    }

    interface FromString<T> extends Function<String, T> {

    }
}
