package ua.com.foxminded.university.controller;

import lombok.AllArgsConstructor;
import lombok.Builder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ua.com.foxminded.university.controller.dto.SimpleDto;
import ua.com.foxminded.university.controller.exception.MvcInternalServerErrorException;
import ua.com.foxminded.university.controller.exception.MvcNotFound;
import ua.com.foxminded.university.dao.query.Select;
import ua.com.foxminded.university.entity.Entity;
import ua.com.foxminded.university.service.EntityService;
import java.util.Collection;
import java.util.Comparator;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;
import static java.lang.String.format;
import static java.util.Comparator.naturalOrder;
import static org.apache.commons.lang3.StringUtils.capitalize;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static ua.com.foxminded.university.dao.query.QueryResults.toLinkedSet;
import static ua.com.foxminded.university.service.criteria.ResultCollectors.sorting;

@AllArgsConstructor
public abstract class SimpleEntityController<T extends Entity<?>> {

    private static final String TIMETABLE = "timetable/{0}/{1}";
    protected final Properties properties;
    protected final EntityService<T> entityService;

    @GetMapping
    @SuppressWarnings("unchecked")
    public String showAll(Model model,
                          @RequestParam Map<String, String> searchParams) {
        model.addAttribute("title", capitalize(properties.pluralName));
        model.addAttribute("pluralName", properties.pluralName);
        Supplier<Collection<SimpleDto>> supplier;
        Comparator<T> orderBy = (Comparator<T>) naturalOrder();
        String likeName = searchParams.get(properties.likeNameKey);
        if (isBlank(likeName)) {
            supplier = () -> entityService.getAll(sorting(orderBy).mapping(SimpleDto::of, toLinkedSet()));
        } else {
            supplier = () -> entityService.getByQuery(getSelectBySearchParams(likeName),
                    sorting(orderBy).mapping(SimpleDto::of, toLinkedSet()));
        }
        model.addAttribute("searchParams", searchParams);
        model.addAttribute("entities", supplier.get());
        return properties.templateOfCataloguePath;
    }

    protected abstract Select<T> getSelectBySearchParams(String likeName);

    @GetMapping("/{id}")
    public String showOne(Model model, @PathVariable("id") int id) throws MvcNotFound {
        Optional<T> optional = entityService.tryGetById(id);
        if (optional.isPresent()) {
            model.addAttribute("id", id);
            model.addAttribute("entity", optional.get());
            return properties.templatePath;
        }
        throw new MvcNotFound(format("Item with ID %s not found", id));
    }

    @GetMapping("/new")
    public String create(Model model) {
        model.addAttribute("id", "new");
        model.addAttribute("entity", entityService.create());
        return properties.templatePath;
    }

    @PostMapping("/new")
    public String add(Model model,
                      @ModelAttribute("entity") T entity) throws MvcInternalServerErrorException {
        if (entity.getId() <= 0 && entityService.save(entity)) {
            return format("redirect:/%s/%s", properties.pluralName, entity.getId());
        }
        throw new MvcInternalServerErrorException();
    }

    @PatchMapping("/{id}")
    public String update(Model model, @PathVariable("id") int id,
                         @ModelAttribute("entity") T entity) throws MvcInternalServerErrorException {
        if (id == entity.getId() && entityService.save(entity)) {
            model.addAttribute("id", id);
            return properties.templatePath;
        }
        throw new MvcInternalServerErrorException();
    }

    @DeleteMapping("/{id}")
    public String delete(Model model, @PathVariable("id") int id,
                         @ModelAttribute("entity") T entity) throws MvcInternalServerErrorException {
        if (id == entity.getId() && entityService.delete(entity)) {
            model.addAttribute("id", "deleted");
            return properties.templatePath;
        }
        throw new MvcInternalServerErrorException();
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }

    @Builder
    protected static class Properties {

        protected final String templateOfCataloguePath;
        protected final String templatePath;
        protected final String pluralName;
        protected final String likeNameKey;

        static class PropertiesBuilder {

            PropertiesBuilder() {
                this.likeNameKey = "likeName";
                this.templateOfCataloguePath = "entities/entities";
            }
        }
    }
}
