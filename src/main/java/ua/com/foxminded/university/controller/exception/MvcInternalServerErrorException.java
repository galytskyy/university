package ua.com.foxminded.university.controller.exception;

import lombok.EqualsAndHashCode;
import lombok.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import ua.com.foxminded.university.aop.AppException;

@Value
@EqualsAndHashCode(callSuper = true)
@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class MvcInternalServerErrorException extends AppException implements HttpInternalServerError {

    HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;

    public MvcInternalServerErrorException() {
        super("Internal server error");
    }

    public MvcInternalServerErrorException(String message) {
        super(message);
    }

    public MvcInternalServerErrorException(String message, Throwable cause) {
        super(message);
    }
}
