package ua.com.foxminded.university.controller;

import lombok.NonNull;
import lombok.Value;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.com.foxminded.university.controller.dto.FilterParams;
import ua.com.foxminded.university.controller.dto.FormOptions;
import ua.com.foxminded.university.controller.dto.SimpleDto;
import ua.com.foxminded.university.entity.*;
import ua.com.foxminded.university.service.EntityService;
import ua.com.foxminded.university.service.criteria.GroupCriteria;
import ua.com.foxminded.university.service.criteria.RoomCriteria;
import ua.com.foxminded.university.service.criteria.StudentCriteria;
import ua.com.foxminded.university.service.criteria.TeacherCriteria;
import javax.annotation.PostConstruct;
import java.util.*;
import java.util.function.Function;
import java.util.function.Supplier;
import static ua.com.foxminded.university.dao.query.QueryResults.toLinkedSet;
import static ua.com.foxminded.university.service.criteria.ResultCollectors.sorting;

@Component
public class EntitySuppliers {

    private final Map<Class<? extends Entity<?>>, EntitySupplier<? extends Entity<?>>>
            supplierMap = new HashMap<>();

    @Autowired
    public EntitySuppliers(EntityService<Group> groupService,
                           EntityService<Teacher> teacherService,
                           EntityService<Student> studentService,
                           EntityService<Room> roomService) {
        supplierMap.put(Group.class, new EntitySupplier<>(
                (id) -> groupService.tryGetAny(GroupCriteria.byId(id)),
                () -> groupService.getAll(sorting(Group::compareTo).mapping(SimpleDto::of, toLinkedSet()))));
        supplierMap.put(Room.class, new EntitySupplier<>(
                (id) -> roomService.tryGetAny(RoomCriteria.byId(id)),
                () -> roomService.getAll(sorting(Room::compareTo).mapping(SimpleDto::of, toLinkedSet()))));
        supplierMap.put(Student.class, new EntitySupplier<>(
                (id) -> studentService.tryGetAny(StudentCriteria.byId(id)),
                () -> studentService.getAll(sorting(Student::compareTo).mapping(SimpleDto::of, toLinkedSet()))));
        supplierMap.put(Teacher.class, new EntitySupplier<>(
                (id) -> teacherService.tryGetAny(TeacherCriteria.byId(id)),
                () -> teacherService.getAll(sorting(Teacher::compareTo).mapping(SimpleDto::of, toLinkedSet()))));
    }

    @PostConstruct
    void init() {
        FormOptions.setEntitySuppliers(this);
        FilterParams.setEntitySuppliers(this);
    }

    public <T extends Entity<?>> Collection<SimpleDto> getAll(@NonNull Class<T> tClass) {
        return getSupplier(tClass).getAll();
    }

    public <T extends Entity<?>> T getById(@NonNull Class<T> tClass, Integer id) {
        return tryGetById(tClass, id).orElse(null);
    }

    public <T extends Entity<?>> Optional<T> tryGetById(@NonNull Class<T> tClass, Integer id) {
        return getSupplier(tClass).getById(id);
    }

    @SuppressWarnings("unchecked")
    private <T extends Entity<?>> EntitySupplier<T> getSupplier(Class<T> tClass) {
        return (EntitySupplier<T>) supplierMap.getOrDefault(tClass,
                new EntitySupplier<>((id) -> Optional.empty(), Collections::emptyList));
    }

    @Value
    private static class EntitySupplier<T extends Entity<?>> {

        Function<Integer, Optional<T>> byId;
        Supplier<Collection<SimpleDto>> all;

        public Optional<T> getById(Integer id) {
            return Optional.ofNullable(id).map(byId).orElse(Optional.empty());
        }

        public Collection<SimpleDto> getAll() {
            return all.get();
        }
    }
}
