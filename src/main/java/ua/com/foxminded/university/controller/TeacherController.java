package ua.com.foxminded.university.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import ua.com.foxminded.university.dao.query.Select;
import ua.com.foxminded.university.entity.Teacher;
import ua.com.foxminded.university.service.EntityService;
import static ua.com.foxminded.university.service.criteria.TeacherCriteria.byLikeFullName;

@Controller
@RequestMapping("/teachers")
public class TeacherController extends SimpleEntityController<Teacher> {

    @Autowired
    public TeacherController(EntityService<Teacher> teacherService) {
        super(Properties.builder().templateOfCataloguePath("persons/persons").templatePath("teachers/teacher")
                        .pluralName("teachers").likeNameKey("likeFullName").build(),
                teacherService);
    }

    @Override
    protected Select<Teacher> getSelectBySearchParams(String likeFullName) {
        return byLikeFullName(likeFullName);
    }
}
