package ua.com.foxminded.university.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ua.com.foxminded.university.annotation.VisibleForTesting;
import ua.com.foxminded.university.controller.dto.FormOptions;
import ua.com.foxminded.university.controller.dto.Page;
import ua.com.foxminded.university.controller.dto.Page.PageBuilder;
import ua.com.foxminded.university.controller.dto.SimpleDto;
import ua.com.foxminded.university.entity.Group;
import ua.com.foxminded.university.entity.Student;
import ua.com.foxminded.university.service.EntityService;
import java.util.Collection;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;
import static org.apache.commons.lang3.StringUtils.capitalize;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static ua.com.foxminded.university.dao.query.QueryResults.toLinkedSet;
import static ua.com.foxminded.university.service.criteria.ResultCollectors.sorting;
import static ua.com.foxminded.university.service.criteria.StudentCriteria.byGroup;
import static ua.com.foxminded.university.service.criteria.StudentCriteria.byLikeFullName;

@Controller
@RequestMapping("/students")
public class StudentController extends CompositeEntityController<Student> {

    public StudentController(EntityService<Student> entityService, ObjectMapper objectMapper) {
        super("students/student", "students", entityService, objectMapper);
    }

    @GetMapping
    public String showAll(Model model,
                          @RequestParam Map<String, String> searchParams) {
        model.addAttribute("title", capitalize(pluralName));
        model.addAttribute("pluralName", pluralName);
        Supplier<Collection<SimpleDto>> supplier;
        String likeFullName = searchParams.get("likeFullName");
        var collector = sorting(Student::compareTo).mapping(SimpleDto::of, toLinkedSet());
        if (isBlank(likeFullName)) {
            supplier = () -> entityService.getAll(collector);
        } else {
            supplier = () -> entityService.getByQuery(byLikeFullName(likeFullName), collector);
        }
        model.addAttribute("searchParams", searchParams);
        model.addAttribute("entities", supplier.get());
        return "persons/persons";
    }

    @GetMapping("/groups/{groupId}")
    @ResponseBody
    public ResponseEntity<Page<SimpleDto>> getStudentsByGroupId(@PathVariable(value = "groupId", required = false) int groupId) {
        return ResponseEntity.ok(buildPage(groupId));
    }

    @SneakyThrows
    @VisibleForTesting
    protected Page<SimpleDto> buildPage(int groupId) {
        PageBuilder<SimpleDto> pageBuilder = Page.<SimpleDto>builder().order("name", "asc");
        Function<Student, SimpleDto> mapper = (s) -> SimpleDto.of(s.getId(), s.getName());
        Group group = super.objectMapper.convertValue(Map.of("groupId", groupId), Group.class);
        Collection<SimpleDto> rows = entityService.getByQuery(byGroup(group), sorting(Student::compareTo)
                .mapping(mapper, toLinkedSet()));
        return pageBuilder.rows(rows).size(rows.size()).total(rows.size()).build();
    }

    @Override
    protected void fillEntityFromOptions(Student student, FormOptions options) {
        options.tryGetGroup().ifPresent(student::setGroup);
    }
}
