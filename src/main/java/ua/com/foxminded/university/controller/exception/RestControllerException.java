package ua.com.foxminded.university.controller.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import ua.com.foxminded.university.aop.AppException;

public class RestControllerException extends AppException {

    @Getter
    private final HttpStatus status;

    public RestControllerException(HttpStatus status) {
        super(status.getReasonPhrase(), null, false, false);
        this.status = status;
    }

    public RestControllerException(HttpStatus status, String message) {
        super(message, null, false, false);
        this.status = status;
    }
}
