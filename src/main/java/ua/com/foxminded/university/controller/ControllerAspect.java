package ua.com.foxminded.university.controller;

import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import ua.com.foxminded.university.aop.AbstractAspect;
import ua.com.foxminded.university.aop.ExceptionConstructor;
import ua.com.foxminded.university.controller.exception.*;

@Aspect
@Log4j2
public class ControllerAspect extends AbstractAspect {

    @Pointcut("execution(public * ua.com.foxminded.university.controller.*Controller.*(..))")
    public void anyPublicMethod() {
        /* @Pointcut */
    }

    @Pointcut("execution(* ua.com.foxminded.university.controller.*Controller.*(..))")
    public void anyMethod() {
        /* @Pointcut */
    }

    @Pointcut("(@annotation(org.springframework.web.bind.annotation.GetMapping) " +
            "|| @annotation(org.springframework.web.bind.annotation.PostMapping)" +
            "|| @annotation(org.springframework.web.bind.annotation.PatchMapping)" +
            "|| @annotation(org.springframework.web.bind.annotation.PutMapping)" +
            "|| @annotation(org.springframework.web.bind.annotation.DeleteMapping)" +
            "|| @annotation(org.springframework.web.bind.annotation.RequestMapping))")
    public void isAnnotatedSomeMapping() {
        /* @Pointcut */
    }

    @Pointcut("!@annotation(org.springframework.web.bind.annotation.ResponseBody)")
    public void notAnnotatedResponseBody() {
        /* @Pointcut */
    }

    @Pointcut("@annotation(org.springframework.web.bind.annotation.ResponseBody)")
    public void isAnnotatedResponseBody() {
        /* @Pointcut */
    }

    @Before("ControllerAspect.anyPublicMethod() && ControllerAspect.isAnnotatedSomeMapping()")
    public void beforeAnyMethod(JoinPoint joinPoint) {
        logInvocation(joinPoint, log);
    }

    @AfterThrowing(pointcut = "ControllerAspect.anyPublicMethod() && ControllerAspect.isAnnotatedSomeMapping()" +
            "&& ControllerAspect.notAnnotatedResponseBody()", throwing = "e")
    public void afterAnyMethodThrowing(JoinPoint joinPoint, Throwable e) {
        String longMethodName = getLongNameOfMethod(joinPoint);
        if (e instanceof MvcHttpException && (e instanceof HttpClientError || e instanceof HttpInternalServerError)) {
            String invocationError = getInvocationErrorMessage(longMethodName, e);
            log.error(invocationError);
        } else {
            handleThrowable(longMethodName, e, MvcInternalServerErrorException::new);
        }
    }

    @AfterThrowing(pointcut = "ControllerAspect.anyPublicMethod() && ControllerAspect.isAnnotatedSomeMapping() " +
            "&& ControllerAspect.isAnnotatedResponseBody()", throwing = "e")
    public void afterAnyRestMethodThrowing(JoinPoint joinPoint, Throwable e) {
        String longMethodName = getLongNameOfMethod(joinPoint);
        if (e instanceof MvcHttpException) {
            String invocationError = getInvocationErrorMessage(longMethodName, e);
            log.error(invocationError);
            throw ((MvcHttpException) e).toRestHttpException();
        } else if (e instanceof HttpClientError || e instanceof HttpInternalServerError) {
            String invocationError = getInvocationErrorMessage(longMethodName, e);
            log.error(invocationError);
        } else {
            handleThrowable(longMethodName, e, RestInternalServerErrorException::new);
        }
    }

    @SneakyThrows
    private void handleThrowable(String longMethodName, Throwable e, ExceptionConstructor exceptionConstructor) {
        if (e instanceof RuntimeException) {
            String invocationError = getInvocationErrorMessage(longMethodName, e);
            log.error(invocationError, e);
        } else {
            String fatalError = getFatalErrorMessage(longMethodName, e);
            log.fatal(fatalError, e);
        }
        throw exceptionConstructor.apply("Internal server error", null);
    }
}
