package ua.com.foxminded.university.controller.dto;

import lombok.Data;
import ua.com.foxminded.university.controller.EntitySuppliers;
import ua.com.foxminded.university.entity.Group;
import ua.com.foxminded.university.entity.Teacher;
import ua.com.foxminded.university.timetable.DateRange;
import ua.com.foxminded.university.timetable.Filter;
import ua.com.foxminded.university.timetable.Filter.FilterBuilder;
import ua.com.foxminded.university.timetable.TimeRange;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Collection;
import static ua.com.foxminded.university.timetable.DateTimeUtils.floorSeconds;

@Data
public class FilterParams {

    private static EntitySuppliers entitySuppliers;

    private boolean filter = false;
    private LocalDate dateFrom;
    private LocalDate dateTo;
    private boolean allDay = false;
    private LocalTime timeFrom;
    private LocalTime timeTo;
    private boolean onlyDrafts = false;
    private boolean allGroups = true;
    private Integer groupId;
    private boolean allTeachers = true;
    private Integer teacherId;
    private Integer roomId;

    public static void setEntitySuppliers(EntitySuppliers entitySuppliers) {
        FilterParams.entitySuppliers = entitySuppliers;
    }

    public void fillFrom(Filter filter) {
        this.dateFrom = filter.getBeginningDate();
        this.dateTo = filter.getEndingDate();
        this.allDay = filter.isAllDay();
        this.timeFrom = floorSeconds(filter.getBeginningTime());
        this.timeTo = floorSeconds(filter.getEndingTime());
        this.onlyDrafts = filter.isOnlyDrafts();
        this.allGroups = filter.isAllGroups();
        this.groupId = filter.getGroupId();
        this.allTeachers = filter.isAllTeachers();
        this.teacherId = filter.getTeacherId();
    }

    public Filter toFilter() {
        if (filter) {
            FilterBuilder filterBuilder = Filter.builder()
                    .dateRange(DateRange.between(dateFrom, dateTo))
                    .timeRange(allDay ? TimeRange.allDay() : TimeRange.between(timeFrom, timeTo))
                    .group(entitySuppliers.getById(Group.class, groupId))
                    .teacher(entitySuppliers.getById(Teacher.class, teacherId))
                    .withIfTrue(FilterBuilder::allGroups, allGroups)
                    .withIfTrue(FilterBuilder::allTeachers, allTeachers)
                    .withIfTrue(FilterBuilder::onlyDrafts, onlyDrafts);
            return filterBuilder.build();
        }
        return Filter.newInstance();
    }

    public Collection<SimpleDto> getGroups() {
        return FilterParams.entitySuppliers.getAll(Group.class);
    }

    public Collection<SimpleDto> getTeachers() {
        return FilterParams.entitySuppliers.getAll(Teacher.class);
    }
}
