package ua.com.foxminded.university.controller.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

@RestControllerAdvice
public class RestControllerExceptionHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(RestBadRequest.class)
    public ResponseEntity<ErrorMessage> handleBadRequest(RestBadRequest e, WebRequest request) {
        return handleRestControllerException(e, request);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(RestNotFound.class)
    public ResponseEntity<ErrorMessage> handleNotFound(RestNotFound e, WebRequest request) {
        return handleRestControllerException(e, request);
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(RestInternalServerErrorException.class)
    public ResponseEntity<ErrorMessage> handleServerError(RestInternalServerErrorException e, WebRequest request) {
        return handleRestControllerException(e, request);
    }

    public ResponseEntity<ErrorMessage> handleRestControllerException(RestControllerException e, WebRequest request) {
        return ResponseEntity.status(e.getStatus()).body(ErrorMessage.builder().httpStatus(e.getStatus())
                .error(e.getStatus().getReasonPhrase()).message(e.getMessage()).servletPath(request).build());
    }
}
