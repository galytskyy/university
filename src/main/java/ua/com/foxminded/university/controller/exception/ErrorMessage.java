package ua.com.foxminded.university.controller.exception;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import java.time.LocalDateTime;

@Value
@Builder(access = AccessLevel.PACKAGE)
public class ErrorMessage {

    LocalDateTime timestamp = LocalDateTime.now();
    int status;
    @Builder.Default
    String error = "";
    @Builder.Default
    String trace = "";
    @Builder.Default
    String message = "";
    @Builder.Default
    String path = "";

    static class ErrorMessageBuilder {

        ErrorMessageBuilder httpStatus(HttpStatus httpStatus) {
            return this.status(httpStatus.value());
        }

        ErrorMessageBuilder servletPath(WebRequest request) {
            return this.path(((ServletWebRequest) request).getRequest().getServletPath());
        }
    }
}
