package ua.com.foxminded.university.controller;

import org.springframework.stereotype.Component;
import org.thymeleaf.context.IExpressionContext;
import org.thymeleaf.dialect.AbstractDialect;
import org.thymeleaf.dialect.IExpressionObjectDialect;
import org.thymeleaf.expression.IExpressionObjectFactory;
import java.util.Collections;
import java.util.Set;
import static java.lang.String.format;

@Component
public class AppDialect extends AbstractDialect implements IExpressionObjectDialect {

    public AppDialect() {
        super("apputils");
    }

    @Override
    public IExpressionObjectFactory getExpressionObjectFactory() {
        return new IExpressionObjectFactory() {

            @Override
            public Set<String> getAllExpressionObjectNames() {
                return Collections.singleton("apputils");
            }

            @Override
            public Object buildObject(IExpressionContext context, String expressionObjectName) {
                return new AppUtils();
            }

            @Override
            public boolean isCacheable(String expressionObjectName) {
                return true;
            }
        };
    }

    public static class AppUtils {

        public String entityFormAction(String entitiesHref, String id) {
            return format("/%s/%s", entitiesHref, id);
        }

        public String entityFormMethod(String id) {
            return switch (id) {
                case "deleted" -> "GET";
                case "new" -> "POST";
                default -> "PATH";
            };
        }
    }
}
