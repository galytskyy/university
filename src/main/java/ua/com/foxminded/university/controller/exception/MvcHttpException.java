package ua.com.foxminded.university.controller.exception;

public interface MvcHttpException {

    RestControllerException toRestHttpException();
}
