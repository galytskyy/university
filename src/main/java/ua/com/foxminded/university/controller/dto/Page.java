package ua.com.foxminded.university.controller.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Singular;
import lombok.Value;
import java.util.Collection;
import java.util.LinkedHashMap;
import static java.util.Collections.emptyList;
import static java.util.Objects.requireNonNullElse;

@Value
@Builder
public class Page<T> {

    int index;
    int size;
    int total;
    LinkedHashMap<String, String> order;
    @Singular
    Collection<T> rows;
    @Getter(lazy = true)
    int count = count();

    private int count() {
        return requireNonNullElse(rows, emptyList()).size();
    }

    public static class PageBuilder<T> {

        private final LinkedHashMap<String, String> order = new LinkedHashMap<>();

        public boolean isPossible() {
            return index >= 0 && size > 0;
        }

        public boolean isImpossible() {
            return !isPossible();
        }

        public PageBuilder<T> order(String key, String value) {
            this.order.put(key, value);
            return this;
        }
    }
}
