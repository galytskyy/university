package ua.com.foxminded.university.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import ua.com.foxminded.university.dao.query.Select;
import ua.com.foxminded.university.entity.Room;
import ua.com.foxminded.university.service.EntityService;
import static ua.com.foxminded.university.service.criteria.RoomCriteria.byLikeName;

@Controller
@RequestMapping("/rooms")
public class RoomController extends SimpleEntityController<Room> {

    @Autowired
    public RoomController(EntityService<Room> roomService) {
        super(Properties.builder().templatePath("rooms/room").pluralName("rooms").build(), roomService);
    }

    @Override
    protected Select<Room> getSelectBySearchParams(String likeName) {
        return byLikeName(likeName);
    }
}
