package ua.com.foxminded.university.controller;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ua.com.foxminded.university.annotation.VisibleForTesting;
import ua.com.foxminded.university.controller.dto.FilterParams;
import ua.com.foxminded.university.controller.dto.FormOptions;
import ua.com.foxminded.university.controller.dto.LectureDto;
import ua.com.foxminded.university.controller.dto.Page;
import ua.com.foxminded.university.controller.dto.Page.PageBuilder;
import ua.com.foxminded.university.controller.exception.MvcBadRequest;
import ua.com.foxminded.university.controller.exception.MvcInternalServerErrorException;
import ua.com.foxminded.university.controller.exception.MvcNotFound;
import ua.com.foxminded.university.dao.LectureQueryFactory.LectureOrder;
import ua.com.foxminded.university.dao.query.QueryResults.SliceResult;
import ua.com.foxminded.university.entity.Lecture;
import ua.com.foxminded.university.service.LectureService;
import ua.com.foxminded.university.timetable.Filter;
import java.util.Map;
import java.util.function.Function;
import static java.text.MessageFormat.format;
import static org.apache.commons.lang3.StringUtils.capitalize;
import static ua.com.foxminded.university.service.criteria.LectureCriteria.slice;
import static ua.com.foxminded.university.service.criteria.LectureCriteria.sliceByFilter;

@Controller
@RequestMapping("/lectures")
public class LectureController extends CompositeEntityController<Lecture> {

    protected static final int PER_PAGE = 15;

    private final LectureService lectureService;

    @Autowired
    public LectureController(LectureService lectureService, ObjectMapper objectMapper) {
        super("lectures/lecture", "lectures", lectureService, objectMapper);
        this.lectureService = lectureService;
    }

    @GetMapping
    public String showAll(Model model,
                          @ModelAttribute("filterParams") FilterParams filterParams,
                          @RequestParam Map<String, String> parameters)
            throws MvcNotFound, MvcInternalServerErrorException, JacksonException {
        Filter filter = filterParams.toFilter();
        filterParams.fillFrom(filter);
        model.addAttribute("parameters", parameters);
        model.addAttribute("data",
                super.objectMapper.writeValueAsString(buildPage(0, filterParams, parameters)));
        return "lectures/lectures";
    }

    @GetMapping("/pages/{index}")
    @ResponseBody
    public ResponseEntity<Page<LectureDto>> getPage(@PathVariable(value = "index", required = false) int index,
                                                    @RequestParam Map<String, String> parameters) {
        return ResponseEntity.ok(buildPage(index, parameters));
    }

    @SneakyThrows
    @VisibleForTesting
    protected Page<LectureDto> buildPage(int index, Map<String, String> parameters) {
        FilterParams filterParams = super.objectMapper.convertValue(parameters, FilterParams.class);
        return buildPage(index, filterParams, parameters);
    }

    @SneakyThrows
    private Page<LectureDto> buildPage(int index, FilterParams filterParams, Map<String, String> parameters) {
        int limit = PER_PAGE;
        int offset = index * PER_PAGE;
        PageBuilder<LectureDto> pageBuilder = Page.<LectureDto>builder().index(index).size(limit);
        SliceResult<LectureDto> sliceResult;
        if (pageBuilder.isImpossible()) {
            throw new MvcBadRequest(super.objectMapper.writeValueAsString(pageBuilder));
        } else {
            RequestParams params = RequestParams.of(parameters);
            LectureOrder orderBy = buildOrderByAndFillPage(params, pageBuilder);
            Filter filter;
            Function<Lecture, LectureDto> mapper = LectureDto::of;
            if (filterParams.isFilter()) {
                filter = filterParams.toFilter();
                sliceResult = lectureService.getSlice(sliceByFilter(filter, orderBy, offset, limit), mapper);
            } else {
                filter = Filter.newInstance();
                sliceResult = lectureService.getSlice(slice(orderBy, offset, limit), mapper);
            }
            filterParams.fillFrom(filter);
        }
        return pageBuilder.total(sliceResult.getTotal()).rows(sliceResult.getRows()).build();
    }

    private LectureOrder buildOrderByAndFillPage(RequestParams params, PageBuilder<LectureDto> pageBuilder) {
        Function<LectureOrder.Field, LectureOrder.Direction> getFromParamsAndPutToPage = (f) -> {
            String paramName = format("by{0}", capitalize(f.alias()));
            LectureOrder.Direction direction = LectureOrder.Direction.of(params.get(paramName));
            pageBuilder.order(paramName, direction.name().toLowerCase());
            return direction;
        };
        Map<LectureOrder.Field, LectureOrder.Direction> orderByMap = LectureOrder.mapForAll(getFromParamsAndPutToPage);

        LectureOrder.Field firstBy = LectureOrder.Field.of(params.get("firstBy"));
        pageBuilder.order("firstBy", firstBy.alias());

        return LectureOrder.create(firstBy, orderByMap);
    }

    @Override
    protected void fillEntityFromOptions(Lecture lecture, FormOptions options) {
        options.tryGetGroup().ifPresent(lecture::setGroup);
        options.tryGetTeacher().ifPresent(lecture::setTeacher);
        options.tryGetRoom().ifPresent(lecture::setRoom);
    }
}

