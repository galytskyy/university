package ua.com.foxminded.university.controller.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import ua.com.foxminded.university.controller.EntitySuppliers;
import ua.com.foxminded.university.entity.*;
import java.util.Collection;
import java.util.Optional;
import static java.util.Objects.nonNull;

@Data
@NoArgsConstructor(staticName = "of")
public class FormOptions {

    private static EntitySuppliers entitySuppliers;

    private Integer groupId;
    private Integer roomId;
    private Integer teacherId;

    public static FormOptions of(Entity<?> entity) {
        FormOptions formOptions = new FormOptions();
        formOptions.fillFrom(entity);
        return formOptions;
    }

    public static void setEntitySuppliers(EntitySuppliers entitySuppliers) {
        FormOptions.entitySuppliers = entitySuppliers;
    }

    public static Collection<SimpleDto> getGroups() {
        return FormOptions.entitySuppliers.getAll(Group.class);
    }

    public static Collection<SimpleDto> getRooms() {
        return FormOptions.entitySuppliers.getAll(Room.class);
    }

    public static Collection<SimpleDto> getStudents() {
        return FormOptions.entitySuppliers.getAll(Student.class);
    }

    public static Collection<SimpleDto> getTeachers() {
        return FormOptions.entitySuppliers.getAll(Teacher.class);
    }

    public void fillFrom(Entity<?> entity) {
        this.groupId = null;
        this.teacherId = null;
        this.roomId = null;
        if (nonNull(entity)) {
            if (entity instanceof HasGroup) {
                this.groupId = ((HasGroup) entity).getGroupId();
            }
            if (entity instanceof HasRoom) {
                this.roomId = ((HasRoom) entity).getRoomId();
            }
            if (entity instanceof HasTeacher) {
                this.teacherId = ((HasTeacher) entity).getTeacherId();
            }
        }
    }

    public Optional<Group> tryGetGroup() {
        return FormOptions.entitySuppliers.tryGetById(Group.class, this.groupId);
    }

    public Optional<Teacher> tryGetTeacher() {
        return FormOptions.entitySuppliers.tryGetById(Teacher.class, this.teacherId);
    }

    public Optional<Room> tryGetRoom() {
        return FormOptions.entitySuppliers.tryGetById(Room.class, this.roomId);
    }
}
