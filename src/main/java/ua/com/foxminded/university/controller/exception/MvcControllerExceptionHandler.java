package ua.com.foxminded.university.controller.exception;

import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class MvcControllerExceptionHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MvcBadRequest.class)
    public String handleBadRequest(Model model, MvcBadRequest e) {
        model.addAttribute("exception", e);
        return "error/400";
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(MvcNotFound.class)
    public String handleNotFound(Model model, MvcNotFound e) {
        model.addAttribute("exception", e);
        return "error/404";
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(MvcInternalServerErrorException.class)
    public String handleServerError(Model model, MvcInternalServerErrorException e) {
        model.addAttribute("exception", e);
        return "error/500";
    }
}
