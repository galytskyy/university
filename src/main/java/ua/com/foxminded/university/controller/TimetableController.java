package ua.com.foxminded.university.controller;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ua.com.foxminded.university.aop.AfterReturningDoLogging;
import ua.com.foxminded.university.controller.dto.LectureDto;
import ua.com.foxminded.university.controller.dto.TimetableDto;
import ua.com.foxminded.university.controller.exception.*;
import ua.com.foxminded.university.entity.*;
import ua.com.foxminded.university.service.EntityService;
import ua.com.foxminded.university.timetable.DateRange;
import ua.com.foxminded.university.timetable.DateRangeType;
import ua.com.foxminded.university.timetable.Filter;
import ua.com.foxminded.university.timetable.Filter.FilterBuilder;
import ua.com.foxminded.university.timetable.TimeRange;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;
import static java.lang.String.format;
import static java.util.stream.Collectors.groupingBy;
import static ua.com.foxminded.university.service.criteria.LectureCriteria.byDateAndBeginning;
import static ua.com.foxminded.university.service.criteria.LectureCriteria.byFilter;
import static ua.com.foxminded.university.service.criteria.ResultCollectors.sorting;

@Controller
@RequestMapping("/timetable")
@AllArgsConstructor(onConstructor = @__({@Autowired}))
public class TimetableController {

    private static final String TIMETABLE_PATH = "timetable/{0}/{1}";

    private final EntityService<Lecture> lectureService;
    private final EntitySuppliers entitySuppliers;
    private final ObjectMapper objectMapper;

    public static <T extends Entity<?>> String createTimetableHref(T entity) {
        if (entity instanceof Teacher) {
            return MessageFormat.format(TIMETABLE_PATH, "teachers", entity.getId());
        } else if (entity instanceof Group) {
            return MessageFormat.format(TIMETABLE_PATH, "groups", entity.getId());
        } else if (entity instanceof Student) {
            return MessageFormat.format(TIMETABLE_PATH, "students", entity.getId());
        }
        return "";
    }

    @GetMapping
    public String showMain(Model model) {
        model.addAttribute("teachers", entitySuppliers.getAll(Teacher.class));
        model.addAttribute("groups", entitySuppliers.getAll(Group.class));
        return "timetable/main";
    }

    @GetMapping("/{subjects}/{id}")
    public String showForSubject(Model model,
                                 @PathVariable("subjects") String subjects,
                                 @PathVariable("id") int id)
            throws MvcNotFound, MvcInternalServerErrorException, JacksonException {
        LocalDate date = LocalDate.now();
        ResponseData responseData = responseData(subjects, id, DateRange.ofType(DateRangeType.EXTENDED_MONTH, date));
        model.addAttribute("data", objectMapper.writeValueAsString(responseData
                .getDataOrElseThrow(MvcNotFound::new)));
        model.addAttribute("subtitle", responseData.getSubtitle());
        model.addAttribute("selectedDate", date);
        return "timetable/calendar";
    }

    @GetMapping("/{subjects}/{id}/extended-month")
    @ResponseBody
    @AfterReturningDoLogging
    public ResponseEntity<TimetableDto> getByMonthForSubject(@PathVariable("subjects") String subjects,
                                                             @PathVariable("id") int id,
                                                             @RequestParam Map<String, String> parameters)
            throws RestNotFound, RestInternalServerErrorException {
        RequestParams params = RequestParams.of(parameters);
        LocalDate date = params.tryGet("date", LocalDate::parse)
                .orElseThrow(() -> new RestBadRequest("Bad required parameter date=" + params.get("date")));
        TimeRange timeRange = params.tryGet("timeFrom", LocalTime::parse)
                .map(timeFrom -> params.tryGet("timeTo", LocalTime::parse)
                        .map(timeTo -> TimeRange.between(timeFrom, timeTo)).orElse(TimeRange.allDay()))
                .orElse(TimeRange.allDay());
        return ResponseEntity.ok(responseData(subjects, id,
                DateRange.ofType(DateRangeType.EXTENDED_MONTH, date), timeRange)
                .getDataOrElseThrow(RestNotFound::new));
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }

    private ResponseData responseData(String subjects, int id, DateRange dateRange) {
        return new ResponseData(subjects, id, dateRange, TimeRange.allDay());
    }

    private ResponseData responseData(String subjects, int id, DateRange dateRange, TimeRange timeRange) {
        return new ResponseData(subjects, id, dateRange, timeRange);
    }

    private class ResponseData {

        private final Supplier<Optional<TimetableDto>> dataSupplier;
        private final Supplier<String> subtitleSupplier;
        private final String exceptionMessage;

        public ResponseData(String subjects, int id, DateRange dateRange, TimeRange timeRange) {
            Optional<FilterBuilder> filterBuilder;
            Optional<? extends Entity<?>> subjectOptional;
            switch (subjects) {
                case "teachers" -> {
                    subjectOptional = entitySuppliers.tryGetById(Teacher.class, id);
                    filterBuilder = subjectOptional.map(t -> Filter.builder().teacher((Teacher) t));
                }
                case "students" -> {
                    subjectOptional = entitySuppliers.tryGetById(Student.class, id);
                    filterBuilder = subjectOptional.flatMap(s -> entitySuppliers
                            .tryGetById(Group.class, ((Student) s).getGroupId())).map(g -> Filter.builder().group(g));
                }
                case "groups" -> {
                    subjectOptional = entitySuppliers.tryGetById(Group.class, id);
                    filterBuilder = subjectOptional.map(g -> Filter.builder().group((Group) g));
                }
                default -> {
                    subjectOptional = Optional.empty();
                    filterBuilder = Optional.empty();
                }
            }
            this.exceptionMessage = format("Path /%s/%s not found", subjects, id);
            this.dataSupplier = () -> filterBuilder.map(fb -> getResponseData(subjectOptional.get(),
                    fb.dateRange(dateRange).timeRange(timeRange).build()));
            this.subtitleSupplier = () -> subjectOptional.map(s -> s.getClass().getSimpleName() + ": " + s.getName())
                    .orElse("");
        }

        public TimetableDto getDataOrElseThrow(Function<String, RuntimeException> exceptionConstructor) {
            return this.dataSupplier.get().orElseThrow(() -> exceptionConstructor.apply(this.exceptionMessage));
        }

        public String getSubtitle() {
            return this.subtitleSupplier.get();
        }

        private TimetableDto getResponseData(Entity<?> subject, Filter filter) {
            Function<Lecture, LectureDto> lectureToDto = LectureDto.forSubject(subject);
            return TimetableDto.of(subject, filter, lectureService.getByQuery(byFilter(filter),
                    sorting(byDateAndBeginning()).mapping(lectureToDto, groupingBy(LectureDto::getDate))));
        }
    }
}
