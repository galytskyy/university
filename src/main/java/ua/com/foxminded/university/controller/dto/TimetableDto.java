package ua.com.foxminded.university.controller.dto;

import lombok.Value;
import ua.com.foxminded.university.entity.Entity;
import ua.com.foxminded.university.timetable.DateRange;
import ua.com.foxminded.university.timetable.Filter;
import ua.com.foxminded.university.timetable.TimeRange;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

@Value
public class TimetableDto {

    String subject;
    DateRange dateRange;
    TimeRange timeRange;
    Map<LocalDate, List<LectureDto>> dataByDays;

    public static <T extends Entity<?>> TimetableDto of(T subject, Filter filter,
                                                        Map<LocalDate, List<LectureDto>> mapByDateOfLectureLists) {
        return new TimetableDto(subject.getName(), filter.getDateRange(), filter.getTimeRange(), mapByDateOfLectureLists);
    }
}
