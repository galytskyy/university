package ua.com.foxminded.university.entity;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import static java.util.Comparator.comparing;
import static java.util.Comparator.nullsFirst;
import static org.apache.commons.lang3.StringUtils.stripToEmpty;

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class Person extends AbstractEntity {

    protected String firstName = "";
    protected String surname = "";

    public String getFullName() {
        return firstName + " " + surname;
    }

    public String getName() {
        return this.getFullName();
    }

    public void setFirstName(String firstName) {
        this.firstName = stripToEmpty(firstName);
    }

    public void setSurname(String surname) {
        this.surname = stripToEmpty(surname);
    }

    protected int compareTo(Person other) {
        return nullsFirst(comparing(Person::getSurname).thenComparing(Person::getFirstName)).compare(this, other);
    }
}
