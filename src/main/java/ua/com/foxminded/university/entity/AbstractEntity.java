package ua.com.foxminded.university.entity;

import lombok.SneakyThrows;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;
import static java.lang.System.identityHashCode;
import static java.util.Objects.isNull;

public abstract class AbstractEntity {

    private static final Map<Class<? extends AbstractEntity>, Method> idGettersMap = new HashMap<>();
    private static final Map<Class<? extends AbstractEntity>, Method> idSettersMap = new HashMap<>();
    private final Method idGetter;
    private final Method idSetter;

    @SneakyThrows
    protected AbstractEntity() {
        try {
            Class<? extends AbstractEntity> aClass = this.getClass();
            this.idGetter = findGetter(aClass);
            this.idSetter = findSetter(aClass);
        } catch (NoSuchMethodException e) {
            throw new NoSuchMethodException("No such public getter and/or setter of Id: " + e.getMessage());
        }
    }

    private static Method findSetter(Class<? extends AbstractEntity> aClass) throws NoSuchMethodException {
        Method idSetter = idSettersMap.get(aClass);
        if (isNull(idSetter)) {
            String simpleName = aClass.getSimpleName();
            String idSetterName = "set" + simpleName + "Id";
            idSetter = aClass.getDeclaredMethod(idSetterName, Integer.class);
            if (idSetter.getModifiers() == Modifier.PUBLIC && idSetter.getReturnType().equals(void.class)) {
                idSettersMap.put(aClass, idSetter);
            } else {
                throw new NoSuchMethodException("No such public method " + idSetterName);
            }
        }
        return idSetter;
    }

    private static Method findGetter(Class<? extends AbstractEntity> aClass) throws NoSuchMethodException {
        Method idGetter = idGettersMap.get(aClass);
        if (isNull(idGetter)) {
            String simpleName = aClass.getSimpleName();
            String idGetterName = "get" + simpleName + "Id";
            idGetter = aClass.getDeclaredMethod(idGetterName);
            Class<?> returnType = idGetter.getReturnType();
            if (idGetter.getModifiers() == Modifier.PUBLIC && returnType.equals(Integer.class)) {
                idGettersMap.put(aClass, idGetter);
            } else {
                throw new NoSuchMethodException("No such public method " + idGetterName);
            }
        }
        return idGetter;
    }

    @SneakyThrows
    public int getId() {
        Integer id = (Integer) idGetter.invoke(this);
        return isNull(id) ? 0 : id;
    }

    @SneakyThrows
    public void setId(int id) {
        idSetter.invoke(this, id);
    }

    @Override
    public int hashCode() {
        return (this.getId() > 0) ? this.getId() : identityHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        var other = getClass().cast(obj);
        return (this.getId() > 0) && (this.getId() == other.getId());
    }
}
