package ua.com.foxminded.university.entity;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import static java.util.Comparator.comparing;
import static java.util.Comparator.nullsFirst;
import static java.util.Objects.*;
import static org.apache.commons.lang3.StringUtils.stripToEmpty;
import static ua.com.foxminded.university.timetable.DateTimeUtils.*;

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Builder(toBuilder = true)
public class Lecture extends AbstractEntity implements Entity<Lecture>, HasGroup, HasTeacher, HasRoom {

    protected Integer lectureId;
    protected String lectureName = "";
    protected LocalDate lectureDate;
    protected LocalTime beginning;
    protected LocalTime ending;
    protected Teacher teacher;
    protected Group group;
    protected Room room;

    protected Lecture() {
        LocalDateTime dateTime = LocalDateTime.now().plusHours(1);
        this.lectureDate = dateTime.toLocalDate();
        this.setBeginning(LocalTime.of(dateTime.getHour(), 0, 0));
        this.setEnding(getBeginning().plusHours(1).minusNanos(1));
    }

    protected Lecture(String lectureName, LocalDate lectureDate, LocalTime beginning, LocalTime ending,
                      Teacher teacher, Group group, Room room) {
        this.setLectureName(lectureName);
        this.setLectureDate(lectureDate);
        this.setBeginning(beginning);
        this.setEnding(ending);
        this.teacher = teacher;
        this.group = group;
        this.room = room;
    }

    public static Lecture newInstance() {
        return new Lecture();
    }

    public static Lecture newInstance(@NonNull Lecture lecture) {
        return lecture.copy();
    }

    public static LectureBuilder builder() {
        return new Lecture().toBuilder();
    }

    public static LectureBuilder builder(Lecture lecture) {
        return isNull(lecture) ? builder() : lecture.toBuilder();
    }

    void copyTo(Lecture lecture) {
        lecture.lectureName = this.lectureName;
        lecture.lectureDate = this.lectureDate;
        lecture.beginning = this.beginning;
        lecture.ending = this.ending;
        lecture.group = this.group;
        lecture.teacher = this.teacher;
        lecture.room = this.room;
    }

    public Lecture copy() {
        Lecture lecture = new Lecture();
        copyTo(lecture);
        return lecture;
    }

    public void setLectureName(String lectureName) {
        this.lectureName = stripToEmpty(lectureName);
    }

    @Override
    public String getName() {
        return this.getLectureName();
    }

    public void setLectureDate(LocalDate lectureDate) {
        this.lectureDate = requireNonNullElse(lectureDate, LocalDate.now());
    }

    public void setBeginning(LocalTime beginning) {
        this.beginning = calcBeginning(beginning);
    }

    public void setEnding(LocalTime ending) {
        this.ending = calcEnding(ending);
    }

    public Duration getDuration() {
        return calcDuration(beginning, ending);
    }

    public void setDuration(Duration duration) {
        this.setEnding(calcEnding(this.getBeginning(), duration));
    }

    public boolean isDraft() {
        return teacher == null || group == null || room == null;
    }

    @Override
    public int compareTo(Lecture other) {
        return nullsFirst(comparing(Lecture::getLectureName)).compare(this, other);
    }

    public static class LectureBuilder {

        /**
         * This method override public method by Lombok
         */
        private LectureBuilder lectureId(Integer id) {
            this.lectureId = id;
            return this;
        }

        public LectureBuilder shift(Duration amountToAdd) {
            if (nonNull(amountToAdd)) {
                if (nonNull(this.beginning)) {
                    this.beginning = this.beginning.plus(amountToAdd);
                }
                if (nonNull(this.ending)) {
                    this.ending = this.ending.plus(amountToAdd);
                }
            }
            return this;
        }

        public LectureBuilder duration(Duration duration) {
            if (nonNull(duration) && nonNull(this.beginning)) {
                this.ending = this.beginning.plus(duration);
            }
            return this;
        }

        public Lecture build() {
            return new Lecture(lectureName, lectureDate, beginning, ending, teacher, group, room);
        }
    }
}
