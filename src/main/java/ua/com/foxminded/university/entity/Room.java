package ua.com.foxminded.university.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import static java.util.Comparator.comparing;
import static java.util.Comparator.nullsFirst;
import static org.apache.commons.lang3.StringUtils.stripToEmpty;

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@NoArgsConstructor(staticName = "newInstance")
public class Room extends AbstractEntity implements Entity<Room> {

    private Integer roomId;
    private String roomName = "";

    public void setRoomName(String roomName) {
        this.roomName = stripToEmpty(roomName);
    }

    @Override
    public String getName() {
        return this.getRoomName();
    }

    @Override
    public int compareTo(Room other) {
        return nullsFirst(comparing(Room::getRoomName)).compare(this, other);
    }
}

