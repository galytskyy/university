package ua.com.foxminded.university.entity;

import lombok.*;
import ua.com.foxminded.university.service.GroupService;
import java.util.Collections;
import java.util.Set;
import java.util.function.Supplier;
import static java.util.Comparator.comparing;
import static java.util.Comparator.nullsFirst;
import static org.apache.commons.lang3.StringUtils.stripToEmpty;

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@NoArgsConstructor(staticName = "newInstance")
public class Group extends AbstractEntity implements Entity<Group> {

    private Integer groupId;
    private String groupName = "";
    @Setter(AccessLevel.PRIVATE)
    @Getter(AccessLevel.PRIVATE)
    private Supplier<Set<Student>> studentsSupplier = Collections::emptySet;

    public static Group nullGroup() {
        return NullGroup.getInstance();
    }

    public void setGroupName(String groupName) {
        this.groupName = stripToEmpty(groupName);
    }

    public Set<Student> getStudents() {
        return studentsSupplier.get();
    }

    public Group withStudents(GroupService.Friend friend, Supplier<Set<Student>> students) {
        this.studentsSupplier = students;
        return this;
    }

    @Override
    public String getName() {
        return this.getGroupName();
    }

    @Override
    public int compareTo(Group other) {
        return nullsFirst(comparing(Group::getGroupName)).compare(this, other);
    }

    @EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
    private static class NullGroup extends Group {

        private static final NullGroup SINGLETON = new NullGroup();

        private static Group getInstance() {
            return SINGLETON;
        }

        public Integer getNullGroupId() {
            return null;
        }

        public void setNullGroupId(Integer id) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void setGroupId(Integer groupId) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void setGroupName(String groupName) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean nonPersistence() {
            return true;
        }
    }
}
