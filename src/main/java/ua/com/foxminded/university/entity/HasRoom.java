package ua.com.foxminded.university.entity;

import static java.util.Objects.isNull;

public interface HasRoom {

    Room getRoom();

    default Integer getRoomId() {
        return isNull(this.getRoom()) ? null : this.getRoom().getRoomId();
    }

    default String getRoomName() {
        return isNull(this.getRoom()) ? "" : this.getRoom().getRoomName();
    }
}