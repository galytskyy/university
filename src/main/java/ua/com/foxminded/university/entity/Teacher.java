package ua.com.foxminded.university.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@NoArgsConstructor(staticName = "newInstance")
@ToString(callSuper = true)
public class Teacher extends Person implements Entity<Teacher> {

    private Integer teacherId;

    @Override
    public int compareTo(Teacher other) {
        return super.compareTo(other);
    }
}
