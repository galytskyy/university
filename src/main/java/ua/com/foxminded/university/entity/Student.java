package ua.com.foxminded.university.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import static java.util.Objects.requireNonNullElse;

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@NoArgsConstructor(staticName = "newInstance")
@ToString(callSuper = true)
public class Student extends Person implements Entity<Student>, HasGroup {

    private Integer studentId;
    private Group group = Group.nullGroup();

    public void setGroup(Group group) {
        this.group = requireNonNullElse(group, Group.nullGroup());
    }

    public Student withGroup(Group group) {
        this.setGroup(group);
        return this;
    }

    @Override
    public int compareTo(Student other) {
        return super.compareTo(other);
    }
}
