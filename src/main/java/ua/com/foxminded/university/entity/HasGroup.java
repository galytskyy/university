package ua.com.foxminded.university.entity;

import static java.util.Objects.isNull;

public interface HasGroup {

    Group getGroup();

    default Integer getGroupId() {
        return isNull(this.getGroup()) ? null : this.getGroup().getGroupId();
    }

    default String getGroupName() {
        return isNull(this.getGroup()) ? "" : this.getGroup().getGroupName();
    }
}