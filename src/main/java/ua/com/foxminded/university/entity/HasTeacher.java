package ua.com.foxminded.university.entity;

import static java.util.Objects.isNull;

public interface HasTeacher {

    Teacher getTeacher();

    default Integer getTeacherId() {
        return isNull(this.getTeacher()) ? null : this.getTeacher().getTeacherId();
    }

    default String getTeacherName() {
        return isNull(this.getTeacher()) ? "" : this.getTeacher().getFullName();
    }
}