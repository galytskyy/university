package ua.com.foxminded.university.entity;

public interface Entity<T extends AbstractEntity> extends Comparable<T> {

    int getId();

    void setId(int id);

    String getName();

    default boolean nonPersistence() {
        return false;
    }
}
