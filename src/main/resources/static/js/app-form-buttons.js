const input_method = document.querySelector("input[name='_method']");
if (input_method) {
    const submitSave = document.getElementById("submit-save");
    submitSave.addEventListener("click", () => {
        input_method.value = 'PATCH';
    });
    const submitDelete = document.getElementById("submit-delete");
    if (submitDelete) {
        submitDelete.addEventListener("click", () => {
            input_method.value = 'DELETE';
        });
    }
}