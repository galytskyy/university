Date.prototype.atDay = function (day) {
    return new Date(this.getFullYear(), this.getMonth(), day);
}

Date.prototype.getStartOfWeek = function () {
    let minusDay = this.getDay() == 0 ? 6 : this.getDay() - 1;
    return new Date(this.getFullYear(), this.getMonth(), this.getDate() - minusDay);
}

Date.prototype.getEndOfWeek = function () {
    let plusDay = this.getDay() == 0 ? 0 : 7 - this.getDay();
    return new Date(this.getFullYear(), this.getMonth(), this.getDate() + plusDay);
}

Date.prototype.getEndOfMonth = function () {
    return new Date(this.getFullYear(), this.getMonth() + 1, 0);
}

Date.prototype.plusDays = function (days) {
    return new Date(this.getFullYear(), this.getMonth(), this.getDate() + days);
}

Date.prototype.plusMonths = function (months) {
    let date = new Date(this.getFullYear(), this.getMonth() + months, this.getDate());
    if ((date.getFullYear() * 12 + date.getMonth()) != (this.getFullYear() * 12 + this.getMonth() + months)) {
        date.setDate(0);
    }
    return date;
}

Date.prototype.plusHours = function (hours) {
    let MINUTES = this.getMinutes();
    let HOURS = this.getHours() + hours;
    if (HOURS < 0) {
        HOURS = 0;
        MINUTES = 0;
    } else if (HOURS >= 24) {
        HOURS = 23;
        MINUTES = 59;
    }
    return new Date(this.getFullYear(), this.getMonth(), this.getDate(), HOURS, MINUTES);
}

Date.prototype.plusMinutes = function (minutes) {
    let MINUTES = this.getMinutes() + minutes;
    let HOURS = this.getHours();
    if (HOURS === 0) {
        MINUTES = Math.max(0, MINUTES);
    } else if (HOURS === 23) {
        MINUTES = Math.min(59, MINUTES);
    }
    return new Date(this.getFullYear(), this.getMonth(), this.getDate(), this.getHours(), MINUTES);
}

Date.prototype.getStartOfDay = function () {
    return new Date(this.getFullYear(), this.getMonth(), this.getDate());
}

Date.prototype.getStartOfHour = function () {
    return new Date(this.getFullYear(), this.getMonth(), this.getDate(), this.getHours());
}

Date.prototype.getStartOfMinute = function () {
    return new Date(this.getFullYear(), this.getMonth(), this.getDate(), this.getHours(), this.getMinutes());
}

Date.prototype.getEndOfDay = function () {
    return new Date(this.getFullYear(), this.getMonth(), this.getDate(), 23, 59);
}

Date.prototype.getWeek = function () {
    let startWeekOfYear = new Date(this.getFullYear(), 0, -7).getEndOfWeek();
    startWeekOfYear.setHours(this.getUTCHours() - startWeekOfYear.getUTCHours());
    return Math.ceil(Math.ceil((this - startWeekOfYear) / 86_400_000) / 7);
}

Date.prototype.toShortString = function () {
    return "" + this.getFullYear() +
            "-" + (this.getMonth() + 1).toString().padStart(2, '0') +
            "-" + this.getDate().toString().padStart(2, '0');
    return this.toISOString().substring(0, 10); // It doesn't work
}

Date.prototype.toLongString = function () {
    let options = {year: 'numeric', month: 'long', day: '2-digit'};
    return new Intl.DateTimeFormat('en-US', options).format(this);
}

Date.prototype.toShortTimeString = function () {
    return "" + this.getHours().toString().padStart(2, '0') +
            ":" + this.getMinutes().toString().padStart(2, '0');
    return this.toTimeString().substring(0, 5);
}

Date.prototype.toWeekString = function () {
    let options = {year: 'numeric', month: 'short'};
    let formatter = new Intl.DateTimeFormat('en-US', options);
    let startOfWeek = this.getStartOfWeek();
    let endOfWeek = this.getEndOfWeek();
    let weekString = formatter.format(endOfWeek);
    if (startOfWeek.getFullYear() != endOfWeek.getFullYear()) {
        return formatter.format(startOfWeek).concat(" - " + weekString);
    }
    if (startOfWeek.getMonth() != endOfWeek.getMonth()) {
        options = {month: 'short'};
        formatter = new Intl.DateTimeFormat('en-US', options);
        return formatter.format(startOfWeek).concat(" - " + weekString);
    }
    return weekString;
}

Date.prototype.toMonthLongString = function () {
    let options = {year: 'numeric', month: 'long'};
    return new Intl.DateTimeFormat('en-US', options).format(this);
}

Date.prototype.getDayName = function () {
    let options = {weekday: 'long'};
    return new Intl.DateTimeFormat('en-US', options).format(this);
}