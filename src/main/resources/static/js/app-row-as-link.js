document.addEventListener('DOMContentLoaded', () => {
    const style = document.createElement('style');
    style.textContent = "tr[data-app-href] { cursor: pointer; }";
    document.head.appendChild(style);

    makeClickableRows();

    function makeClickableRows() {
        const rows = document.querySelectorAll("tr[data-app-href]");
        if (rows.length > 0) {
            rows.forEach(row => {
                row.addEventListener("click", () => {
                    window.location.href = row.dataset.appHref;
                });
            });
        }
    }
});
