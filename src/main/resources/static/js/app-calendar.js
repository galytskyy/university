/**
 * For this script, you need to download the date-utils.js script
 * e.g. <script src="./js/date-utils.js"></script>
 */
document.addEventListener('DOMContentLoaded', () => {

    const scaleMarginFirst = 2;
    const scaleMarginStep = 2.5;

    const controlPanel = getControlPanel();
    const documentData = getDocumentData();
    const renderOptions = {};
    onLoadContent();

    function onLoadContent() {
        onChangePeriod();
        renderMonth();
        let jsonData = document.querySelector("[data-app-response-data]");
        try {
            documentData.timetableData = JSON.parse(jsonData.dataset.appResponseData);
            renderMonthData();
        } catch (exception) {
            fetchAndRenderData();
        } finally {
            if (jsonData) {
                jsonData.remove();
            }
        }
    }

    function onChangeSelectedDate() {
        let selectedDate = new Date(controlPanel.selectedDate.value);
        if (selectedDate != renderOptions.selectedDate) {
            if (selectedDate < renderOptions.monthStart || renderOptions.monthEnd < selectedDate) {
                updateRenderOptions(renderOptions);
                renderMonth();
                fetchAndRenderData();
            } else if (selectedDate < renderOptions.weekRenderStart || renderOptions.weekRenderEnd < selectedDate) {
                updateRenderOptions(renderOptions);
                renderWeek();
                renderWeekData();
            } else if (selectedDate != renderOptions.selectedDate) {
                updateRenderOptions(renderOptions);
                renderDaily();
                renderDailyData();
            }
        }
        changeControlInfo();
    }

    function onShiftSelectedDate(shift) {
        let date = new Date(controlPanel.selectedDate.value);
        if (renderOptions.period == "Month") {
            date = date.plusMonths(shift);
        } else if (renderOptions.period == "Week") {
            date = date.plusDays(shift * 7);
        } else if (renderOptions.period == "Day") {
            date = date.plusDays(shift);
        }
        controlPanel.selectedDate.value = date.toShortString();
        onChangeSelectedDate();
    }

    function onMouseButtonToday() {
        controlPanel.selectedDate.value = new Date().toShortString();
        onChangeSelectedDate();
    }

    function onMouseButtonBack() {
        onShiftSelectedDate(-1);
    }

    function onMouseButtonForward() {
        onShiftSelectedDate(1);
    }

    function onMouseSelectPeriod() {
        let value = this.dataset.appValue;
        if (value != controlPanel.buttonPeriod.dataset.appValue) {
            controlPanel.buttonPeriod.dataset.appValue = value;
            controlPanel.buttonPeriod.innerText = value;
            onChangePeriod();
        }
    }

    function onMouseDay() {
        controlPanel.selectedDate.value = this.dataset.appDate;
        onChangeSelectedDate();
        controlPanel.buttonPeriod.dataset.appValue = 'Day';
        controlPanel.buttonPeriod.innerText = 'Day';
        onChangePeriod();
    }

    function onMouseWeekNumber() {
        controlPanel.selectedDate.value = this.dataset.appDate;
        onChangeSelectedDate();
        controlPanel.buttonPeriod.dataset.appValue = 'Week';
        controlPanel.buttonPeriod.innerText = 'Week';
        onChangePeriod();
    }

    function changeControlInfo() {
        if (renderOptions.period == "Month") {
            controlPanel.controlInfo.innerText = renderOptions.selectedDate.toMonthLongString();
        } else if (renderOptions.period == "Week") {
            controlPanel.controlInfo.innerText = renderOptions.selectedDate.toWeekString();
        } else if (renderOptions.period == "Day") {
            controlPanel.controlInfo.innerText = renderOptions.selectedDate.toLongString();
        }
    }

    function onChangePeriod() {
        updateRenderOptions(renderOptions);
        changeControlInfo();
        if (renderOptions.period == "Month") {
            documentData.weekHeaderDiv.classList.remove("display-none");
            documentData.monthDiv.classList.remove("display-none");
            documentData.weekDiv.classList.add("display-none");
            documentData.dailyDiv.classList.add("display-none");
        } else if (renderOptions.period == "Week") {
            documentData.weekHeaderDiv.classList.remove("display-none");
            documentData.monthDiv.classList.add("display-none");
            documentData.weekDiv.classList.remove("display-none");
            documentData.dailyDiv.classList.add("display-none");
        } else if (renderOptions.period == "Day") {
            documentData.weekHeaderDiv.classList.add("display-none");
            documentData.monthDiv.classList.add("display-none");
            documentData.weekDiv.classList.add("display-none");
            documentData.dailyDiv.classList.remove("display-none");
        }
    }

    function fetchAndRenderData() {
        fetchData().then((responseData) => {
            documentData.timetableData = responseData;
            renderMonthData();
        });
    }

    async function fetchData() {
        let url = new URL(window.location.href);
        url.search = "";
        url.pathname += '/extended-month';
        url.searchParams.set('date', renderOptions.selectedDate.toShortString());

        const response = await fetch(url, {
            method: 'GET', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json'
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *client
            //body: JSON.stringify({}) // body data type must match "Content-Type" header
        });
        return await response.json();

        let xhr = new XMLHttpRequest();
        xhr.responseType = 'json';
        xhr.open("GET", url, true);
        xhr.setRequestHeader('Content-Type', 'application/json');

        xhr.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                let responseData = this.response;
                for (let key in timetableData) {
                    delete timetableData[key];
                }
                for (let key in responseData) {
                    timetableData[key] = responseData[key];
                }
                renderMonthData();
            }
        };
        xhr.send();
    }

    function renderMonthData() {
        for (let key in documentData.daysOfMonthMap) {
            let dayBlock = documentData.daysOfMonthMap[key];
            let dayData = documentData.timetableData.dataByDays[key];
            renderDayData(dayBlock, dayData);
        }
        renderWeekData();
    }

    function renderWeekData() {
        for (let key in documentData.daysOfWeekMap) {
            let dayBlock = documentData.daysOfWeekMap[key];
            let dayData = documentData.timetableData.dataByDays[key];
            renderDayData(dayBlock, dayData);
        }
        renderDailyData();
    }

    function renderDailyData() {
        let key = renderOptions.selectedDate.toShortString();
        let dayData = documentData.timetableData.dataByDays[key];
        let dailyDayColumns = documentData.dailyDayColumns;
        if (dayData) {
            for (let j = 0; j < dayData.length; j++) {
                let lectureParts = lectureToElementArray(dayData[j], j);
                for (let i = 0; i < lectureParts.length && i < dailyDayColumns.length; i++) {
                    dailyDayColumns[i].appendChild(lectureParts[i]);
                }
            }
        }
    }

    function renderDayData(dayBlock, dayData) {
        dayBlock.innerText = dayBlock.dataset.appText;
        if (dayData) {
            dayData.forEach((lecture) => {
                dayBlock.appendChild(createElementWithLecture(lecture));
            });
        }
    }

    function renderMonth() {
        documentData.daysOfMonthMap = {};
        let dayCounter = renderOptions.monthRenderStart;
        documentData.dayOfMonthDivs.forEach(day => {
            let dateAsStr = dayCounter.toShortString();
            day.dataset.appDate = dateAsStr;
            day.dataset.appText = dayCounter.getDate();
            day.innerText = day.dataset.appText;
            documentData.daysOfMonthMap[dateAsStr] = day;
            dayCounter = dayCounter.plusDays(1);
        });
        let numberOfWeeks = renderOptions.monthEndWeek - renderOptions.monthStartWeek + 1;
        documentData.lastWeekDivs.forEach(div => {
            if (numberOfWeeks == 5) {
                div.classList.add('display-none');
            } else {
                div.classList.remove('display-none');
            }
        });
        let weekCounter = renderOptions.monthStartWeek;
        dayCounter = renderOptions.monthRenderStart;
        for (let i = 0; i < 6; i++) {
            documentData.weekNumbers[i].replaceChildren(createElementWithInnerText('span', weekCounter));
            documentData.weekNumbers[i].dataset.appDate = dayCounter.toShortString();
            weekCounter++;
            dayCounter = dayCounter.plusDays(7);
        }
        renderWeek();
    }

    function renderWeek() {
        documentData.daysOfWeekMap = {};
        let dayCounter = renderOptions.weekRenderStart;
        documentData.dayOfWeekDivs.forEach(day => {
            let dateAsStr = dayCounter.toShortString();
            day.dataset.appDate = dateAsStr;
            day.dataset.appText = dayCounter.getDate();
            day.innerText = day.dataset.appText;
            day.replaceChildren(createElementWithInnerHTML('p', day.dataset.appText));
            documentData.daysOfWeekMap[dateAsStr] = day;
            dayCounter = dayCounter.plusDays(1);
        });
        renderDaily();
    }

    function renderDaily() {
        documentData.dailyHeaderDiv.innerText = renderOptions.selectedDate.getDayName();
        documentData.dailyDayDiv.dataset.appText = renderOptions.selectedDate.getDate();
        documentData.dailyDayColumns.forEach(col => {
            col.replaceChildren(createElementWithInnerHTML('p', '&nbsp;'));
        });
        documentData.dailyDayColumns[0].replaceChildren(createElementWithInnerHTML('p', documentData.dailyDayDiv.dataset.appText));
        renderHourScale();
    }

    function renderHourScale() {
        let hourScaleDivs = [documentData.weekHourScaleDiv, documentData.dailyHourScaleDiv];
        let pSample = createElementWithInnerText('p');
        pSample.classList.add('hour');
        hourScaleDivs.forEach(hourScaleBlock => {
            hourScaleBlock.innerText = "";
            for (let h = 0; h < 24; h += 2) {
                let scalePosition = scaleMarginFirst + h * scaleMarginStep;
                hourScaleBlock.appendChild(createScaleMark(pSample, scalePosition, h.toString().padStart(2, '0')));
                scalePosition += 0.5 * scaleMarginStep;
                hourScaleBlock.appendChild(createScaleMark(pSample, scalePosition, '&nbsp; -'));
                scalePosition += 0.5 * scaleMarginStep;
                hourScaleBlock.appendChild(createScaleMark(pSample, scalePosition, '&mdash;'));
                scalePosition += 0.5 * scaleMarginStep;
                hourScaleBlock.appendChild(createScaleMark(pSample, scalePosition, '&nbsp; -'));
            }
        });
    }

    function createScaleMark(pSample, styleTopValue, content) {
        let pClon = pSample.cloneNode();
        pClon.style.top = "" + styleTopValue + "em";
        pClon.replaceChildren(createElementWithInnerHTML('span', content));
        return pClon;
    }

    function createElementWithInnerHTML(tagName, innerHTML = "") {
        let element = document.createElement(tagName);
        element.innerHTML = innerHTML;
        return element;
    }

    function createElementWithInnerText(tagName, innerText = "") {
        let element = document.createElement(tagName);
        element.innerText = innerText;
        return element;
    }

    function createElementWithLecture(lecture) {
        let element = createElementWithInnerText('p');
        element.classList.add('hour', 'lecture');
        let h = +lecture.beginning.substring(0, 2) + lecture.beginning.substring(3, 5) / 60;
        element.style.top = "" + (scaleMarginFirst + h * scaleMarginStep).toFixed(1) + 'em';
        element.innerText = lectureToString(lecture);
        return element;
    }

    function lectureToElementArray(lecture, index) {
        let array = [];
        let element = createElementWithInnerText('p');
        element.classList.add('lecture-part');
        let h = +lecture.beginning.substring(0, 2) + lecture.beginning.substring(3, 5) / 60;
        element.style.top = "" + (scaleMarginFirst + (h - index - 1) * scaleMarginStep).toFixed(1) + 'em';
        let clon = element.cloneNode();
        clon.innerHTML = lecture.beginning.substring(0, 5) + '&nbsp';
        array.push(clon);
        clon = element.cloneNode();
        clon.innerHTML = lecture.ending.substring(0, 5) + '&nbsp';
        array.push(clon);
        clon = element.cloneNode();
        clon.innerHTML = lecture.name + '&nbsp';
        array.push(clon);
        clon = element.cloneNode();
        clon.innerHTML = lecture.another + '&nbsp';
        array.push(clon);
        clon = element.cloneNode();
        clon.innerHTML = lecture.room + '&nbsp';
        array.push(clon);
        return array;
    }

    function lectureToString(lecture) {
        return "" + lecture.beginning.substring(0, 5) + " " + lecture.name
                + ", " + lecture.group + ", " + lecture.teacher + ", " + lecture.room
    }

    function getControlPanel() {
        let controlPanel = {};
        controlPanel.selectedDate = document.querySelector("input[name='selected-date']");
        controlPanel.controlInfo = document.querySelector("[data-app-id='control-info']");
        controlPanel.buttonToday = document.querySelector("[data-app-id='btn-today']");
        controlPanel.buttonToday.addEventListener('click', onMouseButtonToday);
        controlPanel.buttonPeriod = document.querySelector("[data-app-id='btn-period']");
        [controlPanel.buttonDay = document.querySelector("[data-app-id='btn-day']"),
            controlPanel.buttonWeek = document.querySelector("[data-app-id='btn-week']"),
            controlPanel.buttonMonth = document.querySelector("[data-app-id='btn-month']")
        ].forEach((btn) => btn.addEventListener('click', onMouseSelectPeriod));
        controlPanel.buttonBack = document.querySelector("[data-app-id='btn-back']");
        controlPanel.buttonBack.addEventListener('click', onMouseButtonBack);
        controlPanel.buttonForward = document.querySelector("[data-app-id='btn-forward']");
        controlPanel.buttonForward.addEventListener('click', onMouseButtonForward);
        return controlPanel;
    }

    function getDocumentData() {
        let docData = {};
        docData.timetableData = {};
        docData.daysOfMonthMap = {};
        docData.daysOfWeekMap = {};
        docData.weekHeaderDiv = document.querySelector(".grid.week-header");
        docData.monthDiv = document.querySelector(".grid.month");
        docData.dayOfMonthDivs = [...docData.monthDiv.querySelectorAll("[data-app-month-day]")];
        addEventListener(docData.dayOfMonthDivs, 'click', onMouseDay);
        docData.lastWeekDivs = docData.dayOfMonthDivs.slice(35).concat([document.querySelector("[data-app-week='6']")]);
        docData.weekNumbers = [...document.querySelectorAll("[data-app-week]")];
        addEventListener(docData.weekNumbers, 'click', onMouseWeekNumber);
        docData.weekHourScaleDiv = document.querySelector(".week [data-app-type='hour-scale']");
        docData.weekDiv = document.querySelector(".grid.week");
        docData.dayOfWeekDivs = [...docData.weekDiv.querySelectorAll("[data-app-week-day]")];
        addEventListener(docData.dayOfWeekDivs, 'click', onMouseDay);
        docData.dailyDiv = document.querySelector("[data-app-id='daily']");
        docData.dailyHeaderDiv = document.querySelector("[data-app-id='daily-header']");
        docData.dailyHourScaleDiv = document.querySelector(".daily [data-app-type='hour-scale']");
        docData.dailyDayDiv = document.querySelector("[data-app-id='daily-day']");
        docData.dailyDayColumns = [...docData.dailyDayDiv.querySelectorAll(".day-col")];
        return docData;
    }

    function addEventListener(elements, event, listener) {
        elements.forEach(element => {
            element.addEventListener(event, listener);
        });
    }

    function updateRenderOptions(options) {
        options.selectedDate = new Date(controlPanel.selectedDate.value);
        options.period = controlPanel.buttonPeriod.dataset.appValue;
        options.monthStart = options.selectedDate.atDay(1);
        options.monthEnd = options.selectedDate.getEndOfMonth();
        options.monthRenderStart = options.monthStart.getStartOfWeek();
        options.monthRenderEnd = options.monthEnd.getEndOfWeek();
        options.monthStartWeek = options.monthStart.getWeek();
        options.monthEndWeek = options.monthEnd.getWeek();
        options.weekRenderStart = options.selectedDate.getStartOfWeek();
        options.weekRenderEnd = options.selectedDate.getEndOfWeek();
    }
});