/**
 * For this script, you need to download the date-utils.js script
 * e.g. <script src="./js/date-utils.js"></script>
 */
document.addEventListener('DOMContentLoaded', () => {
    const style = document.createElement('style');
    style.textContent = "tr[data-app-href], .orderly { cursor: pointer; }";
    document.head.appendChild(style);

    const documentData = getDocumentData();
    onLoadContent();

    function onLoadContent() {
        let jsonData = document.querySelector("[data-app-response-data]");
        try {
            documentData.data = JSON.parse(jsonData.dataset.appResponseData);
        } catch (exception) {
            fetchAndRenderData();
        } finally {
            if (jsonData) {
                jsonData.remove();
            }
        }
        renderData();
    }

    function fetchAndRenderData() {
        fetchData().then((responseData) => {
            documentData.data = responseData;
            renderData();
        });
    }

    function getFilterData() {
        let formData = {};
        let checkboxFilter = documentData.filter.items.checkboxFilter;
        [...documentData.filter.itself.querySelectorAll("[name]")]
                .filter(e => ifFilterIsCheckedThenAllElseOnlyRequired(e) && skipUnnamedElementAndUncheckedCheckbox(e))
                .forEach(e => formData[e.name] = e.value);
        return formData;

        function skipUnnamedElementAndUncheckedCheckbox(element) {
            return element.name && (element.nodeName !== 'INPUT' || element.type !== 'checkbox' || element.checked);
        }

        function ifFilterIsCheckedThenAllElseOnlyRequired(element) {
            return checkboxFilter.checked || element.dataset.appRequired === 'true';
        }
    }

    async function fetchData() {
        let url = new URL(window.location.href);
        url.search = "";
        url.pathname += '/pages/' + documentData.pagination.pageIndex.value;
        Object.entries(getFilterData()).forEach(([k, v]) => url.searchParams.set(k, '' + v));

        const response = await fetch(url, {
            method: 'GET', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json'
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *client
            //body: JSON.stringify({}) // body data type must match "Content-Type" header
        });
        return await response.json();

        let xhr = new XMLHttpRequest();
        xhr.responseType = 'json';
        xhr.open("GET", url, true);
        xhr.setRequestHeader('Content-Type', 'application/json');

        xhr.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                let responseData = this.response;
            }
        };
        xhr.send();
    }

    function getDocumentData() {
        let documentData = {
            data: getEmptyData(),
            tbody: document.querySelector("tbody[data-app-href]"),
            filter: getFilterForm(),
            ordering: getOrdering(),
            pagination: getPaginationControl()
        };
        let template = documentData.template = {};
        template.row = documentData.tbody.querySelector("tr");
        template.href = template.row.dataset.appHref;
        template.cells = {
            id: template.row.querySelector("td[data-app-col-id='id']"),
            dateTime: template.row.querySelector("td[data-app-col-id='date-time']"),
            name: template.row.querySelector("td[data-app-col-id='name'] a"),
            group: template.row.querySelector("td[data-app-col-id='group']"),
            teacher: template.row.querySelector("td[data-app-col-id='teacher']"),
            room: template.row.querySelector("td[data-app-col-id='room']"),
            link: template.row.querySelector("td[data-app-col-id='link'] a"),
        }

        return documentData;
    }

    function getFilterForm() {
        let filterForm = {}
        filterForm.itself = document.querySelector("fieldset[data-app-id='filter-form']");
        filterForm.items = getFilterFormItems(filterForm.itself);

        filterForm.itself.querySelectorAll(`li[data-app-id^='btn-']`).forEach((btn) => {
            btn.addEventListener('click', onMouseSelectDateOrTimeRange);
        });

        function onMouseSelectDateOrTimeRange() {
            let btnId = this.dataset.appId;
            let now = new Date();
            let date = now.getStartOfDay();
            let time = now.getStartOfMinute();

            let switcher = {}
            switcher['btn-today'] = {method: setRangeAsDay, arg1: date};
            switcher['btn-tomorrow'] = {method: setRangeAsDay, arg1: date.plusDays(1)};
            switcher['btn-after-tomorrow'] = {method: setRangeAsDay, arg1: date.plusDays(2)};
            switcher['btn-day-minus'] = {method: shiftDateRange, arg1: 'day', arg2: -1};
            switcher['btn-day-plus'] = {method: shiftDateRange, arg1: 'day', arg2: 1};
            switcher['btn-current-week'] = {method: setRangeAsWeek, arg1: date.getStartOfWeek()};
            switcher['btn-next-week'] = {method: setRangeAsWeek, arg1: date.plusDays(7)};
            switcher['btn-week-minus'] = {method: shiftDateRange, arg1: 'week', arg2: -1};
            switcher['btn-week-plus'] = {method: shiftDateRange, arg1: 'week', arg2: 1};
            switcher['btn-current-month'] = {method: setRangeAsMonth, arg1: date};
            switcher['btn-next-month'] = {method: setRangeAsMonth, arg1: date.plusMonths(1)};
            switcher['btn-month-minus'] = {method: shiftDateRange, arg1: 'month', arg2: -1};
            switcher['btn-month-plus'] = {method: shiftDateRange, arg1: 'month', arg2: 1};

            switcher['btn-current-hour'] = {method: setHourRange, arg1: time.getStartOfHour()};
            switcher['btn-next-hour'] = {method: setHourRange, arg1: time};
            switcher['btn-hour-minus'] = {method: shiftTimeRange, arg1: -1};
            switcher['btn-hour-plus'] = {method: shiftTimeRange, arg1: 1};
            switcher['btn-after-now'] = {method: setTimeRange, arg1: time, arg2: time.getEndOfDay()};
            switcher['btn-before-now'] = {method: setTimeRange, arg1: time.getStartOfDay(), arg2: time};
            switcher['btn-all-day'] = {method: setTimeRange, arg1: time.getStartOfDay(), arg2: time.getEndOfDay()};

            let method, arg1, arg2;
            ({method, arg1, arg2} = switcher[btnId]);
            if (method) {
                method(arg1, arg2);
            }

            function setHourRange(time) {
                setTimeRange(time, time.plusMinutes(59));
            }

            function setRangeAsDay(date) {
                setDateRange(date, date);
            }

            function setRangeAsWeek(date) {
                setDateRange(date.getStartOfWeek(), date.getEndOfWeek());
            }

            function setRangeAsMonth(date) {
                setDateRange(date.atDay(1), date.getEndOfMonth());
            }

            function shiftTimeRange(value) {
                let today = new Date().toShortString();
                let timeFrom = new Date(today + 'T' + filterForm.items.timeFrom.value + ':00').plusHours(value);
                let timeTo = new Date(today + 'T' + filterForm.items.timeTo.value + ':00').plusHours(value);
                filterForm.items.timeFrom.value = timeFrom.toShortTimeString();
                filterForm.items.timeTo.value = timeTo.toShortTimeString();
            }

            function shiftDateRange(shift, value) {
                let dateFrom = new Date(filterForm.items.dateFrom.value);
                let dateTo = new Date(filterForm.items.dateTo.value);
                switch (shift) {
                    case 'day':
                        dateFrom = dateFrom.plusDays(value);
                        dateTo = dateTo.plusDays(value);
                        break;
                    case 'week':
                        dateFrom = dateFrom.plusDays(value * 7);
                        dateTo = dateTo.plusDays(value * 7);
                        break;
                    case 'month':
                        dateFrom = dateFrom.plusMonths(value);
                        dateTo = dateTo.plusMonths(value);
                        break;
                }
                filterForm.items.dateFrom.value = dateFrom.toShortString();
                filterForm.items.dateTo.value = dateTo.toShortString();
            }

            function setDateRange(dateFrom, dateTo) {
                filterForm.items.dateFrom.value = dateFrom.toShortString();
                filterForm.items.dateTo.value = dateTo.toShortString();
                filterForm.items.periodChoice.click();
            }

            function setTimeRange(timeFrom, timeTo) {
                filterForm.items.timeFrom.value = timeFrom.toShortTimeString();
                filterForm.items.timeTo.value = timeTo.toShortTimeString();
                filterForm.items.timeChoice.click();
            }
        }

        return filterForm;
    }

    function getPaginationControl() {
        let control = document.querySelector("div[data-app-pagination]");

        let pagination = {
            pageIndex: control.querySelector("input[data-app-role='pg-index']"),
            ul: control.querySelector("ul.pagination"),

            onMousePageLink(event) {
                event.preventDefault();
                let pageIndex = pagination.pageIndex;

                let role = this.parentElement.dataset.appRole;
                let thisValue = this.parentElement.value;
                let newIndex = getNewIndex();

                newIndex = Math.max(0, Math.min(newIndex, +pageIndex.max));
                if (newIndex !== +pageIndex.value) {
                    pageIndex.dataset.appValue = pageIndex.value;
                    pageIndex.value = newIndex;
                    fetchAndRenderData();
                }

                function getNewIndex() {
                    switch (role) {
                        case "pg-first":
                            return 0;
                        case "pg-prev":
                            return +pageIndex.value - 1;
                        case "pg-next":
                            return +pageIndex.value + 1;
                        case "pg-last":
                            return +pageIndex.max;
                        case "pg-link":
                            return +thisValue;
                        default:
                            return +pageIndex.value;
                    }
                }
            }
        }

        let pages = pagination.pages = {};
        pages.first = {a: pagination.ul.querySelector("li[data-app-role='pg-first']>a")};
        pages.prev = {a: pagination.ul.querySelector("li[data-app-role='pg-prev']>a")};
        pages.next = {a: pagination.ul.querySelector("li[data-app-role='pg-next']>a")};
        pages.last = {a: pagination.ul.querySelector("li[data-app-role='pg-last']>a")};
        Object.entries(pages).forEach(([, page]) => {
            page.a.addEventListener('click', pagination.onMousePageLink);
            page.li = page.a.parentElement;
        });

        let links = pages.links = [];
        links[0] = {a: pagination.ul.querySelector("li[data-app-role='pg-link']>a")};
        links[0].a.addEventListener('click', pagination.onMousePageLink);
        links[0].li = links[0].a.parentElement;
        links[0].li.classList.remove('active');

        let template = pages.template = {};
        template.li = links[0].li.cloneNode(true);
        template.a = template.li.querySelector('a');

        return pagination;
    }

    function renderData() {
        let rowTemplate = documentData.template;
        let tbody = documentData.tbody;
        let cells = rowTemplate.cells;
        tbody.innerHTML = "";
        documentData.data.rows.forEach(lecture => {
            let href = tbody.dataset.appHref + lecture.id;
            cells.id.innerText = lecture.id;
            cells.name.innerText = lecture.name;
            cells.name.href = href;
            cells.dateTime.innerText = lecture.date + ' ' + lecture.beginning.substring(0, 5);
            cells.group.innerText = lecture.group;
            cells.teacher.innerText = lecture.teacher;
            cells.room.innerText = lecture.room;
            cells.link.href = href;
            let rowClone = rowTemplate.row.cloneNode(true);
            rowClone.dataset.appHref = href;
            rowClone.addEventListener('click', onMouseRow);
            tbody.appendChild(rowClone);
        });
        [...tbody.querySelectorAll("td[data-app-col-id='link'] a")].forEach(a => a.addEventListener('click', onMouseLink));
        renderPagination();

        function onMouseRow() {
            window.location.href = this.dataset.appHref;
        }

        function onMouseLink(event) {
            event.preventDefault();
            event.stopPropagation();
            window.open(this.href, '_blank').focus();
        }
    }

    function renderPagination() {
        let data = documentData.data;
        let pagination = documentData.pagination;
        let pages = pagination.pages;
        let links = pages.links;
        let template = pages.template;
        let pageIndex = pagination.pageIndex;

        let dataSize = Math.trunc(Math.max(1, +data.size));
        let dataTotal = Math.trunc(Math.max(0, +data.total));
        let maxIndex = Math.trunc(dataTotal / dataSize) + Math.min(1, dataTotal % dataSize) - 1;
        let newIndex = Math.trunc(Math.min(Math.max(0, maxIndex), +data.index));

        if (maxIndex < links.length - 1) {
            let newSize = maxIndex < 0 ? 1 : maxIndex + 1;
            while (links.length > newSize) {
                let link = links.pop();
                link.li.remove();
            }
        } else if (maxIndex > links.length - 1) {
            let newSize = maxIndex + 1;
            while (links.length < newSize) {
                template.li.value = links.length;
                template.a.innerText = links.length + 1;
                let clone = {li: template.li.cloneNode(true)};
                clone.a = clone.li.querySelector('a');
                clone.a.addEventListener('click', pagination.onMousePageLink);
                links.push(clone);
                pagination.ul.insertBefore(clone.li, pages.next.li);
            }
        }

        if (newIndex === 0) {
            makeDisabled(pages.first.li, pages.prev.li);
        } else {
            makeEnabled(pages.first.li, pages.prev.li);
        }
        if (newIndex === maxIndex) {
            makeDisabled(pages.next.li, pages.last.li);
        } else {
            makeEnabled(pages.next.li, pages.last.li);
        }
        links.forEach(link => link.li.classList.remove('active'));
        if (maxIndex < 0) {
            links[0].a.innerText = 0;
            makeDisabled(links[0].li);
        } else {
            links[0].a.innerText = 1;
            makeEnabled(links[0].li);
            links[newIndex].li.classList.add('active');
        }

        pageIndex.max = Math.max(0, maxIndex);
        pageIndex.value = newIndex;

        function makeEnabled(...liArgs) {
            liArgs.forEach(li => li.classList.remove('disabled'));
        }

        function makeDisabled(...liArgs) {
            liArgs.forEach(li => li.classList.add('disabled'));
        }
    }

    function getFilterFormItems(form) {
        let formItems = {};
        formItems.checkboxFilter = form.querySelector("fieldset input[name='filter']");
        (formItems.filterIcons = [...document.querySelectorAll("i[data-app-filter-active]")])
                .forEach((icon) => icon.dataset.appFilterActive = formItems.checkboxFilter.checked);
        formItems.dateFrom = form.querySelector("input[data-app-id='dateFrom']");
        formItems.dateTo = form.querySelector("input[data-app-id='dateTo']");
        formItems.periodChoice = form.querySelector("[data-app-id='periodChoice']");
        formItems.timeFrom = form.querySelector("input[data-app-id='timeFrom']");
        formItems.timeTo = form.querySelector("input[data-app-id='timeTo']");
        formItems.timeChoice = form.querySelector("button[data-app-id='timeChoice']");
        formItems.pageIndex = document.querySelector("div[data-app-pagination] input[data-app-role='pg-index']");

        (formItems.buttonFind = form.querySelector("[data-app-id='button-find']"))
                .addEventListener('click', () => onClickFormButton(true));
        (formItems.buttonShowAll = form.querySelector("[data-app-id='button-clear']"))
                .addEventListener('click', () => onClickFormButton(false));
        (formItems.checkboxAllDay = form.querySelector("input[name='allDay']"))
                .addEventListener("change", onChangeAllDay);
        (formItems.checkboxOnlyDrafts = form.querySelector("input[name='onlyDrafts']"))
                .addEventListener("change", onChangeDraftsOrAllGroupsOrAllTeachers);
        (formItems.checkboxAllGroups = form.querySelector("input[name='allGroups']"))
                .addEventListener("change", onChangeDraftsOrAllGroupsOrAllTeachers);
        (formItems.checkboxAllTeachers = form.querySelector("input[name='allTeachers']"))
                .addEventListener("change", onChangeDraftsOrAllGroupsOrAllTeachers);
        formItems.selectGroup = form.querySelector("select[data-app-id='selectGroup']");
        formItems.selectTeacher = form.querySelector("select[data-app-id='selectTeacher']");

        onChangeAllDay();
        onChangeDraftsOrAllGroupsOrAllTeachers();

        Array.of(formItems.checkboxAllGroups, formItems.checkboxAllTeachers)
                .forEach((checkbox) => checkbox.closest("div.input-group").addEventListener('dblclick', doubleClickOnCheckbox(checkbox)));

        return formItems;

        function onChangeDraftsOrAllGroupsOrAllTeachers() {
            setEnabledSelectGroup(!formItems.checkboxOnlyDrafts.checked && formItems.checkboxAllGroups.checked);
            setEnabledSelectTeacher(!formItems.checkboxOnlyDrafts.checked && formItems.checkboxAllTeachers.checked);

            function setEnabledSelectGroup(enable) {
                formItems.selectGroup.disabled = !enable;
                formItems.selectGroup.name = enable ? 'groupId' : '';
            }

            function setEnabledSelectTeacher(enable) {
                formItems.selectTeacher.disabled = !enable;
                formItems.selectTeacher.name = enable ? 'teacherId' : '';
            }
        }

        function doubleClickOnCheckbox(checkbox) {
            return () => {
                if (!checkbox.checked) {
                    checkbox.checked = true;
                    onChangeDraftsOrAllGroupsOrAllTeachers();
                }
            };
        }

        function onChangeAllDay() {
            setEnabledTimeRange(formItems.checkboxAllDay.checked);

            function setEnabledTimeRange(enable) {
                formItems.timeFrom.disabled = !enable;
                formItems.timeFrom.name = enable ? 'timeFrom' : '';
                formItems.timeTo.disabled = !enable;
                formItems.timeTo.name = enable ? 'timeTo' : '';
                formItems.timeChoice.disabled = !enable;
            }
        }

        function onClickFormButton(boolValue) {
            formItems.pageIndex.dataset.appValue = '0';
            formItems.pageIndex.value = 0;
            formItems.checkboxFilter.value = boolValue;
            formItems.checkboxFilter.checked = boolValue;
            formItems.filterIcons.forEach((icon) => icon.dataset.appFilterActive = boolValue);
            fetchAndRenderData();
        }
    }

    function getOrdering() {
        let ordering = {}
        let orderSelects = [...document.querySelectorAll(`fieldset[data-app-id='filter-form'] select[name^='by']`)];
        let columnHeaders = [...document.querySelectorAll(`thead th[data-app-orderly]`)];
        let ordered = ordering.ordered = {};
        columnHeaders.forEach(header => {
            let appOrderly = header.dataset.appOrderly;
            let select = orderSelects.find(select => select.name === withPrefix("by", appOrderly));
            if (select) {
                header.classList.add("orderly");
                let orderly = {};
                orderly.icon = header.querySelector("i[data-app-direction]");
                orderly.header = header;
                orderly.select = select;
                ordered[appOrderly] = orderly;
            }
        });
        makeClickableOrderly(ordering);
        return ordering;

        function withPrefix(prefix, string) {
            return prefix + string[0].toUpperCase() + string.slice(1);
        }
    }

    function makeClickableOrderly(ordering) {
        if (Object.entries(ordering.ordered).length > 0) {
            Object.entries(ordering.ordered).forEach(([, orderly]) => {
                orderly.header.addEventListener('click', onMouseOrderly)
            });
            ordering.firstBy = document.querySelector("fieldset[data-app-id='filter-form'] select[name='firstBy']");
            changeOrderlyImages(ordering);
        }

        function onMouseOrderly() {
            let key = this.dataset.appOrderly;
            let orderly = ordering.ordered[key];
            if (ordering.firstBy.value !== key) {
                ordering.firstBy.value = key;
            } else {
                shiftSelected(orderly.select);
            }
            changeOrderlyImages();
            fetchAndRenderData();

            function shiftSelected(select) {
                let options = [...select.querySelectorAll("option")];
                let index = options.findIndex(o => o.value === select.value);
                index = (index + 1) % options.length;
                select.value = options[index].value;
            }
        }

        function changeOrderlyImages() {
            Object.entries(ordering.ordered).forEach(([key, orderly]) => {
                let priority = key === ordering.firstBy.value ? 'first' : 'next';
                orderly.icon.dataset.appDirection = `${priority}-${orderly.select.value}`;
            });
        }
    }

    function getEmptyData() {
        return {
            "index": 0,
            "size": 1,
            "total": 1,
            "order": {
                "dateTime": "asc",
                "group": "asc",
                "teacher": "asc",
                "firstBy": "dateTime"
            },
            "rows": [
                {
                    "id": 1,
                    "name": "",
                    "date": "2001-01-01",
                    "beginning": "00:00:00",
                    "ending": "23:59:59.999999999",
                    "another": "",
                    "teacher": "",
                    "group": "",
                    "room": ""
                }
            ],
            "count": 1
        }
    }
});
