document.addEventListener('DOMContentLoaded', () => {
    const documentData = getDocumentData();

    function getDocumentData() {
        let documentData = {
            students: {
                data: null,
                template: {
                    cells: {}
                }
            }
        }
        let template = documentData.students.template;
        documentData.students.tbody = document.querySelector("table[data-app-id='students-table']>tbody");
        template.row = documentData.students.tbody.querySelector("tr");
        template.cells.id = template.row.querySelector("td[data-app-col-id='id']");
        template.cells.name = template.row.querySelector("td[data-app-col-id='name']");

        document.querySelector("[data-app-id='btn-students-tab']")
                .addEventListener('click', onMouseTabStudents);
        let btnRefrashe = document.querySelector("[data-app-id='btn-students-refresh']");
        btnRefrashe.addEventListener('click', onMouseRefrasheStudents);
        documentData.students.href = btnRefrashe.href;

        function onMouseTabStudents() {
            if (!documentData.students.data) {
                fetchAndRenderData();
            }
        }

        function onMouseRefrasheStudents(event) {
            event.preventDefault();
            fetchAndRenderData();
        }

        return documentData;
    }

    function renderData() {
        let data = documentData.students.data;
        let template = documentData.students.template;
        let tbody = documentData.students.tbody;
        tbody.innerText = "";
        if (data.size === 0) {
            template.cells.id.innerText = "";
            template.cells.name.innerText = "";
            tbody.appendChild(template.row.cloneNode(true));
        } else {
            data.rows.forEach((stud) => {
                template.cells.id.innerText = stud.id;
                template.cells.name.innerText = stud.name;
                tbody.appendChild(template.row.cloneNode(true));
            });
        }
    }

    function fetchAndRenderData() {
        fetchData().then((responseData) => {
            documentData.students.data = responseData;
            renderData();
        });
    }

    async function fetchData() {
        let url = new URL(documentData.students.href);

        const response = await fetch(url, {
            method: 'GET', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json'
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *client
            //body: JSON.stringify({}) // body data type must match "Content-Type" header
        });
        return await response.json();
    }

    function getEmptyData() {
        return {
            "index": 0,
            "size": 1,
            "total": 1,
            "order": {
                "name": "asc"
            },
            "rows": [
                {
                    "id": 1,
                    "name": ""
                }
            ],
            "count": 1
        }
    }
});