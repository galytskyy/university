DROP SCHEMA IF EXISTS university CASCADE;
CREATE SCHEMA university;

CREATE SEQUENCE university.rooms_pkey_seq START 1000;

CREATE TABLE university.rooms
(
    room_id   INTEGER NOT NULL DEFAULT nextval('university.rooms_pkey_seq'),
    room_name VARCHAR(50),
    CONSTRAINT rooms_pkey PRIMARY KEY (room_id)
);

CREATE SEQUENCE university.teachers_pkey_seq START 1000;

CREATE TABLE university.teachers
(
    teacher_id INTEGER NOT NULL DEFAULT nextval('university.teachers_pkey_seq'),
    first_name VARCHAR(50),
    surname    VARCHAR(50),
    CONSTRAINT teachers_pkey PRIMARY KEY (teacher_id)
);

CREATE SEQUENCE university.students_pkey_seq START 1000;

CREATE TABLE university.students
(
    student_id INTEGER NOT NULL DEFAULT nextval('university.students_pkey_seq'),
    first_name VARCHAR(50),
    surname    VARCHAR(50),
    CONSTRAINT students_pkey PRIMARY KEY (student_id)
);

CREATE SEQUENCE university.groups_pkey_seq START 1000;

CREATE TABLE university.groups
(
    group_id   INTEGER NOT NULL DEFAULT nextval('university.groups_pkey_seq'),
    group_name VARCHAR(50),
    CONSTRAINT groups_pkey PRIMARY KEY (group_id)
);

CREATE TABLE university.student_group
(
    student_id INTEGER NOT NULL,
    group_id   INTEGER,
    CONSTRAINT student_group_uniq UNIQUE (student_id)
);

ALTER TABLE university.student_group
    ADD FOREIGN KEY (student_id) REFERENCES university.students (student_id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE university.student_group
    ADD FOREIGN KEY (group_id) REFERENCES university.groups (group_id) ON UPDATE CASCADE ON DELETE NO ACTION;

CREATE SEQUENCE university.lectures_pkey_seq START 1000;

CREATE TABLE university.lectures
(
    lecture_id   INTEGER NOT NULL DEFAULT nextval('university.lectures_pkey_seq'),
    lecture_name VARCHAR(150),
    lecture_date DATE,
    beginning    TIME(0),
    ending       TIME(0),
    teacher_id   INTEGER,
    group_id     INTEGER,
    room_id      INTEGER,
    CONSTRAINT lectures_pkey PRIMARY KEY (lecture_id)
);

ALTER TABLE university.lectures
    ADD FOREIGN KEY (teacher_id) REFERENCES university.teachers (teacher_id) ON UPDATE CASCADE ON DELETE NO ACTION;

ALTER TABLE university.lectures
    ADD FOREIGN KEY (group_id) REFERENCES university.groups (group_id) ON UPDATE CASCADE ON DELETE NO ACTION;

ALTER TABLE university.lectures
    ADD FOREIGN KEY (room_id) REFERENCES university.rooms (room_id) ON UPDATE CASCADE ON DELETE NO ACTION;