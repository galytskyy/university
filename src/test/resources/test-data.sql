DELETE
FROM university.lectures;
DELETE
FROM university.student_group;
DELETE
FROM university.students;
DELETE
FROM university.groups;
DELETE
FROM university.teachers;
DELETE
FROM university.rooms;

INSERT INTO university.teachers(teacher_id, first_name, surname)
values (1, 'Minerva', 'McGonagall'),
       (2, 'Severus', 'Snape'),
       (3, 'Aurora', 'Sinistra');

INSERT INTO university.students(student_id, first_name, surname)
values (1, 'Harry', 'Potter'),
       (2, 'Ronald', 'Weasley'),
       (3, 'Hermione', 'Granger');
INSERT INTO university.students(student_id, first_name, surname)
values (4, 'Draco', 'Malfoy'),
       (5, 'Vincent', 'Crabbe');

INSERT INTO university.groups(group_id, group_name)
values (1, 'GR-11'),
       (2, 'SL-12');

INSERT INTO university.student_group(student_id, group_id)
values (1, 1),
       (2, 1),
       (3, 1),
       (4, null);

INSERT INTO university.rooms(room_id, room_name)
values (1, 'Transfiguration classroom'),
       (2, 'Potions classroom'),
       (3, 'Disused classroom');

SET @date = CURRENT_DATE - 49;
SET @i = 1;
SET @step = 2;
INSERT INTO university.lectures(lecture_id, lecture_name, lecture_date, beginning, ending, teacher_id, group_id,
                                room_id)
VALUES (@i, 'Transfiguration', CURRENT_DATE, '08:30', '09:59:59', null, 1, 1),
       (@i + 1, 'Potions', CURRENT_DATE + 1, '08:30', '09:59:59', 2, 1, null);
SET @i = @i + @step;
INSERT INTO university.lectures(lecture_id, lecture_name, lecture_date, beginning, ending, teacher_id, group_id,
                                room_id)
VALUES (@i, 'Transfiguration', @date, '11:00', '12:29:59', 1, 1, 1),
       (@i + 1, 'Potions', @date + 1, '13:00', '14:29:59', 2, 1, 2);
SET @date = @date + 7;
SET @i = @i + @step;
INSERT INTO university.lectures(lecture_id, lecture_name, lecture_date, beginning, ending, teacher_id, group_id,
                                room_id)
VALUES (@i, 'Transfiguration', @date, '11:00', '12:29:59', 1, 1, 1),
       (@i + 1, 'Potions', @date + 1, '13:00', '14:29:59', 2, 1, 2);
SET @date = @date + 7;
SET @i = @i + @step;
INSERT INTO university.lectures(lecture_id, lecture_name, lecture_date, beginning, ending, teacher_id, group_id,
                                room_id)
VALUES (@i, 'Transfiguration', @date, '11:00', '12:29:59', 1, 1, 1),
       (@i + 1, 'Potions', @date + 1, '13:00', '14:29:59', 2, 1, 2);
SET @date = @date + 7;
SET @i = @i + @step;
INSERT INTO university.lectures(lecture_id, lecture_name, lecture_date, beginning, ending, teacher_id, group_id,
                                room_id)
VALUES (@i, 'Transfiguration', @date, '11:00', '12:29:59', 1, 1, 1),
       (@i + 1, 'Potions', @date + 1, '13:00', '14:29:59', 2, 1, 2);
SET @date = @date + 7;
SET @i = @i + @step;
INSERT INTO university.lectures(lecture_id, lecture_name, lecture_date, beginning, ending, teacher_id, group_id,
                                room_id)
VALUES (@i, 'Transfiguration', @date, '11:00', '12:29:59', 1, 1, 1),
       (@i + 1, 'Potions', @date + 1, '13:00', '14:29:59', 2, 1, 2);
SET @date = @date + 7;
SET @i = @i + @step;
INSERT INTO university.lectures(lecture_id, lecture_name, lecture_date, beginning, ending, teacher_id, group_id,
                                room_id)
VALUES (@i, 'Transfiguration', @date, '11:00', '12:29:59', 1, 1, 1),
       (@i + 1, 'Potions', @date + 1, '13:00', '14:29:59', 2, 1, 2);
SET @date = @date + 7;
SET @i = @i + @step;
INSERT INTO university.lectures(lecture_id, lecture_name, lecture_date, beginning, ending, teacher_id, group_id,
                                room_id)
VALUES (@i, 'Transfiguration', @date, '11:00', '12:29:59', 1, 1, 1),
       (@i + 1, 'Potions', @date + 1, '13:00', '14:29:59', 2, 1, 2);
SET @date = @date + 7;
SET @i = @i + @step;
INSERT INTO university.lectures(lecture_id, lecture_name, lecture_date, beginning, ending, teacher_id, group_id,
                                room_id)
VALUES (@i, 'Transfiguration', @date, '11:00', '12:29:59', 1, 1, 1),
       (@i + 1, 'Potions', @date + 1, '13:00', '14:29:59', 2, 1, 2);
SET @date = @date + 7;
SET @i = @i + @step;
INSERT INTO university.lectures(lecture_id, lecture_name, lecture_date, beginning, ending, teacher_id, group_id,
                                room_id)
VALUES (@i, 'Transfiguration', @date, '11:00', '12:29:59', 1, 1, 1),
       (@i + 1, 'Potions', @date + 1, '13:00', '14:29:59', 2, 1, 2);
SET @date = @date + 7;
SET @i = @i + @step;
INSERT INTO university.lectures(lecture_id, lecture_name, lecture_date, beginning, ending, teacher_id, group_id,
                                room_id)
VALUES (@i, 'Transfiguration', @date, '11:00', '12:29:59', 1, 1, 1),
       (@i + 1, 'Potions', @date + 1, '13:00', '14:29:59', 2, 1, 2);