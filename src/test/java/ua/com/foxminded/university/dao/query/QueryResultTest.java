package ua.com.foxminded.university.dao.query;

import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import ua.com.foxminded.university.dao.DaoException;
import ua.com.foxminded.university.entity.Room;
import java.util.function.Supplier;
import java.util.stream.Stream;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.sameInstance;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class QueryResultTest {

    @Test
    void collect_shouldThrowException_whenJdbcTemplateThrowException() {
        NamedParameterJdbcTemplate namedParameterJdbcTemplate = mock(NamedParameterJdbcTemplate.class);
        Supplier<Stream<Room>> streamSupplier = () -> namedParameterJdbcTemplate.queryForStream("",
                mock(SqlParameterSource.class), BeanPropertyRowMapper.newInstance(Room.class));
        QueryResult<Room> queryResult = QueryResult.newInstance(streamSupplier);
        RuntimeException expectedCause = mock(RuntimeException.class);
        when(namedParameterJdbcTemplate.queryForStream(any(String.class), any(SqlParameterSource.class), any()))
                .thenThrow(expectedCause);

        DaoException actualException = assertThrows(DaoException.class, () -> queryResult.collect(toList()));

        String expectedMessage = "Invocation error:";
        assertAll(
                () -> assertThat(actualException.getMessage(), startsWith(expectedMessage)),
                () -> assertThat(actualException.getCause(), sameInstance(expectedCause))
        );
    }
}