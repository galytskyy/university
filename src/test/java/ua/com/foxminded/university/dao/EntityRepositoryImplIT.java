package ua.com.foxminded.university.dao;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import ua.com.foxminded.university.IntegrationTest;
import ua.com.foxminded.university.entity.Room;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.sameInstance;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static ua.com.foxminded.university.entity.EntityUtils.newRoom;

@IntegrationTest
class EntityRepositoryImplIT {

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplateSpy;
    @Autowired
    RoomQueryFactory roomQueryFactory;
    @Autowired
    EntityRepository<Room> roomRepository;

    @Test
    void add_shouldThrowException_whenJdbcTemplateThrowException() {
        Room instance = newRoom(null, "Herbology classroom");
        RuntimeException expectedCause = mock(RuntimeException.class);
        doThrow(expectedCause)
                .when(namedParameterJdbcTemplateSpy).update(any(String.class), any(SqlParameterSource.class), any());

        DaoException actualException = assertThrows(DaoException.class, () -> roomRepository.add(instance));

        String expectedMessage = "Invocation error:";
        assertAll(
                () -> assertThat(actualException.getMessage(), startsWith(expectedMessage)),
                () -> assertThat(actualException.getCause(), sameInstance(expectedCause))
        );
    }

    @Test
    void remove_shouldThrowException_whenJdbcTemplateThrowException() {
        Room instance = newRoom(1, "Herbology classroom");
        RuntimeException expectedCause = mock(RuntimeException.class);
        doThrow(expectedCause)
                .when(namedParameterJdbcTemplateSpy).update(any(String.class), any(SqlParameterSource.class));

        DaoException actualException = assertThrows(DaoException.class, () -> roomRepository.remove(instance));

        String expectedMessage = "Invocation error:";
        assertAll(
                () -> assertThat(actualException.getMessage(), startsWith(expectedMessage)),
                () -> assertThat(actualException.getCause(), sameInstance(expectedCause))
        );
    }

    @Test
    void replace_shouldThrowException_whenJdbcTemplateThrowException() {
        Room instance = newRoom(1, "Herbology classroom");
        RuntimeException expectedCause = mock(RuntimeException.class);
        doThrow(expectedCause)
                .when(namedParameterJdbcTemplateSpy).update(any(String.class), any(SqlParameterSource.class));

        DaoException actualException = assertThrows(DaoException.class, () -> roomRepository.replace(instance));

        String expectedMessage = "Invocation error:";
        assertAll(
                () -> assertThat(actualException.getMessage(), startsWith(expectedMessage)),
                () -> assertThat(actualException.getCause(), sameInstance(expectedCause))
        );
    }
}