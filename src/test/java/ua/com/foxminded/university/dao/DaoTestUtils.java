package ua.com.foxminded.university.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.jdbc.JdbcTestUtils;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;
import static java.util.stream.Collectors.joining;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

@TestComponent
public class DaoTestUtils {

    private static final String LS = "\n";
    private static JdbcTemplate JDBC_TEMPLATE;

    @Autowired
    DaoTestUtils(JdbcTemplate jdbcTemplate) {
        DaoTestUtils.JDBC_TEMPLATE = jdbcTemplate;
    }

    public static String textFromTable(String tableName) {
        return textStreamFromTableWhere(tableName, "TRUE", "|").collect(joining(LS));
    }

    public static String textFromTableWhere(String tableName, String whereClause) {
        return textStreamFromTableWhere(tableName, whereClause, "|").collect(joining(LS));
    }

    public static Stream<String> textStreamFromTable(String tableName) {
        return textStreamFromTableWhere(tableName, "TRUE", "|");
    }

    public static Stream<String> textStreamFromTableWhere(String tableName, String whereClause) {
        return textStreamFromTableWhere(tableName, whereClause, "|");
    }

    public static Stream<String> textStreamFromTableWhere(String tableName, String where, String delimiter) {
        return textStreamFromTableWhere(JDBC_TEMPLATE, tableName, where, delimiter);
    }

    public static Stream<String> textStreamFromTableWhere(JdbcTemplate jdbcTemplate, String tableName,
                                                          String whereClause, String delimiter) {
        String sql = "SELECT * FROM " + tableName + (isNotBlank(whereClause) ? " WHERE " + whereClause : "");
        AtomicInteger index = new AtomicInteger(0);
        Supplier<Stream<Integer>> counter = () -> Stream.of(index.getAndIncrement());
        Function<Map<String, Object>, String> rowToString = (m) -> Stream.concat(counter.get(), m.values().stream())
                .map(String::valueOf).collect(joining(delimiter));
        return jdbcTemplate.queryForList(sql).stream().map(rowToString);
    }

    public static int countRowsInTable(String tableName) {
        return JdbcTestUtils.countRowsInTable(JDBC_TEMPLATE, tableName);
    }

    public static int countRowsInTableWhere(String tableName, String whereClause) {
        return JdbcTestUtils.countRowsInTableWhere(JDBC_TEMPLATE, tableName, whereClause);
    }

    public static int deleteFromTableWhere(String tableName, String whereClause, Object... args) {
        return JdbcTestUtils.deleteFromTableWhere(JDBC_TEMPLATE, tableName, whereClause, args);
    }
}
