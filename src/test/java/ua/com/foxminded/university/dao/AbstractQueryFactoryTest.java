package ua.com.foxminded.university.dao;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import ua.com.foxminded.university.dao.AbstractQueryFactory.SqlString;
import java.util.List;
import static java.text.MessageFormat.format;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static ua.com.foxminded.university.dao.GroupQueryFactory.GROUP_FIELDS;
import static ua.com.foxminded.university.dao.StudentQueryFactory.STUDENT_FIELDS;

class AbstractQueryFactoryTest {

    @Nested
    class SqlStringTest {

        @Test
        void getCountSql() {

            SqlString sqlString = SqlString.builder()
                    .fromTable("schema.table").countBy("field_id").where("TRUE").build();
            String actual1 = sqlString.getCountSql();

            SqlString.Params params = SqlString.Params.builder()
                    .fromTable("schema.table").countBy("field_id").where("WHERE (TRUE)").build();
            String actual2 = SqlString.finalizeSql(SqlString.createCountSql(params));

            String expected1 = """
                    SELECT COUNT(field_id) AS _$total
                    FROM schema.table
                    WHERE (TRUE);""";

            assertAll(
                    () -> assertEquals(expected1, actual1),
                    () -> assertEquals(expected1, actual2)
            );
        }

        @Test
        void getCountSql_withoutFields() {

            String actual = SqlString.builder().fromTable("schema.table").build().getCountSql();

            String expected = """
                    SELECT COUNT(*) AS _$total
                    FROM schema.table;""";

            assertEquals(expected, actual);
        }

        @Test
        void getSelectSql() {
            SqlString sqlString = SqlString.builder().fromTable("schema.table")
                    .field("table.field_id, table.field")
                    .leftJoin("schema.joined ON FALSE").where("TRUE")
                    .orderBy("table.field ASC").limit(7).offset(17).build();
            String actual1 = sqlString.getSelectSql();

            SqlString.Params params = SqlString.Params.builder().fromTable("schema.table")
                    .fields("""
                            table.field_id, table.field, 
                            7 AS _$limit, 17 AS _$offset""")
                    .joins("LEFT JOIN schema.joined ON FALSE")
                    .where("WHERE (TRUE)")
                    .orderBy("ORDER BY table.field ASC").limitOffset("LIMIT 7 OFFSET 17").build();
            String actual2 = SqlString.finalizeSql(SqlString.createSelectSql(params));

            String expected = """
                    SELECT table.field_id, table.field, 
                    7 AS _$limit, 17 AS _$offset
                    FROM schema.table
                    LEFT JOIN schema.joined ON FALSE
                    WHERE (TRUE)
                    ORDER BY table.field ASC
                    LIMIT 7 OFFSET 17;""";

            assertAll(
                    () -> assertEquals(expected, actual1),
                    () -> assertEquals(expected, actual2)
            );
        }

        @Test
        void getSelectSql_withCount() {
            SqlString sqlString = SqlString.builder().fromTable("schema.table")
                    .field("table.field_id, table.field")
                    .countBy("field_id")
                    .leftJoin("schema.joined ON FALSE")
                    .where("TRUE").orderBy("table.field ASC").limit(7).offset(17).build();
            String actual1 = sqlString.getSelectSql();

            SqlString.Params params = SqlString.Params.builder().fields("*")
                    .fromTable("""
                            (
                            SELECT table.field_id, table.field, 
                            7 AS _$limit, 17 AS _$offset
                            FROM schema.table
                            LEFT JOIN schema.joined ON FALSE
                            WHERE (TRUE)
                            ORDER BY table.field ASC
                            LIMIT 7 OFFSET 17
                            ) AS _data$sq""")
                    .joins("""
                            RIGHT JOIN (
                            SELECT COUNT(field_id) AS _$total
                            FROM schema.table
                            WHERE (TRUE)
                            ) AS _count$sq ON TRUE""")
                    .build();
            String actual2 = SqlString.finalizeSql(SqlString.createSelectSql(params));

            String expected = """
                    SELECT *
                    FROM (
                    SELECT table.field_id, table.field, 
                    7 AS _$limit, 17 AS _$offset
                    FROM schema.table
                    LEFT JOIN schema.joined ON FALSE
                    WHERE (TRUE)
                    ORDER BY table.field ASC
                    LIMIT 7 OFFSET 17
                    ) AS _data$sq
                    RIGHT JOIN (
                    SELECT COUNT(field_id) AS _$total
                    FROM schema.table
                    WHERE (TRUE)
                    ) AS _count$sq ON TRUE;""";

            assertAll(
                    () -> assertEquals(expected, actual1),
                    () -> assertEquals(expected, actual2)
            );
        }

        @Test
        void getSelectSql_shouldReturnSqlForEagerSelectGroup() {
            final String SCHEMA = AbstractQueryFactory.SCHEMA;
            final String fromTable = format("{0}groups", SCHEMA);
            final List<String> leftJoins = List.of(
                    format("{0}student_group ON student_group.group_id = groups.group_id", SCHEMA),
                    format("{0}students ON student_group.student_id = students.student_id", SCHEMA));

            String actual = SqlString.builder().field(GROUP_FIELDS).fromTable(fromTable)
                    .field(STUDENT_FIELDS).leftJoins(leftJoins)
                    .where("groups.group_id = :groupId").build().getSelectSql();

            String expected = format("""
                    SELECT groups.group_id, groups.group_name, students.student_id, students.first_name, students.surname
                    FROM {0}groups
                    LEFT JOIN {0}student_group ON student_group.group_id = groups.group_id
                    LEFT JOIN {0}students ON student_group.student_id = students.student_id
                    WHERE (groups.group_id = :groupId);""", SCHEMA);

            assertEquals(expected, actual);
        }
    }
}