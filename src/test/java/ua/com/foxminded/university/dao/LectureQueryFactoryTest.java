package ua.com.foxminded.university.dao;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ua.com.foxminded.university.dao.LectureQueryFactory.LectureOrder;
import ua.com.foxminded.university.dao.LectureQueryFactory.LectureOrder.Direction;
import ua.com.foxminded.university.dao.query.*;
import ua.com.foxminded.university.entity.Lecture;
import ua.com.foxminded.university.timetable.Filter;
import static java.text.MessageFormat.format;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static ua.com.foxminded.university.service.criteria.LectureCriteria.newOrder;

class LectureQueryFactoryTest {

    LectureQueryFactory lectureQueryFactory;

    @BeforeEach
    void setUp() {
        this.lectureQueryFactory = new LectureQueryFactory();
    }

    @Test
    void selectById() {
        SelectOne<Lecture> lectureSelectOne = lectureQueryFactory.selectById(1);
        String actual = QueryUtils.getSql((Query) lectureSelectOne);

        String expected = format("""
                SELECT lectures.lecture_id, lectures.lecture_name, lectures.lecture_date, lectures.beginning, lectures.ending,
                teachers.teacher_id, teachers.first_name, teachers.surname, groups.group_id, groups.group_name, rooms.room_id, rooms.room_name
                FROM {0}lectures
                LEFT JOIN {0}teachers ON teachers.teacher_id = lectures.teacher_id
                LEFT JOIN {0}groups ON groups.group_id = lectures.group_id
                LEFT JOIN {0}rooms ON rooms.room_id = lectures.room_id
                WHERE (lectures.lecture_id = :lectureId);""", AbstractQueryFactory.SCHEMA);

        assertEquals(expected, actual);
    }

    @Test
    void selectSlice() {
        LectureOrder lectureOrder = newOrder().dateTime(Direction.ASC).group(LectureOrder.Direction.ASC)
                .teacher(LectureOrder.Direction.ASC);
        int limit = 7;
        int offset = 17;

        SelectSliceByEntities<Lecture> selectSliceByLectures = lectureQueryFactory.selectSlice(lectureOrder, offset, limit);
        String actual = QueryUtils.getSql(selectSliceByLectures);

        String expected = format("""
                SELECT *
                FROM (
                SELECT lectures.lecture_id, lectures.lecture_name, lectures.lecture_date, lectures.beginning, lectures.ending,
                teachers.teacher_id, teachers.first_name, teachers.surname, groups.group_id, groups.group_name, rooms.room_id, rooms.room_name, 
                7 AS _$limit, 17 AS _$offset
                FROM {0}lectures
                LEFT JOIN {0}teachers ON teachers.teacher_id = lectures.teacher_id
                LEFT JOIN {0}groups ON groups.group_id = lectures.group_id
                LEFT JOIN {0}rooms ON rooms.room_id = lectures.room_id
                ORDER BY lecture_date ASC, beginning ASC, group_name ASC, surname ASC, first_name ASC, lecture_id ASC
                LIMIT 7 OFFSET 17
                ) AS _data$sq
                RIGHT JOIN (
                SELECT COUNT(lecture_id) AS _$total
                FROM {0}lectures
                ) AS _count$sq ON TRUE;""", AbstractQueryFactory.SCHEMA);

        assertEquals(expected, actual);
    }

    @Test
    void selectByFilter() {
        Filter filter = Filter.newInstance();
        String beginningDate = "'" + filter.getBeginningDate().toString() + "'";
        Select<Lecture> selectByFilter = lectureQueryFactory.selectByFilter(filter);
        String actual = QueryUtils.getSql((Query) selectByFilter);

        String expected = format("""
                SELECT lectures.lecture_id, lectures.lecture_name, lectures.lecture_date, lectures.beginning, lectures.ending,
                teachers.teacher_id, teachers.first_name, teachers.surname, groups.group_id, groups.group_name, rooms.room_id, rooms.room_name
                FROM {0}lectures
                LEFT JOIN {0}teachers ON teachers.teacher_id = lectures.teacher_id
                LEFT JOIN {0}groups ON groups.group_id = lectures.group_id
                LEFT JOIN {0}rooms ON rooms.room_id = lectures.room_id
                WHERE ((lecture_date BETWEEN {1} AND {1}
                ) AND (TRUE));""", AbstractQueryFactory.SCHEMA, beginningDate);

        assertEquals(expected, actual);
    }
}