package ua.com.foxminded.university.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ua.com.foxminded.university.IntegrationTest;
import ua.com.foxminded.university.dao.DaoTestUtils;
import ua.com.foxminded.university.entity.Lecture;
import ua.com.foxminded.university.timetable.Filter;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import static java.lang.String.format;
import static java.util.stream.Collectors.joining;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ua.com.foxminded.university.entity.EntityUtils.parseGroup;
import static ua.com.foxminded.university.entity.EntityUtils.parseTeacher;
import static ua.com.foxminded.university.entity.TimetableUtils.*;

@IntegrationTest
class LectureServiceIT {

    static final String WHERE_ID_EQUALS = "lectures.lecture_id = %d";
    static final String WHERE_NAME_AND_DATE = "lectures.lecture_name = '%s' AND lectures.lecture_date = '%tF'";
    static final String tableName = "university.lectures";

    @Autowired
    LectureService service;

    @Test
    void create_shouldReturnNewEmptyInstance_whenEveryCall() {
        Lecture expected = newLecture(null, "", LocalDateTime.now().plusHours(1), null, null, null);

        Lecture actual = service.create();

        /* Date and time are ignored because test can be execute at transition of days or hours */
        assertThat(actual, samePropertyValuesAs(expected, "date", "beginning", "ending", "duration"));
    }

    @Test
    void add_shouldInsertIntoDbAndReturnTrue_whenInstanceIsNonPersistence() {
        LocalDateTime afterTomorrow = LocalDateTime.now().plusDays(2);
        Lecture instance = newLecture(0, "Herbology", afterTomorrow,
                parseTeacher("1,Minerva McGonagall"), parseGroup("1,GR-11"), null);

        boolean actualResult = service.save(instance);
        int actualNumberOfRows = DaoTestUtils.countRowsInTableWhere(tableName,
                format(WHERE_NAME_AND_DATE, instance.getLectureName(), instance.getLectureDate()));

        assertAll(
                () -> assertTrue(actualResult),
                () -> assertThat(instance.getId(), greaterThan(0)),
                () -> assertThat(actualNumberOfRows, equalTo(1))
        );
    }

    @Test
    void update_shouldUpdateInDbAndReturnTrue_whenInstanceIsPersistence() {
        LocalDateTime afterTomorrow = LocalDateTime.now().plusDays(2);
        Lecture instance = newLecture(1, "Herbology", afterTomorrow,
                parseTeacher("1,Minerva McGonagall"), parseGroup("1,GR-11"), null);
        int previousNumberOfRows = DaoTestUtils.countRowsInTableWhere(tableName,
                format(WHERE_ID_EQUALS, instance.getId()));

        boolean actualResult = service.save(instance);
        int actualNumberOfRows = DaoTestUtils.countRowsInTableWhere(tableName,
                format(WHERE_ID_EQUALS + " AND " + WHERE_NAME_AND_DATE,
                        instance.getId(), instance.getLectureName(), instance.getLectureDate()));

        assertAll(
                () -> assertTrue(actualResult),
                () -> assertThat(previousNumberOfRows, equalTo(1)),
                () -> assertThat(actualNumberOfRows, equalTo(1))
        );
    }

    @Test
    void replace() {
        int id = 1;
        LocalDate today = LocalDate.now();
        LocalDate afterTomorrow = today.plusDays(2);
        Filter filter = Filter.newInstance().withOnlyDrafts();
        Lecture draft = service.tryGetAny(filter.and(l -> l.getId() == id)).orElse(Lecture.newInstance());
        Lecture replacement = Lecture.builder(draft).lectureDate(afterTomorrow).shift(Duration.ofHours(4))
                .teacher(parseTeacher("1,Minerva McGonagall")).build();
        String draftRow = DaoTestUtils.textStreamFromTableWhere(tableName, "lectures.lecture_id = 1")
                .collect(joining(""));

        boolean actualResult = service.replace(draft, replacement);
        String replacedRow = DaoTestUtils.textStreamFromTableWhere(tableName, "lectures.lecture_id = 1")
                .collect(joining(""));

        String expectedDraftRow = format("0|1|Transfiguration|%tF|08:30:00|09:59:59|null|1|1", today);
        String expectedReplacedRow = format("0|1|Transfiguration|%tF|12:30:00|14:00:00|1|1|1", afterTomorrow);
        assertAll(
                () -> assertTrue(actualResult),
                () -> assertThat(draft.getId(), lessThan(0)),
                () -> assertThat(replacement.getId(), equalTo(id)),
                () -> assertThat(draftRow, equalTo(expectedDraftRow)),
                () -> assertThat(replacedRow, equalTo(expectedReplacedRow))
        );
    }

    @Test
    void delete_shouldDeleteFromDbAndReturnTrue_whenInstanceIsPersistence() {
        int id = 1;
        Lecture instance = newLecture(1, "", LocalDateTime.now(), null, null, null);
        int previousNumberOfRows = DaoTestUtils.countRowsInTableWhere(tableName,
                format(WHERE_ID_EQUALS, id));

        boolean actualResult = service.delete(instance);
        int actualNumberOfRows = DaoTestUtils.countRowsInTableWhere(tableName,
                format(WHERE_ID_EQUALS, id));

        assertAll(
                () -> assertTrue(actualResult),
                () -> assertThat(previousNumberOfRows, equalTo(1)),
                () -> assertThat(actualNumberOfRows, equalTo(0)),
                () -> assertThat(instance.getId(), lessThan(0))
        );
    }
}