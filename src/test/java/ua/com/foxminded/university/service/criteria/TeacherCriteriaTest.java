package ua.com.foxminded.university.service.criteria;

import org.junit.jupiter.api.Test;
import ua.com.foxminded.university.entity.Teacher;
import java.util.function.Predicate;
import static org.junit.jupiter.api.Assertions.*;
import static ua.com.foxminded.university.entity.EntityUtils.newTeacher;

class TeacherCriteriaTest {

    @Test
    void byEqualFullName_shouldReturnPredicate_whenEveryCall() {
        String firstName = "Minerva";
        String lastName = "McGonagall";
        Teacher likeTeacher = newTeacher(1, firstName, lastName);
        Teacher notLikeTeacher = newTeacher(2, "Severus", "Snape");

        Predicate<Teacher> actual = TeacherCriteria.byEqualFullName(firstName + " " + lastName);

        assertAll(
                () -> assertTrue(actual.test(likeTeacher)),
                () -> assertFalse(actual.test(notLikeTeacher))
        );
    }
}