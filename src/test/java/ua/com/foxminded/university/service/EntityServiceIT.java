package ua.com.foxminded.university.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import ua.com.foxminded.university.IntegrationTest;
import ua.com.foxminded.university.dao.DaoException;
import ua.com.foxminded.university.dao.EntityRepository;
import ua.com.foxminded.university.dao.RoomQueryFactory;
import ua.com.foxminded.university.entity.Room;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static ua.com.foxminded.university.entity.EntityUtils.newRoom;
import static ua.com.foxminded.university.service.criteria.ResultCollectors.collecting;

@IntegrationTest
class EntityServiceIT {

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplateSpy;
    @Autowired
    EntityRepository<Room> roomRepository;
    @Autowired
    RoomQueryFactory roomQueryFactory;
    @Autowired
    EntityService<Room> roomService;

    @Test
    void getAny_shouldThrowException_whenEntityRepositoryThrowException() {
        DaoException expectedCause = mock(DaoException.class);
        doThrow(expectedCause).when(namedParameterJdbcTemplateSpy)
                .queryForStream(any(String.class), any(SqlParameterSource.class), any());

        ServiceException actualException = assertThrows(ServiceException.class,
                () -> roomService.tryGetAny(roomQueryFactory.selectById(1)));

        String expectedMessage = "Invocation error:";
        assertAll(
                () -> assertThat(actualException.getMessage(), startsWith(expectedMessage)),
                () -> assertThat(actualException.getCause(), isA(DaoException.class)),
                () -> assertThat(actualException.getCause().getCause(), sameInstance(expectedCause))
        );
    }

    @Test
    void getByPredicate_shouldThrowException_whenEntityRepositoryThrowException() {
        DaoException expectedCause = mock(DaoException.class);
        doThrow(expectedCause).when(namedParameterJdbcTemplateSpy)
                .queryForStream(any(String.class), any(SqlParameterSource.class), any());

        ServiceException actualException = assertThrows(ServiceException.class,
                () -> roomService.getAll(collecting(toList())));

        String expectedMessage = "Invocation error:";
        assertAll(
                () -> assertThat(actualException.getMessage(), startsWith(expectedMessage)),
                () -> assertThat(actualException.getCause(), isA(DaoException.class)),
                () -> assertThat(actualException.getCause().getCause(), sameInstance(expectedCause))
        );
    }

    @Test
    void getByQuery_shouldThrowException_whenEntityRepositoryThrowException() {
        DaoException expectedCause = mock(DaoException.class);
        doThrow(expectedCause).when(namedParameterJdbcTemplateSpy)
                .queryForStream(any(String.class), any(SqlParameterSource.class), any());

        ServiceException actualException = assertThrows(ServiceException.class,
                () -> roomService.getByQuery(roomQueryFactory.selectAll(), collecting(toList())));

        String expectedMessage = "Invocation error:";
        assertAll(
                () -> assertThat(actualException.getMessage(), startsWith(expectedMessage)),
                () -> assertThat(actualException.getCause(), isA(DaoException.class)),
                () -> assertThat(actualException.getCause().getCause(), sameInstance(expectedCause))
        );
    }

    @Test
    void save_shouldThrowException_whenMethodAddOfEntityRepositoryThrowException() {
        Room instance = newRoom(null, "Herbology classroom");
        DaoException expectedCause = mock(DaoException.class);
        doThrow(expectedCause).when(namedParameterJdbcTemplateSpy)
                .update(any(String.class), any(SqlParameterSource.class), any());

        ServiceException actualException = assertThrows(ServiceException.class,
                () -> roomService.save(instance));

        String expectedMessage = "Invocation error:";
        assertAll(
                () -> assertThat(actualException.getMessage(), startsWith(expectedMessage)),
                () -> assertThat(actualException.getCause(), isA(DaoException.class)),
                () -> assertThat(actualException.getCause().getCause(), sameInstance(expectedCause))
        );
    }

    @Test
    void save_shouldThrowException_whenMethodReplaceOfEntityRepositoryThrowException() {
        Room instance = newRoom(1, "Herbology classroom");
        DaoException expectedCause = mock(DaoException.class);
        doThrow(expectedCause).when(namedParameterJdbcTemplateSpy)
                .update(any(String.class), any(SqlParameterSource.class));

        ServiceException actualException = assertThrows(ServiceException.class,
                () -> roomService.save(instance));

        String expectedMessage = "Invocation error:";
        assertAll(
                () -> assertThat(actualException.getMessage(), startsWith(expectedMessage)),
                () -> assertThat(actualException.getCause(), isA(DaoException.class)),
                () -> assertThat(actualException.getCause().getCause(), sameInstance(expectedCause))
        );
    }

    @Test
    void delete_shouldThrowException_whenEntityRepositoryThrowException() {
        Room instance = newRoom(1, "Herbology classroom");
        RuntimeException expectedCause = mock(RuntimeException.class);
        doThrow(expectedCause).when(namedParameterJdbcTemplateSpy)
                .update(any(String.class), any(SqlParameterSource.class));

        ServiceException actualException = assertThrows(ServiceException.class,
                () -> roomService.delete(instance));

        String expectedMessage = "Invocation error:";
        assertAll(
                () -> assertThat(actualException.getMessage(), startsWith(expectedMessage)),
                () -> assertThat(actualException.getCause(), isA(DaoException.class)),
                () -> assertThat(actualException.getCause().getCause(), sameInstance(expectedCause))
        );
    }
}