package ua.com.foxminded.university.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ua.com.foxminded.university.IntegrationTest;
import ua.com.foxminded.university.dao.DaoTestUtils;
import ua.com.foxminded.university.entity.Teacher;
import ua.com.foxminded.university.service.criteria.TeacherCriteria;
import java.util.List;
import java.util.Set;
import static java.lang.String.format;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ua.com.foxminded.university.entity.EntityUtils.newTeacher;
import static ua.com.foxminded.university.service.criteria.ResultCollectors.collecting;
import static ua.com.foxminded.university.service.criteria.ResultCollectors.filtering;
import static ua.com.foxminded.university.service.criteria.TeacherCriteria.byEqualFullName;

@IntegrationTest
class TeacherServiceIT {

    static final String WHERE_ID_EQUALS = "teachers.teacher_id = %d";
    static final String WHERE_NAME_EQUALS = "teachers.first_name = '%s' AND teachers.surname = '%s'";
    static final String WHERE_ALL_EQUAL = WHERE_ID_EQUALS + " AND " + WHERE_NAME_EQUALS;
    static final String tableName = "university.teachers";

    @Autowired
    EntityService<Teacher> teacherService;

    @Test
    void create_shouldReturnNewEmptyInstance_whenEveryCall() {
        Teacher expected = newTeacher(null, "", "");

        Teacher actual = teacherService.create();

        assertThat(actual, samePropertyValuesAs(expected));
    }

    @Test
    void getByPredicate_shouldReturnInstancesByPredicate_whenThereAreSuchInstances() {
        Teacher expected = newTeacher(1, "Minerva", "McGonagall");
        String fullName = expected.getFullName();

        List<Teacher> actual = teacherService.getAll(filtering(byEqualFullName(fullName)).collecting(toList()));

        assertAll(
                () -> assertThat(actual, iterableWithSize(1)),
                () -> assertThat(actual, contains(expected))
        );
    }

    @Test
    void getByQuery_shouldReturnInstancesByQuery_whenThereAreSuchInstances() {
        String likeFullName = "Severus%Sn%";
        Teacher expected = newTeacher(2, "Severus", "Snape");

        Set<Teacher> actual = teacherService.getByQuery(TeacherCriteria.byLikeFullName(likeFullName), collecting(toSet()));

        assertAll(
                () -> assertThat(actual, iterableWithSize(1)),
                () -> assertThat(actual, contains(expected))
        );
    }

    @Test
    void save_shouldInsertIntoDbAndReturnTrue_whenInstanceIsNonPersistence() {
        Teacher instance = newTeacher(0, "Quirinus", "Quirrell");

        boolean actualResult = teacherService.save(instance);
        int actualNumberOfRows = DaoTestUtils.countRowsInTableWhere(tableName,
                format(WHERE_NAME_EQUALS, instance.getFirstName(), instance.getSurname()));

        assertAll(
                () -> assertTrue(actualResult),
                () -> assertThat(instance.getId(), greaterThan(0)),
                () -> assertThat(actualNumberOfRows, equalTo(1))
        );
    }

    @Test
    void save_shouldUpdateInDbAndReturnTrue_whenInstanceIsPersistence() {
        Teacher instance = newTeacher(3, "Quirinus", "Quirrell");
        int previousNumberOfRows = DaoTestUtils.countRowsInTableWhere(tableName,
                format(WHERE_NAME_EQUALS, instance.getFirstName(), instance.getSurname()));

        boolean actualResult = teacherService.save(instance);
        int actualNumberOfRows = DaoTestUtils.countRowsInTableWhere(tableName,
                format(WHERE_ALL_EQUAL, instance.getId(), instance.getFirstName(), instance.getSurname()));

        assertAll(
                () -> assertTrue(actualResult),
                () -> assertThat(previousNumberOfRows, equalTo(0)),
                () -> assertThat(actualNumberOfRows, equalTo(1))
        );
    }

    @Test
    void delete_shouldDeleteFromDbAndReturnTrue_whenInstanceIsPersistence() {
        int id = 3;
        Teacher instance = newTeacher(id, "Aurora", "Sinistra");
        int previousNumberOfRows = DaoTestUtils.countRowsInTableWhere(tableName,
                format(WHERE_ID_EQUALS, id));

        boolean actualResult = teacherService.delete(instance);
        int actualNumberOfRows = DaoTestUtils.countRowsInTableWhere(tableName,
                format(WHERE_ID_EQUALS, id));

        assertAll(
                () -> assertTrue(actualResult),
                () -> assertThat(previousNumberOfRows, equalTo(1)),
                () -> assertThat(actualNumberOfRows, equalTo(0)),
                () -> assertThat(instance.getId(), lessThan(0))
        );
    }
}