package ua.com.foxminded.university.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ua.com.foxminded.university.IntegrationTest;
import ua.com.foxminded.university.dao.DaoTestUtils;
import ua.com.foxminded.university.entity.Room;
import ua.com.foxminded.university.service.criteria.RoomCriteria;
import java.util.List;
import java.util.Set;
import static java.lang.String.format;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;
import static ua.com.foxminded.university.entity.EntityUtils.newRoom;
import static ua.com.foxminded.university.service.criteria.ResultCollectors.collecting;
import static ua.com.foxminded.university.service.criteria.ResultCollectors.filtering;
import static ua.com.foxminded.university.service.criteria.RoomCriteria.byEqualName;

@IntegrationTest
class RoomServiceIT {

    static final String WHERE_ID_EQUALS = "rooms.room_id = %d";
    static final String WHERE_NAME_EQUALS = "rooms.room_name = '%s'";
    static final String WHERE_ALL_EQUAL = WHERE_ID_EQUALS + " AND " + WHERE_NAME_EQUALS;
    static final String tableName = "university.rooms";
    static final String[] ignoreFields = {"roomName"};

    @Autowired
    EntityService<Room> roomService;

    @Test
    void create_shouldReturnNewEmptyInstance_whenEveryCall() {
        Room expected = newRoom(null, "");

        Room actual = roomService.create();

        assertThat(actual, samePropertyValuesAs(expected, ignoreFields));
    }

    @Test
    void getByPredicate_shouldReturnInstancesByPredicate_whenThereAreSuchInstances() {
        String name = "Transfiguration classroom";
        Room expected = newRoom(1, name);

        List<Room> actual = roomService.getAll(filtering(byEqualName(name)).collecting(toList()));

        assertAll(
                () -> assertThat(actual, iterableWithSize(1)),
                () -> assertThat(actual, contains(expected))
        );
    }

    @Test
    void getByQuery_shouldReturnInstancesByQuery_whenThereAreSuchInstances() {
        String likeName = "Potions%";
        Room expected = newRoom(2, "Potions classroom");

        Set<Room> actual = roomService.getByQuery(RoomCriteria.byLikeName(likeName), collecting(toSet()));

        assertAll(
                () -> assertThat(actual, iterableWithSize(1)),
                () -> assertThat(actual, contains(expected))
        );
    }

    @Test
    void add_shouldInsertIntoDbAndReturnTrue_whenInstanceIsNonPersistence() {
        Room instance = newRoom(0, "Herbology classroom");

        boolean actualResult = roomService.save(instance);
        int actualNumberOfRows = DaoTestUtils.countRowsInTableWhere(tableName,
                format(WHERE_NAME_EQUALS, instance.getRoomName()));

        assertAll(
                () -> assertTrue(actualResult),
                () -> assertThat(instance.getId(), greaterThan(0)),
                () -> assertThat(actualNumberOfRows, equalTo(1))
        );
    }

    @Test
    void update_shouldUpdateInDbAndReturnTrue_whenInstanceIsPersistence() {
        Room instance = newRoom(2, "Potions laboratory");
        int previousNumberOfRows = DaoTestUtils.countRowsInTableWhere(tableName,
                format(WHERE_NAME_EQUALS, instance.getRoomName()));

        boolean actualResult = roomService.save(instance);
        int actualNumberOfRows = DaoTestUtils.countRowsInTableWhere(tableName,
                format(WHERE_ALL_EQUAL, instance.getId(), instance.getRoomName()));

        assertAll(
                () -> assertTrue(actualResult),
                () -> assertThat(previousNumberOfRows, equalTo(0)),
                () -> assertThat(actualNumberOfRows, equalTo(1))
        );
    }

    @Test
    void delete_shouldDeleteFromDbAndReturnTrue_whenInstanceIsPersistence() {
        int id = 3;
        Room instance = newRoom(id, "Disused classroom");
        int previousNumberOfRows = DaoTestUtils.countRowsInTableWhere(tableName,
                format(WHERE_ID_EQUALS, id));

        boolean actualResult = roomService.delete(instance);
        int actualNumberOfRows = DaoTestUtils.countRowsInTableWhere(tableName,
                format(WHERE_ID_EQUALS, id));

        assertAll(
                () -> assertTrue(actualResult),
                () -> assertThat(previousNumberOfRows, equalTo(1)),
                () -> assertThat(actualNumberOfRows, equalTo(0)),
                () -> assertThat(instance.getId(), lessThan(0))
        );
    }

    @Test /* This is DaoTestUtils test */
    void textFromTable() {

        String actual = DaoTestUtils.textFromTable(tableName);

        String expected = """
                0|1|Transfiguration classroom
                1|2|Potions classroom
                2|3|Disused classroom""";

        assertEquals(expected, actual);
    }
}