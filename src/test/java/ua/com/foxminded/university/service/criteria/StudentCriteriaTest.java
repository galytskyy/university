package ua.com.foxminded.university.service.criteria;

import org.junit.jupiter.api.Test;
import ua.com.foxminded.university.entity.Student;
import java.util.function.Predicate;
import static org.junit.jupiter.api.Assertions.*;
import static ua.com.foxminded.university.entity.EntityUtils.newStudent;

class StudentCriteriaTest {

    @Test
    void byEqualFullName_shouldReturnPredicate_whenEveryCall() {
        String firstName = "Harry";
        String lastName = "Potter";
        Student likeStudent = newStudent(1, firstName, lastName);
        Student notLikeStudent = newStudent(2, "Ronald", "Weasley");

        Predicate<Student> actual = StudentCriteria.byEqualFullName(firstName + " " + lastName);

        assertAll(
                () -> assertTrue(actual.test(likeStudent)),
                () -> assertFalse(actual.test(notLikeStudent))
        );
    }
}