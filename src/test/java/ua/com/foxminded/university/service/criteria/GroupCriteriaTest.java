package ua.com.foxminded.university.service.criteria;

import org.junit.jupiter.api.Test;
import ua.com.foxminded.university.entity.Group;
import java.util.function.Predicate;
import static org.junit.jupiter.api.Assertions.*;
import static ua.com.foxminded.university.entity.EntityUtils.newGroup;

class GroupCriteriaTest {

    @Test
    void byEqualName_shouldReturnPredicate_whenEveryCall() {
        String name = "GR-11";
        Group likeGroup = newGroup(1, name);
        Group notLikeGroup = newGroup(2, "SL-12");

        Predicate<Group> actual = GroupCriteria.byEqualName(name);

        assertAll(
                () -> assertTrue(actual.test(likeGroup)),
                () -> assertFalse(actual.test(notLikeGroup))
        );
    }
}