package ua.com.foxminded.university.service.criteria;

import org.junit.jupiter.api.Test;
import ua.com.foxminded.university.entity.Room;
import java.util.function.Predicate;
import static org.junit.jupiter.api.Assertions.*;
import static ua.com.foxminded.university.entity.EntityUtils.newRoom;

class RoomCriteriaTest {

    @Test
    void byEqualName_shouldReturnPredicate_whenEveryCall() {
        String name = "Transfiguration class room";
        Room likeRoom = newRoom(1, name);
        Room notLikeRoom = newRoom(2, "Potions laboratory");

        Predicate<Room> actual = RoomCriteria.byEqualName(name);

        assertAll(
                () -> assertTrue(actual.test(likeRoom)),
                () -> assertFalse(actual.test(notLikeRoom))
        );
    }
}