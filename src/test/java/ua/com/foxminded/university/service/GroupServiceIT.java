package ua.com.foxminded.university.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ua.com.foxminded.university.IntegrationTest;
import ua.com.foxminded.university.dao.DaoTestUtils;
import ua.com.foxminded.university.entity.Group;
import ua.com.foxminded.university.service.criteria.GroupCriteria;
import java.util.Set;
import static java.lang.String.format;
import static java.util.stream.Collectors.toSet;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ua.com.foxminded.university.entity.EntityUtils.newGroup;
import static ua.com.foxminded.university.service.criteria.GroupCriteria.byEqualName;
import static ua.com.foxminded.university.service.criteria.ResultCollectors.collecting;
import static ua.com.foxminded.university.service.criteria.ResultCollectors.filtering;

@IntegrationTest
class GroupServiceIT {

    static final String WHERE_ID_EQUALS = "groups.group_id = %d";
    static final String WHERE_NAME_EQUALS = "groups.group_name = '%s'";
    static final String WHERE_ALL_EQUAL = WHERE_ID_EQUALS + " AND " + WHERE_NAME_EQUALS;
    static final String tableName = "university.groups";
    static final String[] ignoreFields = {"groupName"};

    @Autowired
    EntityService<Group> groupService;

    @Test
    void create_shouldReturnNewEmptyInstance_whenEveryCall() {
        Group expected = newGroup(null, "");

        Group actual = groupService.create();

        assertThat(actual, samePropertyValuesAs(expected, ignoreFields));
    }

    @Test
    void getByPredicate_shouldReturnInstancesByPredicate_whenThereAreSuchInstances() {
        String name = "GR-11";
        Group expected = newGroup(1, name);

        Set<Group> actual = groupService.getAll(filtering(byEqualName(name)).collecting(toSet()));

        assertAll(
                () -> assertThat(actual, iterableWithSize(1)),
                () -> assertThat(actual, contains(expected))
        );
    }

    @Test
    void getByQuery_shouldReturnInstancesByQuery_whenThereAreSuchInstances() {
        String likeName = "SL%";
        Group expected = newGroup(2, "SL-12");

        Set<Group> actual = groupService.getByQuery(GroupCriteria.byLikeName(likeName), collecting(toSet()));

        assertAll(
                () -> assertThat(actual, iterableWithSize(1)),
                () -> assertThat(actual, contains(expected))
        );
    }

    @Test
    void getAny_shouldReturnInstancesByQuery_whenThereAreSuchInstances() {
        int groupId = 1;
        Group expected = newGroup(1, "GR-11");

        Group actual = groupService.tryGetAny(GroupCriteria.byId(groupId)).orElse(null);

        assertAll(
                () -> assertThat(actual, equalTo(expected)),
                () -> assertThat(actual, samePropertyValuesAs(expected, ignoreFields))
        );
    }

    @Test
    void add_shouldInsertIntoDbAndReturnTrue_whenInstanceIsNonPersistence() {
        Group instance = newGroup(0, "HF-12");

        boolean actualResult = groupService.save(instance);
        int actualNumberOfRows = DaoTestUtils.countRowsInTableWhere(tableName,
                format(WHERE_NAME_EQUALS, instance.getGroupName()));

        assertAll(
                () -> assertTrue(actualResult),
                () -> assertThat(instance.getId(), greaterThan(0)),
                () -> assertThat(actualNumberOfRows, equalTo(1))
        );
    }

    @Test
    void update_shouldUpdateInDbAndReturnTrue_whenInstanceIsPersistence() {
        Group instance = newGroup(2, "SL-14");
        int previousNumberOfRows = DaoTestUtils.countRowsInTableWhere(tableName,
                format(WHERE_NAME_EQUALS, instance.getGroupName()));

        boolean actualResult = groupService.save(instance);
        int actualNumberOfRows = DaoTestUtils.countRowsInTableWhere(tableName,
                format(WHERE_ALL_EQUAL, instance.getId(), instance.getGroupName()));

        assertAll(
                () -> assertTrue(actualResult),
                () -> assertThat(previousNumberOfRows, equalTo(0)),
                () -> assertThat(actualNumberOfRows, equalTo(1))
        );
    }

    @Test
    void delete_shouldDeleteFromDbAndReturnTrue_whenInstanceIsPersistence() {
        int id = 2;
        Group instance = newGroup(id, "SL-12");
        int previousNumberOfRows = DaoTestUtils.countRowsInTableWhere(tableName,
                format(WHERE_ID_EQUALS, id));

        boolean actualResult = groupService.delete(instance);
        int actualNumberOfRows = DaoTestUtils.countRowsInTableWhere(tableName,
                format(WHERE_ID_EQUALS, id));

        assertAll(
                () -> assertTrue(actualResult),
                () -> assertThat(previousNumberOfRows, equalTo(1)),
                () -> assertThat(actualNumberOfRows, equalTo(0)),
                () -> assertThat(instance.getId(), lessThan(0))
        );
    }
}