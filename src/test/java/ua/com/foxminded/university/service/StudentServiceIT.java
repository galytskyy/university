package ua.com.foxminded.university.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ua.com.foxminded.university.IntegrationTest;
import ua.com.foxminded.university.dao.DaoTestUtils;
import ua.com.foxminded.university.entity.Group;
import ua.com.foxminded.university.entity.Student;
import ua.com.foxminded.university.service.criteria.StudentCriteria;
import java.util.Set;
import static java.lang.String.format;
import static java.util.stream.Collectors.toSet;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ua.com.foxminded.university.entity.EntityUtils.newGroup;
import static ua.com.foxminded.university.entity.EntityUtils.newStudent;
import static ua.com.foxminded.university.service.criteria.ResultCollectors.collecting;
import static ua.com.foxminded.university.service.criteria.ResultCollectors.filtering;
import static ua.com.foxminded.university.service.criteria.StudentCriteria.byEqualFullName;

@IntegrationTest
class StudentServiceIT {

    static final String WHERE_ID_EQUALS = "students.student_id = %d";
    static final String WHERE_NAME_EQUALS = "students.first_name = '%s' AND students.surname = '%s'";
    static final String WHERE_ALL_EQUAL = WHERE_ID_EQUALS + " AND " + WHERE_NAME_EQUALS;
    static final String WHERE_STUDENT_ID = "student_group.student_id = %d";
    static final String WHERE_STUDENT_ID_AND_GROUP_ID = WHERE_STUDENT_ID + " AND student_group.group_id = %d";
    static final String tableName = "university.students";
    static final String addTableName = "university.student_group";
    static final String[] ignoreFields = {"groupId"};

    @Autowired
    EntityService<Student> studentService;

    @Test
    void create_shouldReturnNewEmptyInstance_whenEveryCall() {
        Student expected = newStudent(null, "", "");

        Student actual = studentService.create();

        assertThat(actual, samePropertyValuesAs(expected, ignoreFields));
    }

    @Test
    void getByPredicate_shouldReturnInstancesByPredicate_whenThereAreSuchInstances() {
        Student expected = newStudent(1, "Harry", "Potter");
        String fullName = expected.getFullName();

        Set<Student> actual = studentService.getAll(filtering(byEqualFullName(fullName)).collecting(toSet()));

        assertAll(
                () -> assertThat(actual, iterableWithSize(1)),
                () -> assertThat(actual, contains(expected))
        );
    }

    @Test
    void getByQuery_shouldReturnInstancesByQuery_whenThereAreSuchInstances() {
        String likeFullName = "Ron%";
        Student expectedStudent = newStudent(2, "Ronald", "Weasley");
        Group expectedGroup = newGroup(1, "GR-11");
        expectedStudent.setGroup(expectedGroup);

        Set<Student> actual = studentService.getByQuery(StudentCriteria.byLikeFullName(likeFullName), collecting(toSet()));
        Student actualStudent = actual.stream().findAny().orElse(null);

        assertAll(
                () -> assertThat(actual, iterableWithSize(1)),
                () -> assertThat(actualStudent, samePropertyValuesAs(expectedStudent))
        );
    }

    @Test
    void add_shouldInsertIntoDbAndReturnTrue_whenInstanceIsNonPersistence() {
        Student instance = newStudent(0, "Neville", "Longbottom");
        instance.setGroup(newGroup(1, "GR-11"));

        boolean actualResult = studentService.save(instance);
        int actualNumberOfRows = DaoTestUtils.countRowsInTableWhere(tableName,
                format(WHERE_NAME_EQUALS, instance.getFirstName(), instance.getSurname()));
        int actualNumberOfRowsByGroup = DaoTestUtils.countRowsInTableWhere(addTableName,
                format(WHERE_STUDENT_ID_AND_GROUP_ID, instance.getId(), instance.getGroup().getId()));

        assertAll(
                () -> assertTrue(actualResult),
                () -> assertThat(instance.getId(), greaterThan(0)),
                () -> assertThat(actualNumberOfRows, equalTo(1)),
                () -> assertThat(actualNumberOfRowsByGroup, equalTo(1))
        );
    }

    @Test
    void save_shouldInsertIntoDbAndReturnTrue_whenInstanceIsNonPersistence() {
        Student instance = newStudent(0, "Neville", "Longbottom");

        boolean actualResult = studentService.save(instance);
        int actualNumberOfRows = DaoTestUtils.countRowsInTableWhere(tableName,
                format(WHERE_NAME_EQUALS, instance.getFirstName(), instance.getSurname()));

        assertAll(
                () -> assertTrue(actualResult),
                () -> assertThat(instance.getId(), greaterThan(0)),
                () -> assertThat(actualNumberOfRows, equalTo(1))
        );
    }

    @Test
    void save_shouldUpdateInDbAndReturnTrue_whenInstanceIsPersistence() {
        Student instance = newStudent(4, "Neville", "Longbottom");
        instance.setGroup(newGroup(1, "GR-11"));
        int previousNumberOfRows = DaoTestUtils.countRowsInTableWhere(tableName,
                format(WHERE_ALL_EQUAL, instance.getId(), instance.getFirstName(), instance.getSurname()));
        int previousNumberOfRowsByGroup = DaoTestUtils.countRowsInTableWhere(addTableName,
                format(WHERE_STUDENT_ID, instance.getId()));

        boolean actualResult = studentService.save(instance);
        int actualNumberOfStudentRows = DaoTestUtils.countRowsInTableWhere(tableName,
                format(WHERE_ALL_EQUAL, instance.getId(), instance.getFirstName(), instance.getSurname()));
        int actualNumberOfRowsByGroup = DaoTestUtils.countRowsInTableWhere(addTableName,
                format(WHERE_STUDENT_ID_AND_GROUP_ID, instance.getId(), instance.getGroup().getId()));

        assertAll(
                () -> assertTrue(actualResult),
                () -> assertThat("previousNumberOfRows = 0", previousNumberOfRows, equalTo(0)),
                () -> assertThat("previousNumberOfRowsByGroup = 1", previousNumberOfRowsByGroup, equalTo(1)),
                () -> assertThat("actualNumberOfStudentRows = 1", actualNumberOfStudentRows, equalTo(1)),
                () -> assertThat("actualNumberOfRowsByGroup = 1", actualNumberOfRowsByGroup, equalTo(1))
        );
    }

    @Test
    void save_shouldUpdateInDbAndReturnTrue_whenInstanceIsPersistenceAndGroupIsNull() {
        Student instance = newStudent(4, "Neville", "Longbottom");
        int previousNumberOfRows = DaoTestUtils.countRowsInTableWhere(tableName,
                format(WHERE_ALL_EQUAL, instance.getId(), instance.getFirstName(), instance.getSurname()));
        int previousNumberOfRowsByGroup = DaoTestUtils.countRowsInTableWhere(addTableName,
                format(WHERE_STUDENT_ID, instance.getId()));

        boolean actualResult = studentService.save(instance);
        int actualNumberOfStudentRows = DaoTestUtils.countRowsInTableWhere(tableName,
                format(WHERE_ALL_EQUAL, instance.getId(), instance.getFirstName(), instance.getSurname()));
        int actualNumberOfRowsByGroup = DaoTestUtils.countRowsInTableWhere(addTableName,
                format(WHERE_STUDENT_ID, instance.getId()));

        assertAll(
                () -> assertTrue(actualResult),
                () -> assertThat(previousNumberOfRows, equalTo(0)),
                () -> assertThat(previousNumberOfRowsByGroup, equalTo(1)),
                () -> assertThat(actualNumberOfStudentRows, equalTo(1)),
                () -> assertThat(actualNumberOfRowsByGroup, equalTo(1))
        );
    }

    @Test
    void update_shouldUpdateInDbAndReturnTrue_whenInstanceIsPersistence() {
        Student instance = newStudent(4, "Neville", "Longbottom");
        instance.setGroup(newGroup(1, "GR-11"));
        int previousNumberOfRows = DaoTestUtils.countRowsInTableWhere(tableName,
                format(WHERE_ALL_EQUAL, instance.getId(), instance.getFirstName(), instance.getSurname()));
        DaoTestUtils.deleteFromTableWhere(addTableName, format(WHERE_STUDENT_ID, instance.getId()));

        boolean actualResult = studentService.save(instance);
        int actualNumberOfStudentRows = DaoTestUtils.countRowsInTableWhere(tableName,
                format(WHERE_ALL_EQUAL, instance.getId(), instance.getFirstName(), instance.getSurname()));
        int actualNumberOfRowsByGroup = DaoTestUtils.countRowsInTableWhere(addTableName,
                format(WHERE_STUDENT_ID_AND_GROUP_ID, instance.getId(), instance.getGroup().getId()));

        assertAll(
                () -> assertTrue(actualResult),
                () -> assertThat(previousNumberOfRows, equalTo(0)),
                () -> assertThat(actualNumberOfStudentRows, equalTo(1)),
                () -> assertThat(actualNumberOfRowsByGroup, equalTo(1))
        );
    }

    @Test
    void update_shouldUpdateInDbAndReturnTrue_whenInstanceIsPersistenceAndGroupIsNull() {
        Student instance = newStudent(4, "Neville", "Longbottom");
        int previousNumberOfRows = DaoTestUtils.countRowsInTableWhere(tableName,
                format(WHERE_ALL_EQUAL, instance.getId(), instance.getFirstName(), instance.getSurname()));
        DaoTestUtils.deleteFromTableWhere(addTableName, format(WHERE_STUDENT_ID, instance.getId()));

        boolean actualResult = studentService.save(instance);
        int actualNumberOfStudentRows = DaoTestUtils.countRowsInTableWhere(tableName,
                format(WHERE_ALL_EQUAL, instance.getId(), instance.getFirstName(), instance.getSurname()));
        int actualNumberOfRowsByGroup = DaoTestUtils.countRowsInTableWhere(addTableName,
                format(WHERE_STUDENT_ID, instance.getId()));

        assertAll(
                () -> assertTrue(actualResult),
                () -> assertThat(previousNumberOfRows, equalTo(0)),
                () -> assertThat(actualNumberOfStudentRows, equalTo(1)),
                () -> assertThat(actualNumberOfRowsByGroup, equalTo(0))
        );
    }

    @Test
    void delete_shouldDeleteFromDbAndReturnTrue_whenInstanceIsPersistence() {
        int id = 4;
        Student instance = newStudent(id, "Draco", "Malfoy");
        int previousNumberOfRows = DaoTestUtils.countRowsInTableWhere(tableName,
                format(WHERE_ID_EQUALS, id));

        boolean actualResult = studentService.delete(instance);
        int actualNumberOfRows = DaoTestUtils.countRowsInTableWhere(tableName,
                format(WHERE_ID_EQUALS, id));

        assertAll(
                () -> assertTrue(actualResult),
                () -> assertThat(previousNumberOfRows, equalTo(1)),
                () -> assertThat(actualNumberOfRows, equalTo(0)),
                () -> assertThat(instance.getId(), lessThan(0))
        );
    }
}