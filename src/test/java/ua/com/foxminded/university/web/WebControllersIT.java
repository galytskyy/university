package ua.com.foxminded.university.web;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.function.Executable;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ua.com.foxminded.university.controller.dto.SimpleDto;
import ua.com.foxminded.university.dao.DaoException;
import ua.com.foxminded.university.dao.EntityRepository;
import ua.com.foxminded.university.entity.Room;
import ua.com.foxminded.university.entity.Student;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.EnumMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Stream;
import static java.lang.Integer.parseInt;
import static java.lang.String.format;
import static java.util.Objects.nonNull;
import static org.apache.commons.lang3.StringUtils.stripEnd;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ua.com.foxminded.university.dao.DaoTestUtils.countRowsInTableWhere;
import static ua.com.foxminded.university.dao.DaoTestUtils.textFromTableWhere;
import static ua.com.foxminded.university.entity.EntityUtils.*;
import static ua.com.foxminded.university.experimental.Utils.Condition.unless;
import static ua.com.foxminded.university.web.WebControllersIT.Page.Attributes.HEADING;
import static ua.com.foxminded.university.web.WebControllersIT.Page.Attributes.TITLE;

@ActiveProfiles("web-test")
@Sql({"classpath:test-schema.sql", "classpath:test-data.sql"})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.Random.class)
class WebControllersIT {

    @Autowired
    WebDriver chromeDriver;
    String baseUrl;
    @SpyBean
    EntityRepository<Room> roomRepository;
    @LocalServerPort
    int port;
    @Autowired
    MockMvc mockMvc;
    @Autowired
    TestRestTemplate restTemplate;
    @Autowired
    ObjectMapper objectMapper;

    static String getMethodDescriptionTemplate() {
        return format("%s(): %%s", Thread.currentThread().getStackTrace()[2].getMethodName());
    }

    static WebElement findElement(WebDriver webDriver, String xpath) {
        return webDriver.findElement(By.xpath(xpath));
    }

    static List<WebElement> findElements(WebDriver webDriver, String xpath) {
        return webDriver.findElements(By.xpath(xpath));
    }

    static void findElementAndClick(WebDriver webDriver, String xpath) {
        findElement(webDriver, xpath).click();
    }

    static WebElement getElementByLabel(WebDriver webDriver, String xpath) {
        findElementAndClick(webDriver, xpath);
        return webDriver.switchTo().activeElement();
    }

    @BeforeAll
    void beforeAll() {
        baseUrl = format("http://localhost:%d", port);
    }

    @AfterAll
    void afterAll() {
        if (nonNull(chromeDriver)) {
            chromeDriver.quit();
        }
    }

    static class Page {

        Map<Attributes, String> texts = new EnumMap<>(Attributes.class);
        String url;
        String descriptionTemplate = "";

        static Page expected(String url, String descriptionTemplate) {
            Page page = new Page();
            page.setUrl(url);
            page.descriptionTemplate = descriptionTemplate;
            return page;
        }

        static Page actual(WebDriver webDriver) {
            Page page = new Page();
            page.setUrl(webDriver.getCurrentUrl());
            page.texts.put(TITLE, webDriver.getTitle());
            WebElement element = webDriver.findElement(By.id("page-heading"));
            page.texts.put(HEADING, element.getText());
            return page;
        }

        static void acceptAssertsThatIsEqualTo(Stream.Builder<Executable> asserts, Page actual, Page expected) {
            asserts.accept(() -> assertThat(actual.url)
                    .as(expected.descriptionTemplate, "URL")
                    .isEqualTo(expected.url));
            Stream.of(Attributes.values()).forEach((k) -> asserts.accept(() -> assertThat(actual.get(k))
                    .as(expected.descriptionTemplate, k.description)
                    .isEqualTo(expected.get(k))));
        }

        public void setUrl(String url) {
            this.url = stripEnd(url.trim(), "/");
        }

        String get(Attributes key) {
            return this.texts.get(key);
        }

        Page with(String value, Attributes... keys) {
            for (Attributes key : keys) {
                texts.put(key, value);
            }
            return this;
        }

        enum Attributes {
            TITLE("Title"), HEADING("Heading");

            String description;

            Attributes(String description) {
                this.description = description;
            }
        }
    }

    @Nested
    class MainPage {

        @Test
        void mainPage() {
            String description = getMethodDescriptionTemplate();
            String url = baseUrl;
            chromeDriver.navigate().to(url);

            Page actualPage = Page.actual(chromeDriver);

            Page expectedPage = Page.expected(url, description).with("University", TITLE, HEADING);
            Stream.Builder<Executable> asserts = Stream.builder();
            Page.acceptAssertsThatIsEqualTo(asserts, actualPage, expectedPage);
            assertAll(asserts.build());
        }

        @Test
        void navigationMenu() {
            String description = getMethodDescriptionTemplate();
            String url = baseUrl;
            chromeDriver.navigate().to(url);

            WebElement buttonCatalogues = findElement(chromeDriver, "//nav//button[contains(text(),'Catalogues')]");
            WebElement linkToCatalogueOfStudents = findElement(chromeDriver, "//nav//a[text()='Students']");
            unless(linkToCatalogueOfStudents.isDisplayed()).then(buttonCatalogues::click)
                    .inAnyCase(linkToCatalogueOfStudents::click);
            Page actualPage = Page.actual(chromeDriver);

            String expectedUrl = format("%s/students", baseUrl);
            Page expectedPage = Page.expected(expectedUrl, description).with("Students", TITLE, HEADING);
            Stream.Builder<Executable> asserts = Stream.builder();
            Page.acceptAssertsThatIsEqualTo(asserts, actualPage, expectedPage);
            assertAll(asserts.build());
        }
    }

    @Nested
    class StudentPages {

        @Test
        void catalogueOfStudentsPage() {
            String description = getMethodDescriptionTemplate();
            String url = baseUrl;
            chromeDriver.navigate().to(url);

            String linkToCatalogueOfStudentsXPath = "//tbody//td[./a[text()='Students']]";
            findElementAndClick(chromeDriver, linkToCatalogueOfStudentsXPath);
            Page actualPage = Page.actual(chromeDriver);

            String expectedUrl = format("%s/students", baseUrl);
            Page expectedPage = Page.expected(expectedUrl, description).with("Students", TITLE, HEADING);
            Stream.Builder<Executable> asserts = Stream.builder();
            Page.acceptAssertsThatIsEqualTo(asserts, actualPage, expectedPage);
            assertAll(asserts.build());
        }

        @Test
        void studentPage() {
            String description = getMethodDescriptionTemplate();
            String url = format("%s/students", baseUrl);
            chromeDriver.navigate().to(url);

            String linkToStudentPageXPath = "//tbody[@data-app-role='catalogue-items']/tr[.//text()='Hermione Granger']";
            findElementAndClick(chromeDriver, linkToStudentPageXPath);
            Page actualPage = Page.actual(chromeDriver);

            String expectedUrl = format("%s/students/3", baseUrl);
            Page expectedPage = Page.expected(expectedUrl, description).with("Student", TITLE, HEADING);
            Stream.Builder<Executable> asserts = Stream.builder();
            Page.acceptAssertsThatIsEqualTo(asserts, actualPage, expectedPage);
            assertAll(asserts.build());
        }

        @Test
        void findStudentsByLikeFulName() {
            String description = getMethodDescriptionTemplate();
            String url = format("%s/students", baseUrl);
            chromeDriver.navigate().to(url);

            findElement(chromeDriver, "//input[@placeholder='Like full name']").sendKeys("H G");
            findElement(chromeDriver, "//input[@type='submit'][@value='Find']").click();
            Page actualPage = Page.actual(chromeDriver);
            List<WebElement> tableRows = findElements(chromeDriver, "//tbody[@data-app-role='catalogue-items']/tr");
            String actualName = tableRows.get(0).findElement(By.xpath(".//td[2]")).getText();

            String expectedUrl = format("%s/students?likeFullName=H+G", baseUrl);
            String expectedName = "Hermione Granger";
            int expectedNumberOfRows = countRowsInTableWhere("university.students",
                    "students.first_name || ' ' || students.surname LIKE 'H%G%'");
            Page expectedPage = Page.expected(expectedUrl, description).with("Students", TITLE, HEADING);
            Stream.Builder<Executable> asserts = Stream.builder();
            Page.acceptAssertsThatIsEqualTo(asserts, actualPage, expectedPage);
            asserts.accept(() -> assertThat(tableRows).as(description, "Number of rows").hasSize(expectedNumberOfRows));
            asserts.accept(() -> assertThat(actualName).as(description, "Student's name").isEqualTo(expectedName));
            assertAll(asserts.build());
        }

        @Test
        void timetableOfStudent() {
            String description = getMethodDescriptionTemplate();
            String url = format("%s/students", baseUrl);
            chromeDriver.navigate().to(url);

            String linkToTimetableXPath = "//tbody/tr/td[.//text()='Hermione Granger']/following-sibling::td/a";
            findElementAndClick(chromeDriver, linkToTimetableXPath);
            Page actualPage = Page.actual(chromeDriver);

            String expectedUrl = format("%s/timetable/students/3", baseUrl);
            Page expectedPage = Page.expected(expectedUrl, description).with("Timetable", TITLE, HEADING);
            Stream.Builder<Executable> asserts = Stream.builder();
            Page.acceptAssertsThatIsEqualTo(asserts, actualPage, expectedPage);
            assertAll(asserts.build());
        }

        @Test
        void createStudent() {
            String description = getMethodDescriptionTemplate();
            String url = format("%s/students", baseUrl);
            chromeDriver.navigate().to(url);

            String buttonNewXPath = "//*[contains(@class, 'btn')][text()='New']";
            findElementAndClick(chromeDriver, buttonNewXPath);
            getElementByLabel(chromeDriver, "//label[text()='First name']").sendKeys("Parvati");
            getElementByLabel(chromeDriver, "//label[text()='Surname']").sendKeys("Patil");
            new Select(getElementByLabel(chromeDriver, "//label[text()='Group']")).selectByVisibleText("GR-11");
            findElementAndClick(chromeDriver, "//input[@value='Save']");
            Page actualPage = Page.actual(chromeDriver);
            String actualId = getElementByLabel(chromeDriver, "//label[text()='ID']").getAttribute("value");
            String actualRow = getStudentRowFromDbById(parseInt(actualId));

            String expectedRow = format("0|%1$s|Parvati|Patil|&|0|%1$s|1", actualId);
            String expectedUrl = format("%s/students/%s", baseUrl, actualId);
            Page expectedPage = Page.expected(expectedUrl, description).with("Student", TITLE, HEADING);
            Stream.Builder<Executable> asserts = Stream.builder();
            Page.acceptAssertsThatIsEqualTo(asserts, actualPage, expectedPage);
            asserts.accept(() -> assertThat(actualRow).as(description, "Row in DB").isEqualTo(expectedRow));
            assertAll(asserts.build());
        }

        @Test
        void readStudent() {
            String description = getMethodDescriptionTemplate();
            String url = format("%s/students", baseUrl);
            chromeDriver.navigate().to(url);

            String linkToStudentPageXPath = "//tbody[@data-app-role='catalogue-items']/tr[.//text()='Hermione Granger']";
            findElementAndClick(chromeDriver, linkToStudentPageXPath);
            Student actualStudent = newStudent(
                    parseInt(getElementByLabel(chromeDriver, "//label[text()='ID']").getAttribute("value")),
                    getElementByLabel(chromeDriver, "//label[.='First name']").getAttribute("value"),
                    getElementByLabel(chromeDriver, "//label[.='Surname']").getAttribute("value"));
            Select selectGroup = new Select(getElementByLabel(chromeDriver, "//label[text()='Group']"));
            actualStudent.setGroup(newGroup(
                    parseInt(selectGroup.getFirstSelectedOption().getAttribute("value")),
                    selectGroup.getFirstSelectedOption().getText()));
            findElementAndClick(chromeDriver, "//form//*[contains(@class,'btn')][text()='Close']");
            Page actualPage = Page.actual(chromeDriver);
            String actualRow = studentAsDbRow(actualStudent);

            String expectedRow = getStudentRowFromDbById(3);
            Student expectedStudent = parseStudent("3,Hermione Granger", "1,GR-11");
            String expectedUrl = format("%s/students", baseUrl);
            Page expectedPage = Page.expected(expectedUrl, description).with("Students", TITLE, HEADING);
            Stream.Builder<Executable> asserts = Stream.builder();
            Page.acceptAssertsThatIsEqualTo(asserts, actualPage, expectedPage);
            asserts.accept(() -> assertThat(actualStudent).as(description, "Student").usingRecursiveComparison()
                    .isEqualTo(expectedStudent));
            asserts.accept(() -> assertThat(actualRow).as(description, "Row in DB").isEqualTo(expectedRow));
            assertAll(asserts.build());
        }

        @Test
        @Disabled("Disabled because 'DevTools failed to load source map: ... Load canceled due to load timeout'")
        /* TODO
        Disabled because 'DevTools failed to load source map:
        Could not load content for http://localhost:XXXXX/js/bootstrap.bundle.min.js.map:
        Load canceled due to load timeout' */
        void studentListOnGroupForm() {
            String description = getMethodDescriptionTemplate();
            int groupId = 1;
            String url = format("%s/groups/%d", baseUrl, groupId);
            chromeDriver.navigate().to(url);

            Page actualPage = Page.actual(chromeDriver);
            findElementAndClick(chromeDriver, "//button[@role='tab'][contains(.,'Students')]");
            List<WebElement> actualItems = new WebDriverWait(chromeDriver, 5L)
                    .until(driver -> findElements(driver, "//table[@data-app-id='students-table']/tbody/tr"));

            int expectedSize = countRowsInTableWhere(
                    "university.student_group", format("student_group.group_id = %d", groupId));
            Page expectedPage = Page.expected(url, description).with("Group", TITLE, HEADING);
            Stream.Builder<Executable> asserts = Stream.builder();
            Page.acceptAssertsThatIsEqualTo(asserts, actualPage, expectedPage);
            asserts.accept(() -> assertThat(actualItems).as(description, "Number of Students").hasSize(expectedSize));
            assertAll(asserts.build());
        }

        @Test
        void updateStudent() {
            String description = getMethodDescriptionTemplate();
            int studentId = 5;
            String rowBeforeAct = getStudentRowFromDbById(studentId);
            String url = format("%s/students/%d", baseUrl, studentId);
            chromeDriver.navigate().to(url);

            getElementByLabel(chromeDriver, "//label[text()='First name']").sendKeys(Keys.chord(Keys.CONTROL, "a"), "Neville");
            getElementByLabel(chromeDriver, "//label[text()='Surname']").sendKeys(Keys.chord(Keys.CONTROL, "a"), "Longbottom");
            WebElement groupSelect = getElementByLabel(chromeDriver, "//label[text()='Group']");
            groupSelect.click();
            groupSelect.findElement(By.xpath("./option[text()='GR-11']")).click();
            findElementAndClick(chromeDriver, "//input[@value='Save']");
            Page actualPage = Page.actual(chromeDriver);
            String actualRow = getStudentRowFromDbById(studentId);

            String expectedRow = format("0|%1$d|Neville|Longbottom|&|0|%1$d|1", studentId);
            String expectedUrl = format("%s/students/%d", baseUrl, studentId);
            Page expectedPage = Page.expected(expectedUrl, description).with("Student", TITLE, HEADING);
            Stream.Builder<Executable> asserts = Stream.builder();
            Page.acceptAssertsThatIsEqualTo(asserts, actualPage, expectedPage);
            asserts.accept(() -> assertThat(actualRow).as(description, "Row in DB").isEqualTo(expectedRow));
            asserts.accept(() -> assertThat(actualRow).as(description, "Row in DB").isNotEqualTo(rowBeforeAct));
            assertAll(asserts.build());
        }

        @Test
        void deleteStudent() {
            String description = getMethodDescriptionTemplate();
            String url = format("%s/students/4", baseUrl);
            chromeDriver.navigate().to(url);

            findElementAndClick(chromeDriver, "//input[@value='Delete']");
            Page actualPage = Page.actual(chromeDriver);
            int[] actualNumberOfRows = countStudentRowsInDbById(4);

            String expectedUrl = format("%s/students/4", baseUrl);
            Page expectedPage = Page.expected(expectedUrl, description).with("Student", TITLE, HEADING);
            Stream.Builder<Executable> asserts = Stream.builder();
            Page.acceptAssertsThatIsEqualTo(asserts, actualPage, expectedPage);
            asserts.accept(() -> assertThat(actualNumberOfRows).as(description, "Number of rows").containsOnly(0));
            assertAll(asserts.build());
        }

        String studentAsDbRow(Student student) {
            return format("0|%1$s|%2$s|%3$s|&|0|%1$s|%4$s",
                    student.getStudentId(), student.getFirstName(), student.getSurname(), student.getGroupId());
        }

        int[] countStudentRowsInDbById(int id) {
            return new int[]{countRowsInTableWhere("university.students", format("students.student_id = %d", id)),
                    countRowsInTableWhere("university.student_group", format("student_group.student_id = %d", id))};
        }

        String getStudentRowFromDbById(int id) {
            return textFromTableWhere("university.students", format("students.student_id = %d", id)) + "|&|" +
                    textFromTableWhere("university.student_group", format("student_group.student_id = %d", id));
        }
    }

    @Nested
    class ErrorPages {

        @Test
        void nonFoundPage() {
            String description = getMethodDescriptionTemplate();
            String url = format("%s/students/12345", baseUrl);
            chromeDriver.navigate().to(url);

            Page actualPage = Page.actual(chromeDriver);

            Page expectedPage = Page.expected(url, description).with("404", TITLE).with("Error 404", HEADING);
            Stream.Builder<Executable> asserts = Stream.builder();
            Page.acceptAssertsThatIsEqualTo(asserts, actualPage, expectedPage);
            assertAll(asserts.build());
        }

        @Test
        void badRequestPage() {
            String description = getMethodDescriptionTemplate();
            String dateInBadFormat = "24.02.2022";
            String url = format("%s/lectures?filter=true&dateFrom=%s", baseUrl, dateInBadFormat);

            chromeDriver.navigate().to(url);
            Page actualPage = Page.actual(chromeDriver);

            Page expectedPage = Page.expected(url, description).with("400", TITLE).with("Error 400", HEADING);
            Stream.Builder<Executable> asserts = Stream.builder();
            Page.acceptAssertsThatIsEqualTo(asserts, actualPage, expectedPage);
            assertAll(asserts.build());
        }

        @Test
        void internalServerErrorPage() {
            String description = getMethodDescriptionTemplate();
            doThrow(new DaoException("Oops", null)).when(roomRepository).findAll();
            String url = format("%s/rooms", baseUrl);
            chromeDriver.navigate().to(url);

            Page actualPage = Page.actual(chromeDriver);

            Page expectedPage = Page.expected(url, description).with("500", TITLE).with("Error 500", HEADING);
            Stream.Builder<Executable> asserts = Stream.builder();
            Page.acceptAssertsThatIsEqualTo(asserts, actualPage, expectedPage);
            assertAll(asserts.build());
        }
    }

    @Nested
    class TimetablePages {

        @Test
        void timetableMainPage() {
            String description = getMethodDescriptionTemplate();
            String url = baseUrl;
            chromeDriver.navigate().to(url);

            String linkToCatalogueOfStudentsXPath = "//tbody//td[./a[text()='Timetable']]";
            findElementAndClick(chromeDriver, linkToCatalogueOfStudentsXPath);
            Page actualPage = Page.actual(chromeDriver);

            String expectedUrl = format("%s/timetable", baseUrl);
            Page expectedPage = Page.expected(expectedUrl, description).with("Timetable", TITLE, HEADING);
            Stream.Builder<Executable> asserts = Stream.builder();
            Page.acceptAssertsThatIsEqualTo(asserts, actualPage, expectedPage);
            assertAll(asserts.build());
        }

        @Test
        void studentMonthlyTimetablePage() {
            String description = getMethodDescriptionTemplate();
            String url = format("%s/timetable/students/3", baseUrl);
            chromeDriver.navigate().to(url);

            Page actualPage = Page.actual(chromeDriver);
            String actualSubheading = findElement(chromeDriver, "//main//*[contains(text(),'Student:')]").getText();
            WebElement actualButton = findElement(chromeDriver, "//main//button[contains(text(),'Month')]");
            String nowYearMonth = YearMonth.now().format(DateTimeFormatter.ofPattern("MMMM yyyy", Locale.US));
            String periodXpath = format("//main//*[contains(text(),'%s')]", nowYearMonth);
            WebElement actualPeriod = findElement(chromeDriver, periodXpath);

            String expectedSubheading = "Student: Hermione Granger";
            Page expectedPage = Page.expected(url, description).with("Timetable", TITLE, HEADING);
            Stream.Builder<Executable> asserts = Stream.builder();
            Page.acceptAssertsThatIsEqualTo(asserts, actualPage, expectedPage);
            asserts.accept(() -> assertThat(actualSubheading).as(description, "Subheading")
                    .isEqualTo(expectedSubheading));
            asserts.accept(() -> assertThat(actualButton).as(description, "Button of choice period").isNotNull());
            asserts.accept(() -> assertThat(actualPeriod).as(description, "Period info").isNotNull());
            assertAll(asserts.build());
        }

        @Test
        void studentWeeklyTimetablePage() {
            String description = getMethodDescriptionTemplate();
            String url = format("%s/timetable/students/3", baseUrl);
            chromeDriver.navigate().to(url);

            String firstWeekXPath = "//main//div[@data-app-week='1']";
            findElementAndClick(chromeDriver, firstWeekXPath);
            Page actualPage = Page.actual(chromeDriver);
            String actualSubheading = findElement(chromeDriver, "//main//*[contains(text(),'Student:')]").getText();
            WebElement actualButton = findElement(chromeDriver, "//main//button[contains(text(),'Week')]");

            String expectedSubheading = "Student: Hermione Granger";
            Page expectedPage = Page.expected(url, description).with("Timetable", TITLE, HEADING);
            Stream.Builder<Executable> asserts = Stream.builder();
            Page.acceptAssertsThatIsEqualTo(asserts, actualPage, expectedPage);
            asserts.accept(() -> assertThat(actualSubheading).as(description, "Subheading").isEqualTo(expectedSubheading));
            asserts.accept(() -> assertThat(actualButton).as(description, "Button of choice period").isNotNull());
            assertAll(asserts.build());
        }

        @Test
        void studentDailyTimetablePage() {
            String description = getMethodDescriptionTemplate();
            String url = format("%s/timetable/students/3", baseUrl);
            chromeDriver.navigate().to(url);

            LocalDate today = LocalDate.now();
            String todayISO = today.format(DateTimeFormatter.ISO_LOCAL_DATE);
            String dayXpath = format("//main//div[@data-app-month-day][@data-app-date='%s']", todayISO);
            findElementAndClick(chromeDriver, dayXpath);
            Page actualPage = Page.actual(chromeDriver);
            String actualSubheading = findElement(chromeDriver, "//main//*[contains(text(),'Student:')]")
                    .getText();
            WebElement actualButton = findElement(chromeDriver, "//main//button[contains(text(),'Day')]");
            String todayString = today.format(DateTimeFormatter.ofPattern("MMMM dd, yyyy", Locale.US));
            String periodXpath = format("//main//*[contains(text(),'%s')]", todayString);
            WebElement actualPeriod = findElement(chromeDriver, periodXpath);

            String expectedSubheading = "Student: Hermione Granger";
            Page expectedPage = Page.expected(url, description).with("Timetable", TITLE, HEADING);
            Stream.Builder<Executable> asserts = Stream.builder();
            Page.acceptAssertsThatIsEqualTo(asserts, actualPage, expectedPage);
            asserts.accept(() -> assertThat(actualSubheading).as(description, "Subheading").isEqualTo(expectedSubheading));
            asserts.accept(() -> assertThat(actualButton).as(description, "Button of choice period").isNotNull());
            asserts.accept(() -> assertThat(actualPeriod).as(description, "Period info").isNotNull());
            assertAll(asserts.build());
        }
    }

    @Nested
    class RestMethods {

        @Test
        void getStudentsOfGroup() throws Exception {
            List<SimpleDto> listOfStudents = List.of(
                    SimpleDto.of(3, "Hermione Granger"),
                    SimpleDto.of(1, "Harry Potter"),
                    SimpleDto.of(2, "Ronald Weasley"));

            mockMvc.perform(MockMvcRequestBuilders.get("/groups/1/students"))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.total").value(listOfStudents.size()))
                    .andExpect(jsonPath("$.count").value(listOfStudents.size()))
                    .andExpect(jsonPath("$.order.name").value("asc"))
                    .andExpect(jsonPath("$.rows").isArray())
                    .andExpect(jsonPath("$.rows", hasSize(listOfStudents.size())));
        }

        @Test
        void getStudentsByGroupId() throws Exception {
            String url = format("%s/students/groups/%d", baseUrl, 1);
            List<SimpleDto> listOfStudents = List.of(
                    SimpleDto.of(3, "Hermione Granger"),
                    SimpleDto.of(1, "Harry Potter"),
                    SimpleDto.of(2, "Ronald Weasley"));
            JsonNode arrayOfStudents = objectMapper.valueToTree(listOfStudents);

            JsonNode jsonNode = objectMapper.readTree(restTemplate.getForObject(url, String.class));

            assertAll(
                    () -> assertThat(jsonNode.get("total").asInt()).isEqualTo(listOfStudents.size()),
                    () -> assertThat(jsonNode.get("count").asInt()).isEqualTo(listOfStudents.size()),
                    () -> assertThat(jsonNode.get("order").get("name").textValue()).isEqualTo("asc"),
                    () -> assertThat(jsonNode.get("rows").isArray()).isTrue(),
                    () -> assertThat(jsonNode.get("rows").size()).isEqualTo(listOfStudents.size()),
                    () -> assertThat(jsonNode.get("rows")).isEqualTo(arrayOfStudents)
            );
        }
    }
}