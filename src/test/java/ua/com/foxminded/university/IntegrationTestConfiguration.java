package ua.com.foxminded.university;

import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import javax.sql.DataSource;

@TestConfiguration
@ActiveProfiles("test")
@ComponentScan({"ua.com.foxminded.university"})
@SpyBean(NamedParameterJdbcTemplate.class)
public class IntegrationTestConfiguration {

}
