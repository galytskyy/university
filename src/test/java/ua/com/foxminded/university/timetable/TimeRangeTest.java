package ua.com.foxminded.university.timetable;

import org.junit.jupiter.api.Test;
import java.time.Duration;
import java.time.LocalTime;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

class TimeRangeTest {

    @Test
    void getDuration_shouldReturnDurationWithInclusiveEndingOfRange_whenEveryCall() {
        LocalTime beginning = LocalTime.of(0, 1);
        LocalTime ending = LocalTime.of(1, 2);
        TimeRange timeRange = TimeRange.between(beginning, ending);

        Duration actual = timeRange.getDuration();

        Duration expected = Duration.ofMinutes(62);
        assertThat(actual, equalTo(expected));
    }
}