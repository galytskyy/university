package ua.com.foxminded.university.timetable;

import org.junit.jupiter.api.Test;
import java.time.LocalDate;
import java.time.YearMonth;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.*;

class DateRangeTest {

    @Test
    void ofType_shouldReturnDataRange_whenArgumentsAreDateRangeTypeAndDateIsFromRange() {
        LocalDate today = LocalDate.now();
        LocalDate startOfWeek = today.minusDays(today.getDayOfWeek().getValue() - 1);

        DateRange actual = DateRange.ofType(DateRangeType.WEEK, today);

        DateRange expected = DateRange.of(startOfWeek, DateRangeType.WEEK);
        assertThat(actual, equalTo(expected));
    }

    @Test
    void ofType_shouldReturnDataRange_whenArgumentIsTypicalDateRange() {
        LocalDate tomorrow = LocalDate.now().plusDays(1);

        DateRange actual = DateRange.ofType(TypicalDateRange.TOMORROW);

        DateRange expected = DateRange.ofType(DateRangeType.DAY, tomorrow);
        assertThat(actual, equalTo(expected));
    }

    @Test
    void of_shouldReturnDataRange_whenArgumentsAreDateFromAndDateRangeType() {
        LocalDate fromDate = LocalDate.of(2022, 1, 31);

        DateRange actual = DateRange.of(fromDate, DateRangeType.MONTH);

        DateRange expected = DateRange.between(fromDate, LocalDate.of(2022, 2, 28));
        assertThat(actual, equalTo(expected));
    }

    @Test
    void of_shouldReturnDataRange_whenArgumentIsSomeMonth() {
        YearMonth currentMonth = YearMonth.now();

        DateRange actual = DateRange.of(currentMonth);

        DateRange expected = DateRange.between(currentMonth.atDay(1), currentMonth.atEndOfMonth());
        assertThat(actual, equalTo(expected));
    }

    @Test
    void contains_shouldReturnTrue_whenDateInBoundOfRange() {
        YearMonth currentMonth = YearMonth.now();
        LocalDate date1 = currentMonth.atDay(8);
        LocalDate date2 = currentMonth.atDay(17);
        DateRange dateRange = DateRange.between(date1, date2);

        boolean actual1 = dateRange.contains(date1);
        boolean actual2 = dateRange.contains(date2);

        assertAll(
                () -> assertTrue(actual1),
                () -> assertTrue(actual2)
        );
    }

    @Test
    void contains_shouldReturnFalse_whenDateOutBoundOfRange() {
        YearMonth currentMonth = YearMonth.now();
        LocalDate date1 = currentMonth.atDay(8);
        LocalDate date2 = currentMonth.atDay(17);
        DateRange dateRange = DateRange.between(date1, date2);
        date1 = date1.minusDays(1);
        date2 = date2.plusDays(1);

        boolean actual1 = dateRange.contains(date1);
        boolean actual2 = dateRange.contains(date2);

        assertAll(
                () -> assertFalse(actual1),
                () -> assertFalse(actual2)
        );
    }
}