package ua.com.foxminded.university.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ua.com.foxminded.university.IntegrationTest;
import ua.com.foxminded.university.controller.dto.LectureDto;
import ua.com.foxminded.university.controller.dto.Page;
import ua.com.foxminded.university.dao.DaoTestUtils;
import static java.util.Collections.emptyMap;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

@IntegrationTest
class LectureControllerIT {

    @Autowired
    LectureController lectureController;

    @Test
    void buildPage_shouldReturnEmptyPage_whenIndexOfPageIsGreaterThatMaxAvailableIndex() {
        int index = 10;

        Page<LectureDto> page = lectureController.buildPage(index, emptyMap());

        int expectedTotal = DaoTestUtils.countRowsInTable("university.lectures");
        int expectedSize = LectureController.PER_PAGE;
        int expectedMaxIndex = expectedTotal / expectedSize + Integer.min(1, expectedTotal % expectedSize);
        int expectedCount = 0;
        assertAll(
                () -> assertThat(index).isGreaterThan(expectedMaxIndex),
                () -> assertThat(page.getIndex()).as("Index of page isn't expected").isEqualTo(index),
                () -> assertThat(page.getTotal()).as("Total count isn't expected").isEqualTo(expectedTotal),
                () -> assertThat(page.getSize()).as("Size of page isn't expected").isEqualTo(expectedSize),
                () -> assertThat(page.getCount()).as("Count isn't expected").isEqualTo(expectedCount),
                () -> assertThat(page.getRows()).as("Number of rows isn't expected").hasSize(expectedCount),
                () -> assertThat(page.getRows().size()).isEqualTo(expectedCount)
        );
    }
}