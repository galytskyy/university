package ua.com.foxminded.university.experimental;

public class Utils {

    private Utils() {
    }

    public static class Condition {

        boolean assumption;

        private Condition(boolean assumption) {
            this.assumption = assumption;
        }

        public static Condition assume(boolean condition) {
            return new Condition(condition);
        }

        public static Condition unless(boolean condition) {
            return new Condition(no(condition));
        }

        public static boolean and(boolean arg1, boolean arg2, boolean... args) {
            for (boolean arg : args) {
                if (no(arg)) {
                    return false;
                };
            }
            return arg1 && arg2;
        }

        public static boolean or(boolean arg1, boolean arg2, boolean... args) {
            for (boolean arg : args) {
                if (arg) {
                    return true;
                };
            }
            return arg1 || arg2;
        }

        public static boolean no(boolean arg) {
            return !arg;
        }

        public Condition andIf(boolean arg1, boolean... args) {
            return new Condition(and(this.assumption, arg1, args));
        }

        public Condition orIf(boolean arg1, boolean... args) {
            return new Condition(or(this.assumption, arg1, args));
        }

        public Condition then(Runnable action) {
            if (assumption) {
                action.run();
            }
            return new Condition(this.assumption);
        }

        public Condition otherwise(Runnable action) {
            if (!assumption) {
                action.run();
            }
            return new Condition(this.assumption);
        }

        public void inAnyCase(Runnable action) {
            action.run();
        }

        public Condition otherwiseAssume(boolean value) {
            return new Condition(no(this.assumption) && value);
        }

        public Condition otherwiseUnless(boolean value) {
            return new Condition(no(this.assumption) && no(value));
        }
    }
}
