package ua.com.foxminded.university.experimental;

import java.util.function.Function;

public class Second {

    private static final Function<Second, First> firstConstructor = FirstConfidant.getFirstConstructor();
    private final First first;
    private int something;

    public Second() {
        this.first = firstConstructor.apply(this);
    }

    /* This is private constructor */
    private Second(First first) {
        this.first = first;
    }

    public static void takeConstructor(First.SecondConfidant secondConfidant) {
        /* Trust private methods */
        secondConfidant.giveMethods(Second::new, Second::setSomething);
    }

    /* This is private setter */
    private void setSomething(int something) {
        this.something = something;
    }

    /* The class is final that cannot be inherited */
    public static final class FirstConfidant {

        /* This is private constructor that could not be created from the outside */
        private FirstConfidant() {
        }

        private static Function<Second, First> getFirstConstructor() {
            return First.takeMethods(new FirstConfidant());
        }
    }
}
