package ua.com.foxminded.university.experimental;

import java.util.function.BiConsumer;
import java.util.function.Function;

public class First {

    private static final SecondConfidant SECOND_CONFIDANT = new SecondConfidant();
    private final Second second;

    public First() {
        this.second = SECOND_CONFIDANT.create(this);
    }

    /* This is private constructor */
    private First(Second second) {
        this.second = second;
    }

    public static Function<Second, First> takeMethods(Second.FirstConfidant firstConfidant) {
        /* Return a private method */
        return First::new;
    }

    /* This is private method */
    public void setSomething(int value) {
        SECOND_CONFIDANT.setSomething(this.second, value);
    }

    /* The class is final that cannot be inherited */
    public static final class SecondConfidant {

        private Function<First, Second> constructor;
        private BiConsumer<Second, Integer> somethingSetter;

        /* This is private constructor that could not be created from the outside */
        private SecondConfidant() {
            Second.takeConstructor(this);
        }

        public void giveMethods(Function<First, Second> constructor, BiConsumer<Second, Integer> somethingSetter) {
            this.constructor = constructor;
            this.somethingSetter = somethingSetter;
        }

        private Second create(First first) {
            return constructor.apply(first);
        }

        private void setSomething(Second second, int value) {
            somethingSetter.accept(second, value);
        }
    }
}
