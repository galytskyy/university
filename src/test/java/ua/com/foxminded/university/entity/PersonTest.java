package ua.com.foxminded.university.entity;

import org.junit.jupiter.api.Test;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;
import static ua.com.foxminded.university.entity.EntityUtils.newStudent;
import static ua.com.foxminded.university.entity.EntityUtils.newTeacher;

class PersonTest {

    @Test
    void compareTo_shouldReturnZero_whenObjectsAreEquals() {
        Person person1 = newStudent(1, "Harry", "Potter");
        Person person2 = newStudent(1, "Harry", "Potter");

        assertAll(
                () -> assertNotSame(person1, person2),
                () -> assertEquals(person1, person2),
                () -> assertThat(person1.compareTo(person2), is(0))
        );
    }

    @Test
    void compareTo_shouldReturnLessZero_whenObjectsAreNotEquals() {
        Person person1 = newStudent(1, "Harry", "Potter");
        Person person2 = newTeacher(5, "Ginevra", "Weasley");

        assertAll(
                () -> assertNotSame(person1, person2),
                () -> assertNotEquals(person1, person2),
                () -> assertThat(person1.compareTo(person2), lessThan(0))
        );
    }

    @Test
    void compareTo_shouldReturnGreaterZero_whenObjectsAreNotEquals() {
        Person person1 = newStudent(1, "Harry", "Potter");
        Person person2 = newStudent(3, "Hermione", "Granger");

        assertAll(
                () -> assertNotSame(person1, person2),
                () -> assertNotEquals(person1, person2),
                () -> assertThat(person1.compareTo(person2), greaterThan(0))
        );
    }
}