package ua.com.foxminded.university.entity;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertAll;
import static ua.com.foxminded.university.entity.EntityUtils.newGroup;

class GroupTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void equals_shouldReturnTrue_whenIdsAreEqualAndIdsAreGreaterThanZero() {
        Group[] groups = {
                newGroup(1, "GR-11"),
                newGroup(2, "GR-11"),
                newGroup(1, "SL-12"),
                newGroup(2, "GR-11")};

        assertAll(
                () -> assertThat(groups[2], equalTo(groups[0])),
                () -> assertThat(groups[3], equalTo(groups[1]))
        );
    }

    @Test
    void equals_shouldReturnFalse_whenIdsAreNotEqualOrIdsAreNotGreaterThanZero() {
        Group[] groups = {
                newGroup(1, "GR-11"),
                newGroup(0, "GR-11"),
                newGroup(0, "GR-11"),
                newGroup(null, "GR-11"),
                newGroup(-1, "GR-11")};

        /* no action */

        assertAll(
                () -> assertThat(groups[1], not(equalTo(groups[0]))),
                () -> assertThat(groups[2], not(equalTo(groups[1]))),
                () -> assertThat(groups[3], not(equalTo(groups[2]))),
                () -> assertThat(groups[4], not(equalTo(groups[3])))
        );
    }

    @Test
    void hashCode_shouldReturnValueOfId_whenIdIsGreaterThanZero() {
        Group[] groups = {
                newGroup(1, "GR-11"),
                newGroup(2, "GR-11")};

        /* no action */

        assertAll(
                () -> assertThat(groups[0].hashCode(), equalTo(groups[0].getId())),
                () -> assertThat(groups[1].hashCode(), equalTo(groups[1].getId()))
        );
    }

    @Test
    void hashCode_shouldReturnIdentityHashCode_whenIdIsLessOrEqualsZero() {
        Group[] groups = {
                newGroup(0, "GR-11"),
                newGroup(-1, "GR-11"),
                newGroup(null, "GR-11")};

        /* no action */

        assertAll(
                () -> assertThat(groups[0].hashCode(), not(equalTo(groups[0].getId()))),
                () -> assertThat(groups[1].hashCode(), not(equalTo(groups[1].getId()))),
                () -> assertThat(groups[2].hashCode(), not(equalTo(groups[2].getId())))
        );
    }
}