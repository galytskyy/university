package ua.com.foxminded.university.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Optional;
import static java.util.Objects.isNull;
import static ua.com.foxminded.university.entity.EntityUtils.*;

public class TimetableUtils {

    public static Lecture newLecture(Integer id, String name, LocalDate date, LocalTime beginning, LocalTime ending,
                                     Teacher teacher, Group group, Room room) {
        Lecture lecture = Lecture.newInstance();
        lecture.lectureId = id;
        lecture.lectureName = name;
        lecture.lectureDate = date;
        lecture.beginning = beginning;
        lecture.ending = ending;
        lecture.teacher = teacher;
        lecture.group = group;
        lecture.room = room;
        return lecture;
    }

    public static Lecture newLecture(Integer id, String name, LocalDateTime beginningDateTime,
                                     Teacher teacher, Group group, Room room) {
        return newLecture(id, name, beginningDateTime.toLocalDate(), beginningDateTime.toLocalTime(),
                beginningDateTime.toLocalTime().plusHours(1).minusNanos(1), teacher, group, room);
    }

    public static Lecture newLecture(String id, String name, String date, String beginning, String ending,
                                     String teacher, String group, String room) {
        Integer lectureId = isNull(id) ? null : Integer.valueOf(id);
        return newLecture(lectureId, name, LocalDate.parse(date), LocalTime.parse(beginning), LocalTime.parse(ending),
                parseTeacher(teacher), parseGroup(group), parseRoom(room));
    }
}

