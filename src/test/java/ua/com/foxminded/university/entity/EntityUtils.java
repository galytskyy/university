package ua.com.foxminded.university.entity;

import static java.util.Objects.isNull;

public class EntityUtils {

    public static Group newGroup(Integer id, String name) {
        Group group = Group.newInstance();
        group.setGroupId(id);
        group.setGroupName(name);
        return group;
    }

    public static Group parseGroup(String string) {
        if (isNull(string)) {
            return null;
        }
        String[] strings = string.split(",");
        return newGroup(Integer.parseInt(strings[0]), strings[1]);
    }

    public static Room newRoom(Integer id, String name) {
        Room room = Room.newInstance();
        room.setRoomId(id);
        room.setRoomName(name);
        return room;
    }

    public static Room parseRoom(String string) {
        if (isNull(string)) {
            return null;
        }
        String[] strings = string.split(",");
        return newRoom(Integer.parseInt(strings[0]), strings[1]);
    }

    public static Teacher newTeacher(Integer id, String firstName, String lastName) {
        Teacher teacher = Teacher.newInstance();
        teacher.setTeacherId(id);
        teacher.setFirstName(firstName);
        teacher.setSurname(lastName);
        return teacher;
    }

    public static Teacher parseTeacher(String string) {
        if (isNull(string)) {
            return null;
        }
        String[] strings = string.split("[,\\s]+");
        return newTeacher(Integer.parseInt(strings[0]), strings[1], strings[2]);
    }

    public static Student newStudent(Integer id, String firstName, String lastName) {
        Student student = Student.newInstance();
        student.setStudentId(id);
        student.setFirstName(firstName);
        student.setSurname(lastName);
        return student;
    }

    public static Student parseStudent(String string) {
        if (isNull(string)) {
            return null;
        }
        String[] strings = string.split("[,\\s]+");
        return newStudent(Integer.parseInt(strings[0]), strings[1], strings[2]);
    }

    public static Student newStudent(Integer id, String firstName, String lastName, Group group) {
        Student student = newStudent(id, firstName, lastName);
        student.setGroup(group);
        return student;
    }

    public static Student parseStudent(String string, String group) {
        if (isNull(string)) {
            return null;
        }
        Student student = parseStudent(string);
        student.setGroup(parseGroup(group));
        return student;
    }
}
