## University app

### What is the project?

This software provides an opportunity to manage a timetable of university.

![Timetable](./docs/ScreenTimetable.jpg)

[UML class diagram](./docs/ClassDiagram.md)

### What technologies are used?

- Spring Boot
- Spring MVC
- Spring Data JDBC
- Spring AOP
- Thymeleaf
- Bootstrap

### How to build?

For building, you need JDK version 15 or later.

Open any command line interface (Command Prompt, PowerShell, Git Bash, etc.). 

Go to the root directory of the project.

Execute command

*Windows*
```
./make.bat
```
*Linux*
```
./make.sh
```
After building

*Windows*
```
cd dist
./app.bat
```
*Linux*
```
cd dist
./app.bat
```
Open any web browser and go to http://localhost:8080/