@echo off
rem ---------------------------------------------------------------------------
chcp 65001 1>nul
mkdir dist 2>nul
mkdir dist\init 2>nul
rem ---------------------------------------------------------------------------
echo.
echo Building university application...
echo version 0.1.5
echo.
call gradlew clean build --info

rem ---------------------------------------------------------------------------
copy build\libs\university-0.1.5.jar dist /Y 1>nul
copy conf\app.bat dist /Y 1>nul
copy conf\init.bat dist /Y 1>nul
copy conf\init\* dist\init /Y 1>nul
rem ---------------------------------------------------------------------------
echo.
