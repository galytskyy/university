#!/bin/bash
# ---------------------------------------------------------------------------
mkdir dist > /dev/null 2>&1
# mkdir dist/init > /dev/null 2>&1
# -------------------------------------------------------------------
echo
echo Building university application...
echo version 0.1.5 
echo
# -------------------------------------------------------------------
chmod a+x ./gradlew
./gradlew clean build --info
# -------------------------------------------------------------------
cp -f build/libs/university-0.1.5.jar dist
cp -f conf/app.sh dist
chmod a+x dist/app.sh
# -------------------------------------------------------------------
echo
